
/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2023/3/21 14:33:18                           */
/*==============================================================*/
# goodssort
use meizu;
INSERT INTO `tb_goodssort`(`sid`, `sname`, `sphoto`) VALUES (1, '手机', 'img/goodssort/1.jpg');
INSERT INTO `tb_goodssort`(`sid`, `sname`, `sphoto`) VALUES (2, '声学', 'img/goodssort/2.jpg');
INSERT INTO `tb_goodssort`(`sid`, `sname`, `sphoto`) VALUES (3, '配件', 'img/goodssort/3.jpg');
INSERT INTO `tb_goodssort`(`sid`, `sname`, `sphoto`) VALUES (4, 'PANDAER', 'img/goodssort/4.jpg');
INSERT INTO `tb_goodssort`(`sid`, `sname`, `sphoto`) VALUES (5, 'lipro', 'img/goodssort/5.jpg');

# goods
INSERT INTO `tb_goods`(`gid`, `sid`, `gname`, `gdetail`, `gdiscount`, `gphoto`,`gstate`) VALUES (1, 1, '魅族 18X', ' 高通骁龙 870 + UFS 3.1 强劲性能 | 10bit 120Hz OLED 屏幕 | Flyme 9.2 | 屏下光学指纹解锁 ', NULL, 'img/maingphoto/g1.jpg',default );
INSERT INTO `tb_goods`(`gid`, `sid`, `gname`, `gdetail`, `gdiscount`, `gphoto`,`gstate`) VALUES (2, 1, '魅族 18s', '高通骁龙 888+ | 162g 轻妙手感 | 6.2 英寸 2K+120Hz 满血小屏 | 0.1s 超声波指纹解锁 | Flyme 9.2 | 以旧换新 限时回收补贴至高400元 ', '【3.8起 购机限时赠魅族定制圆珠笔「50支一盒」赠完即止】【限时6期免息】\r\n【加价购特惠 手机壳仅需89元】【魅族商城APP积分兑红包 至高50元】', 'img/maingphoto/g2.jpg',default);
INSERT INTO `tb_goods`(`gid`, `sid`, `gname`, `gdetail`, `gdiscount`, `gphoto`,`gstate`) VALUES (3, 1, '魅族 18s Pro', '高通骁龙 888+ | AR 全场景大师影像系统 | 2K+120Hz 四曲微弧屏 | 0.1s 超声波指纹解锁 | 有线/无线双 40W 超充 | 以旧换新 限时回收补贴至高400元', '【3.8起 购机限时赠魅族定制圆珠笔「50支一盒」赠完即止】【限时12期免息】\r\n【加价购特惠 手机壳仅需89元】【魅族商城APP积分兑红包 至高50元】', 'img/maingphoto/g3.jpg',default);
INSERT INTO `tb_goods`(`gid`, `sid`, `gname`, `gdetail`, `gdiscount`, `gphoto`,`gstate`) VALUES (4, 2, '魅蓝 Blus 2 主动降噪耳机', '全场景 ANC 主动降噪 | 「妙音」ENC通话降噪 | 12mm 分频超动圈 | 智能佩戴检测 | IPX5 级防水 | 24 小时续航 | 无线充电 | lifeme 专属 App', '', 'img/maingphoto/g4.jpg',default);
INSERT INTO `tb_goods`(`gid`, `sid`, `gname`, `gdetail`, `gdiscount`, `gphoto`,`gstate`) VALUES (5, 2, '魅蓝 Blus Air 真无线耳机', '「妙音」双路四麦 ENC 智能通话降噪 | 定制 12mm 分频「超动圈」 | 50ms 超低延时 | 25小时超长续航 | 半入耳设计 IPX5 防水 | 专属 APP + flyme 妙连', '', 'img/maingphoto/g5.jpg',default);


# detial
# 18x
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (1, 1, 1699.00, '白色', '全网通公开版 8+128GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (2, 1, 1699.00, '黑色', '全网通公开版 8+128GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (3, 1, 1699.00, '蓝色', '全网通公开版 8+128GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (4, 1, 1849.00, '白色', '全网通公开版 8+256GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (5, 1, 1849.00, '黑色', '全网通公开版 8+256GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (6, 1, 1849.00, '蓝色', '全网通公开版 8+256GB');
# INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (7, 1, 1999.00, '白色', '全网通公开版 12+256GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (8, 1, 1999.00, '黑色', '全网通公开版 12+256GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (9, 1, 1999.00, '蓝色', '全网通公开版 12+256GB');
# 18s
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 2, 2699.00, '蓝色', '全网通公开版 8+128GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 2, 2999.00, '蓝色', '全网通公开版 8+256GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 2, 3299.00, '蓝色', '全网通公开版 12+256GB');
# 18s pro
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 3, 3499.00, '白色', '全网通公开版 8+128GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 3, 3499.00, '黑色', '全网通公开版 8+128GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 3, 3499.00, '蓝色', '全网通公开版 8+128GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 3, 3899.00, '白色', '全网通公开版 8+256GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 3, 3899.00, '黑色', '全网通公开版 8+256GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 3, 3899.00, '蓝色', '全网通公开版 8+256GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 3, 4299.00, '白色', '全网通公开版 12+256GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 3, 4299.00, '黑色', '全网通公开版 12+256GB');
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 3, 4299.00, '蓝色', '全网通公开版 12+256GB');
# Blus 2
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 4, 269.00, '天青色', null);
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 4, 269.00, '烟雨色', null);
# Blus Air
INSERT INTO `tb_detail`(`did`, `gid`, `dprice`, `dcolor`, `dversion`) VALUES (null, 5, 129.00, '白色', null);

#tb_photo
# 18x
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 1, '/img/detialphoto/white18x1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 1, '/img/detialphoto/white18x2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 1, '/img/detialphoto/white18x3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 1, '/img/detialphoto/white18x4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 2, '/img/detialphoto/black18x1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 2, '/img/detialphoto/black18x2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 2, '/img/detialphoto/black18x3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 2, '/img/detialphoto/black18x4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 3, '/img/detialphoto/blue18x1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 3, '/img/detialphoto/blue18x2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 3, '/img/detialphoto/blue18x3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 3, '/img/detialphoto/blue18x4.jpg');


INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 4, '/img/detialphoto/white18x1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 4, '/img/detialphoto/white18x2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 4, '/img/detialphoto/white18x3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 4, '/img/detialphoto/white18x4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 5, '/img/detialphoto/black18x1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 5, '/img/detialphoto/black18x2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 5, '/img/detialphoto/black18x3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 5, '/img/detialphoto/black18x4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 6, '/img/detialphoto/blue18x1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 6, '/img/detialphoto/blue18x2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 6, '/img/detialphoto/blue18x3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 6, '/img/detialphoto/blue18x4.jpg');


# INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 7, '/img/detialphoto/white18x1.jpg');
# INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 7, '/img/detialphoto/white18x2.jpg');
# INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 7, '/img/detialphoto/white18x3.jpg');
# INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 7, '/img/detialphoto/white18x4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 8, '/img/detialphoto/black18x1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 8, '/img/detialphoto/black18x2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 8, '/img/detialphoto/black18x3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 8, '/img/detialphoto/black18x4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 9, '/img/detialphoto/blue18x1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 9, '/img/detialphoto/blue18x2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 9, '/img/detialphoto/blue18x3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 9, '/img/detialphoto/blue18x4.jpg');

# 18s
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 10, '/img/detialphoto/blue18s1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 10, '/img/detialphoto/blue18s2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 10, '/img/detialphoto/blue18s3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 10, '/img/detialphoto/blue18s4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 11, '/img/detialphoto/blue18s1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 11, '/img/detialphoto/blue18s2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 11, '/img/detialphoto/blue18s3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 11, '/img/detialphoto/blue18s4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 12, '/img/detialphoto/blue18s1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 12, '/img/detialphoto/blue18s2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 12, '/img/detialphoto/blue18s3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 12, '/img/detialphoto/blue18s4.jpg');

# 18s pro
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 13, '/img/detialphoto/white18spro1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 13, '/img/detialphoto/white18spro2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 13, '/img/detialphoto/white18spro3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 13, '/img/detialphoto/white18spro4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 14, '/img/detialphoto/black18spro1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 14, '/img/detialphoto/black18spro2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 14, '/img/detialphoto/black18spro3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 14, '/img/detialphoto/black18spro4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 15, '/img/detialphoto/blue18spro1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 15, '/img/detialphoto/blue18spro2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 15, '/img/detialphoto/blue18spro3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 15, '/img/detialphoto/blue18spro4.jpg');


INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 16, '/img/detialphoto/white18spro1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 16, '/img/detialphoto/white18spro2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 16, '/img/detialphoto/white18spro3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 16, '/img/detialphoto/white18spro4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 17, '/img/detialphoto/black18spro1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 17, '/img/detialphoto/black18spro2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 17, '/img/detialphoto/black18spro3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 17, '/img/detialphoto/black18spro4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 18, '/img/detialphoto/blue18spro1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 18, '/img/detialphoto/blue18spro2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 18, '/img/detialphoto/blue18spro3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 18, '/img/detialphoto/blue18spro4.jpg');


INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 19, '/img/detialphoto/white18spro1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 19, '/img/detialphoto/white18spro2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 19, '/img/detialphoto/white18spro3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 19, '/img/detialphoto/white18spro4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 20, '/img/detialphoto/black18spro1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 20, '/img/detialphoto/black18spro2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 20, '/img/detialphoto/black18spro3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 20, '/img/detialphoto/black18spro4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 21, '/img/detialphoto/blue18spro1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 21, '/img/detialphoto/blue18spro2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 21, '/img/detialphoto/blue18spro3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 21, '/img/detialphoto/blue18spro4.jpg');

# Blus 2
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 22, '/img/detialphoto/greenb1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 22, '/img/detialphoto/greenb2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 22, '/img/detialphoto/greenb3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 22, '/img/detialphoto/greenb4.jpg');

INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 23, '/img/detialphoto/whiteb1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 23, '/img/detialphoto/whiteb2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 23, '/img/detialphoto/whiteb3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 23, '/img/detialphoto/whiteb4.jpg');

# Blus Air.jpg
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 24, '/img/detialphoto/whitea1.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 24, '/img/detialphoto/whitea2.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 24, '/img/detialphoto/whitea3.jpg');
INSERT INTO `tb_photo`(`pid`, `did`, `photo`) VALUES (null, 24, '/img/detialphoto/whitea4.jpg');



# 创建一个用户
insert into tb_user values (1,default,'../../img/people/head.png',null,'19892121401','a123456',default);
# 创建一个后台管理员
insert into tb_htuser values(null,'哈哈哈','19892121401','a123456');
