<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta charset="utf-8">
		<title>账号管理</title>
		<link rel="icon" href="http://www-res.flyme.cn/resources/flymeos/v4/assets/common/favicon.ico">
		<link rel="stylesheet" href="/css/userinfo.css">
		<script src="js/jquery-3.5.1.min.js?<%=Math.random()%>"></script>
		<script>history.go(1);</script>
		<script>
			let reader=new FileReader();
			onload=function(){
				let b=document.querySelector(".tankuang");
				$(".operate").click(function(){
					b.style.display="block";
				});
				$(".close").click(function(){
					b.style.display="none";
				});
				let file=document.querySelector("input[name='uheadshot']");
				file.addEventListener("change",function () {
					let one=event.target.files[0];//获取选中文件
					reader.readAsDataURL(one);//读取文件
				});
				reader.onload=function (){//读取后自动执行
					document.querySelector("#myimg").src=reader.result;
				};
				$(".out").click(function (){
					if (confirm("您确定要退出吗？")){
						location.href="/user";
					}
				})
			}
			function toIndex() {
				open("/index");
			}
		</script>
	</head>
	<body>
		<c:if test="${ not empty error}" var="err">
			<h1 style="color: red">${error}</h1>
		</c:if>
		<c:if test="${not err}">
			<!-- 头部开始 -->
			<div class="head">
				<div class="logo" onclick="toIndex()"></div>
				<ul>
					<li style="width:73px;">
						<a class="a1" style="background-position-x: -168px;" href="/index"></a>
					</li>
					<li style="width:37px;">
						<a class="a2" style="background-position-x: -288px;"></a>
					</li>
					<li style="width:55px;">
						<a class="a3" style="background-position-x: -374px;"></a>
					</li>
					<li style="width:60px;">
						<a class="a4" style="background-position-x: -473px;"></a>
					</li>
					<li style="width:39px;">
						<a class="a5" style="background-position-x: -577px;"></a>
					</li>
					<li style="width:39px;">
						<a class="a6" style="background-position-x: -661px;"></a>
					</li>
				</ul>
				<div class="headRight">
			<span>
				<a class="out" style="width: 28px;">退出</a>
				<span id="line">|</span>
				<c:if test="${'user_'.equals(user.uname)}" var="r">
					<a style="max-width: 150px;" href="/userinfo?v=show">${user.uname}${user.uid}</a>
				</c:if>
				<c:if test="${!r}">
					<a style="max-width: 150px;" href="/userinfo?v=show">${user.uname}</a>
				</c:if>
			</span>
				</div>
			</div>
			<!-- 头部结束 -->
			<!-- 主体开始 -->
			<div class="context">
				<div class="contexhead">
					<ul>
						<li>
							<a>账号管理</a>
						</li>
					</ul>
				</div>
				<div class="name">
					<div class="photo">
						<img src="${user.uheadshot}" width="100%" height="100%">
					</div>
					<div class="nickname">
						<div style="height: 30px;">
							<div class="nname">
								<c:if test="${'user_'.equals(user.uname)}" var="nr">
									<a style="max-width: 150px;">${user.uname}${user.uid}</a>
								</c:if>
								<c:if test="${!nr}">
									<a style="max-width: 150px;">${user.uname}</a>
								</c:if>
							</div>
						</div>
						<div class="operate">编辑账号信息</div>
					</div>
				</div>
				<div class="message">
					<div class="safehead">
						<div class="safeleft">账号安全</div>
					</div>
					<div class="main">
						<div class="part">
							<div class="one">邮箱</div>
							<div class="middle">
								<c:if test="${empty user.uemail || ''.equals(user.uemail)}" var="er">
									<p class="up">未绑定</p>
									<p class="down">绑定后可通过邮箱找回密码</p>
								</c:if>
								<c:if test="${not er}">
									<p class="up">${user.uemail}</p>
									<p class="down">已绑定，可通过邮箱找回密码</p>
								</c:if>
							</div>
						</div>
						<div class="part">
							<div class="one">手机号码</div>
							<div class="middle">
								<p class="up">
										${user.nphone}
								</p>
								<p class="down">已验证，可通过手机找回密码</p>
							</div>
						</div>
						<div class="part">
							<div class="one">密码修改</div>
							<div class="middle">
								<p class="up">已设置</p>
								<p class="down">建议使用数字和字母混用设置密码</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- 主体结束 -->
			<!-- 底部开始 -->
			<div class="footer">
				<div class="footerInner">
					<div class="serves">
						<div class="innerLink">
							<a>关于魅族</a>
							<span></span>
							<a>工作机会</a>
							<span></span>
							<a>联系我们</a>
							<span></span>
							<a>法律声明</a>
							<span></span>
							<a>常见问题</a>
							<span></span>
							<div class="language">
								简体中文&nbsp;&nbsp;&nbsp;
								<a class="English">English</a>
							</div>
						</div>
						<div class="serve">
							<span>客服热线&nbsp;</span>
							<span>400-788-3333&nbsp;</span>
							<a> 在线客服</a>
						</div>
						<div class="subscribe">
							<a>
								<i class="sub-img1"></i>
							</a>
							<a class="weixin">
								<img src="img/buttomImg/weixin.png" />
								<i class="sub-img2"></i>
							</a>
							<a>
								<i class="sub-img3"></i>
							</a>
						</div>
					</div>
					<div class="copyrightWrap">
						<span>©2023 Meizu Telecom Equipment Co., Ltd. All rights reserved.</span>
						<a style="width: 166px;">备案号: 粤ICP备13003602号-4</a>
						<a style="width: 178.5px;">经营许可证编号: 粤B2-20130198</a>
						<a style="width: 48px;">营业执照</a>
					</div>
				</div>
			</div>
			<!-- 弹框 -->
			<div class="tankuang">
				<div class="xiugaikuang">
					<p class="close-tips">
						<button class="close">×</button>
					</p>
					<form id="myform" method="post" action="/userinfo" enctype="multipart/form-data" >
						<input type="hidden" name="v" value="change">
						<input type="hidden" name="uid" value="${user.uid}">
						<div class="upload">+
							<input type="file" name="uheadshot" accept="image/*">
							<img id="myimg" width="100" height="100" src="${user.uheadshot}">
							<p>点击图片更换头像</p>
						</div>
						<div class="change">
							<div class="op">
								<div class="changeop">用户名：</div>
								<div class="input" ><input name="uname" value="${user.uname}"/></div>
							</div>
							<div class="op">
								<div class="changeop">邮箱：</div>
								<div class="input"><input name="uemail" type="email" value="${user.uemail}"/></div>
							</div>
							<div class="op">
								<div class="changeop">手机号：</div>
								<div class="input"><input name="uphone" value="${user.uphone}"/></div>
							</div>
							<div class="op">
								<div class="changeop">密码：</div>
								<div class="input"><input name="upassword" type="password" value="${user.upassword}"/></div>
							</div>
						</div>
						<div class="submit">
							<div class="tip"></div>
							<button type="submit" class="button" onclick="return check()">更新</button>
						</div>
					</form>
				</div>
			</div>
		</c:if>
	</body>
	<script>
		function check() {
			let uname=document.querySelector('[name="uname"]').value;
			let uemail=document.querySelector('[name="uemail"]').value;
			let uphone=document.querySelector('[name="uphone"]').value;
			let upassword=document.querySelector('[name="upassword"]').value;
			let tip = document.querySelector(".tip");
			let reg = /^\s*$/;
			if (!(reg.test(uname) || reg.test(uemail) || reg.test(uphone) || reg.test(upassword))) {
				if (/^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\d{8}$/.test(uphone)) {
					if (/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(uemail)){
						if (/^(?=.*\d)(?=.*[A-z])[\da-zA-Z]{6,15}$/.test(upassword)) {
							return true;
						} else {
							tip.style.display = "block";
							tip.innerHTML = `更改的密码不符合规范（6-15位，含字母）`;
							return false;
						}
					}else{
						tip.innerHTML = `请输入正确的邮箱`;
						tip.style.display = "block";
						return false;
					}
				} else {
					tip.innerHTML = `请输入正确的手机号`;
					tip.style.display = "block";
					return false;
				}
			} else {
				tip.style.display = "block";
				tip.innerHTML = `修改栏目的内容不能为空`;
				return false;
			}
		}
	</script>
</html>
