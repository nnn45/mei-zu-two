<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta charset="utf-8">
		<title>商品列表（搜索）</title>
		<link rel="shortcut icon" href="../../img/订单确认/favicon-90c2e618ff.ico" />
		<link rel="stylesheet" href="../../css/top.css"/>
		<link rel="stylesheet" href="../../css/base.css"/>
		<link rel="stylesheet" href="../../css/foot.css"/>
<%--		<link rel="stylesheet" href="../../css/goodlist.css"/>--%>
		<script src="../../js/jquery-3.5.1.min.js"></script>
		<script type="text/javascript">
			document.write('<link rel="stylesheet" type="text/css" href="../../css/goodlist.css?v=' + Math.random() + '"/>');
		</script>
	</head>
	<script>
		$(function () {
			$(".choices a").click(function () {
				$(".choices a").css("color", "#333");
				$(this).css("color", "#008cff");
			});
		});
		$(function () {
			$("#left").click(function (){
				console.log("进入了上一页")
				if ($("#pageno").val()>1){
					let pageNo=$("#pageno").val();
					let page=document.querySelectorAll("#center");
					page[pageNo-1].style.color="#333";
					pageNo=parseInt($("#pageno").val())-1;
					let search=$("#search").val();
					let order=$("#order").val();
					$.ajax({
						url:"/search",
						type:"post",
						data:{
							"v":"rightPage",
							pageNo,
							search,
							order
						},
						dataType:"json",
						success:function (resp) {
							let ul=document.querySelector(".product-box-div");

							let header=document.querySelector(".choices");
							header.innerHTML=" ";
							ul.innerHTML=" ";
							let html=``;
							// console.log(resp.pager.datas);
						if (resp.pager.datas!=null) {
							  header.innerHTML=`<a class="jiage" onclick="jiageSort()">价格↑</a>
                                          <a class="xinpin">新品</a>
                                           <a class="tuijian" onclick="tuijianSort()">推荐</a>`;
							resp.pager.datas.forEach(b => {
								html += `<a href="/detail?did=\${b.did}" target="_blank">
                               <li class="good">
                               <img src="\${b.gphoto}">
                               <ul class="coul">
                               </ul>
                               <h3>\${b.gname}</h3>
                               <p class="desc">&nbsp;\${b.gdiscount}\${b.gdetail}</p>
                               <p class="it-price">
                               <span>限时特惠 ￥</span>
                               <span class="vm-price">\${b.dprice}</span>
                               </p>
                               </li>
                               </a>`;

							})
							ul.innerHTML = html;
							$("#pageno").val(resp.pager.pageNo);
							$("#search").val(resp.search);
							$("#order").val(resp.order);
							if (order==1){
								$(".choices .jiage").css("color", "#008cff");
							}else {
								$(".choices .tuijian").css("color", "#008cff");
							}
							if (pageNo==pageCount){
								$("#right").css("color","gainsboro");
							}else{
								$("#right").css("color","#333");
							}
							if (pageNo==1){
								$("#left").css("color","gainsboro");
							}else {
								$("#left").css("color","#333");
							}
							let center=document.querySelectorAll("#center");
							center[pageNo-1].style.color="#008cff";
						 }
						}
					})
				}

			})
		})
		$(function () {
			$("#right").click(function () {
				console.log("进入了下一页")
				let pageNo = parseInt($("#pageno").val());
				let pageCount = parseInt($("#pageCount").val());
				if (pageNo < pageCount) {
					let page=document.querySelectorAll("#center");
					page[pageNo-1].style.color="#333";
					pageNo = pageNo + 1;
					let search = $("#search").val();
					let order=$("#order").val();
					$.ajax({
						url:"/search",
						type:"post",
						data:{
							"v":"rightPage",
							pageNo,
							search,
							order
						},
						dataType: "json",
						success: function (resp) {
							let ul = document.querySelector(".product-box-div");
							let header=document.querySelector(".choices");
							header.innerHTML=" ";
							ul.innerHTML =" ";
							let html = ``;
							// console.log(resp);
							if (resp.pager.datas != null) {
								header.innerHTML = `<a class="jiage" onclick="jiageSort()">价格↑</a>
                                            <a class="xinpin">新品</a>
                                          <a class="tuijian" onclick="tuijianSort()">推荐</a>`;
								resp.pager.datas.forEach(a => {
									html += `<a href="/detail?did=` + a.did + `" target="_blank">
                                     <li class="good">
                                     <img src="` + a.gphoto + `">
                                     <ul class="coul">
                                     </ul>
                                     <h3>` + a.gname + `</h3>
                                     <p class="desc">&nbsp;` + a.gdiscount + a.gdetail + `</p>
                                     <p class="it-price">
                                     <span>限时特惠 ￥</span>
                                     <span class="vm-price">` + a.dprice + `</span>
                                     </p>
                                     </li>
                                     </a>`;

								})
								// console.log(html)
								ul.innerHTML = html;
								$("#pageno").val(resp.pager.pageNo);
								$("#search").val(resp.search);
								$("#order").val(resp.order);
								if (order==1){
									$(".choices .jiage").css("color", "#008cff");
								}else {
									$(".choices .tuijian").css("color", "#008cff");
								}
								if (pageNo==pageCount){
									$("#right").css("color","gainsboro");
								}else{
									$("#right").css("color","#333");
								}
								if (pageNo==1){
									$("#left").css("color","gainsboro");
								}else {
									$("#left").css("color","#333");
								}
								let center=document.querySelectorAll("#center");
								center[pageNo-1].style.color="#008cff";
							}
						}
					})
				}

			})
		})
		/*按价格升序*/
		function jiageSort(){
			let pageCount=$("#pageCount").val();
			let order=1;
			let search = $("#search").val();
			let pageNo=$("#pageno").val();
			$.ajax({
				url:"/search",
				type:"post",
				data:{
					"v":"rightPage",
					order,
					search,
					pageNo
				},
				dataType:"json",
				success:(resp=>{
					let ul = document.querySelector(".product-box-div");
					let header=document.querySelector(".choices");
					header.innerHTML=" ";
					ul.innerHTML = " ";
					let html = ``;
					// console.log(resp);
					if (resp.pager.datas != null) {
						header.innerHTML = `<a class="jiage" onclick="jiageSort()">价格↑</a>
                                            <a class="xinpin">新品</a>
                                           <a class="tuijian" onclick="tuijianSort()">推荐</a>`;
						resp.pager.datas.forEach(a => {
							html += `<a href="/detail?did=` + a.did + `" target="_blank">
                                     <li class="good">
                                     <img src="` + a.gphoto + `">
                                     <ul class="coul">
                                     </ul>
                                     <h3>` + a.gname + `</h3>
                                     <p class="desc">&nbsp;` + a.gdiscount + a.gdetail + `</p>
                                     <p class="it-price">
                                     <span>限时特惠 ￥</span>
                                     <span class="vm-price">` + a.dprice + `</span>
                                     </p>
                                     </li>
                                     </a>`;

						})
						ul.innerHTML = html;
						$("#pageno").val(resp.pager.pageNo);
						$("#search").val(resp.search);
						$("#order").val(resp.order);
						if (order==1){
							$(".choices .jiage").css("color", "#008cff");
						}else {
							$(".choices .tuijian").css("color", "#008cff");
						}
						if (pageNo==pageCount){
							$("#right").css("color","gainsboro");
						}else{
							$("#right").css("color","#333");
						}
						if (pageNo==1){
							$("#left").css("color","gainsboro");
						}else {
							$("#left").css("color","#333");
						}
						let center=document.querySelectorAll("#center");
						center[pageNo-1].style.color="#008cff";
					}
				})
			})
		}
		/*按价格降序*/
		function tuijianSort(){
			console.log("进入了推荐")
			let pageCount=$("#pageCount").val();
			let order=0;
			let search = $("#search").val();
			let pageNo=$("#pageno").val();
			$.ajax({
				url:"/search",
				type:"post",
				data:{
					"v":"rightPage",
					order,
					search,
					pageNo

				},
				dataType:"json",
				success:(resp=>{
					let ul = document.querySelector(".product-box-div");
					let header=document.querySelector(".choices");
					header.innerHTML=" ";
					ul.innerHTML = " ";
					let html = ``;
					if (resp.pager.datas != null) {
						header.innerHTML = `<a class="jiage" onclick="jiageSort()">价格↑</a>
                                            <a class="xinpin">新品</a>
                                           <a class="tuijian" onclick="tuijianSort()">推荐</a>`;
						resp.pager.datas.forEach(a => {
							html += `<a href="/detail?did=` + a.did + `" target="_blank">
                                     <li class="good">
                                     <img src="` + a.gphoto + `">
                                     <ul class="coul">
                                     </ul>
                                     <h3>` + a.gname + `</h3>
                                     <p class="desc">&nbsp;` + a.gdiscount + a.gdetail + `</p>
                                     <p class="it-price">
                                     <span>限时特惠 ￥</span>
                                     <span class="vm-price">` + a.dprice + `</span>
                                     </p>
                                     </li>
                                     </a>`;

						})
						ul.innerHTML = html;
						$("#pageno").val(resp.pager.pageNo);
						$("#search").val(resp.search);
						$("#order").val(resp.order);
						if (order==1){
							$(".choices .jiage").css("color", "#008cff");
						}else {
							$(".choices .tuijian").css("color", "#008cff");
						}
						if (pageNo==pageCount){
							$("#right").css("color","gainsboro");
						}else{
							$("#right").css("color","#333");
						}
						if (pageNo==1){
							$("#left").css("color","gainsboro");
						}else {
							$("#left").css("color","#333");
						}
						let center=document.querySelectorAll("#center");
						center[pageNo-1].style.color="#008cff";
					}
				})
			})
		}
		function topage(h){
			console.log("进入了"+h+"页")
			let pageCount=$("#pageCount").val();
			let search = $("#search").val();
            let pageNo=h;
			let order=$("#order").val();
			$.ajax({
				url:"/search",
				type:"post",
				data:{
					"v":"rightPage",
					pageNo,
					search,
					order
				},
				dataType:"json",
				success:(resp=>{
					let ul = document.querySelector(".product-box-div");
					let header=document.querySelector(".choices");
					header.innerHTML=" ";
					ul.innerHTML = " ";
					let html = ``;
					if (resp.pager.datas != null) {
						header.innerHTML = `<a class="jiage" onclick="jiageSort()">价格↑</a>
                                            <a class="xinpin">新品</a>
                                           <a class="tuijian" onclick="tuijianSort()">推荐</a>`;
						resp.pager.datas.forEach(a => {
							html += `<a href="/detail?did=` + a.did + `" target="_blank">
                                     <li class="good">
                                     <img src="` + a.gphoto + `">
                                     <ul class="coul">
                                     </ul>
                                     <h3>` + a.gname + `</h3>
                                     <p class="desc">&nbsp;` + a.gdiscount + a.gdetail + `</p>
                                     <p class="it-price">
                                     <span>限时特惠 ￥</span>
                                     <span class="vm-price">` + a.dprice + `</span>
                                     </p>
                                     </li>
                                     </a>`;

						})
						ul.innerHTML = html;
						$("#pageno").val(resp.pager.pageNo);
						$("#search").val(resp.search);
						$("#order").val(resp.order);
						if (order==1){
							$(".choices .jiage").css("color", "#008cff");
						}else {
							$(".choices .tuijian").css("color", "#008cff");
						}
						if (pageNo==pageCount){
							$("#right").css("color","gainsboro");
						}else{
							$("#right").css("color","#333");
						}
						if (pageNo==1){
							$("#left").css("color","gainsboro");
						}else {
							$("#left").css("color","#333");
						}
						let center=document.querySelectorAll("#center");
						center[pageNo-1].style.color = "#008cff";
						for (let i=0;i<center.length;i++) {
							if (i!=pageNo-1){
								center[i].style.color="#333";
							}
						}
					}
				})
			})
		}
	</script>
	<body>
		<c:import url="top.jsp"></c:import>
		<div class="main">
			<div class="result">
				<div class="headto">
					<div class="allres">全部结果 >&nbsp;</div>
					<input type="hidden" value="${search}" id="search">
					<input type="hidden" value="${pager.pageNo}" id="pageno">
					<input type="hidden" value="${order}" id="order">
					<div class="span">&nbsp;${search}</div>
					<c:if test="${empty pager.datas}" var="none">
					</c:if>
					<c:if test="${not none}">
					<div class="choices">
						<a class="jiage" onclick="jiageSort()">价格↑</a>
						<a class="xinpin">新品</a>
						<a class="tuijian" onclick="tuijianSort()">推荐</a>
					</div>
					<c:if test="${order==1}" var="co">
					<script>
						$(".choices .jiage").css("color", "#008cff");
					</script>
					</c:if>
					<c:if test="${not co}">
					<script>
						$(".choices .tuijian").css("color", "#008cff");
					</script>
					</c:if>
					</c:if>
				</div>
				<c:if test="${none}">
				<div class="none">没有找到相关产品，建议您尝试其他关键字</div>
				</c:if>
				<c:if test="${not none}">
				<div class="results">
					<ul class="product-box-div">
						<c:forEach items="${pager.datas}" var="p">
							<a href="/detail?did=${p.did}" target="_blank">
								<li class="good">
									<img src="${p.gphoto}">
									<h3>${p.gname}</h3>
									<p class="desc">&nbsp;${p.gdiscount}${p.gdetail}</p>
									<p class="it-price">
										<span>限时特惠 ￥</span>
										<span class="vm-price">${p.dprice}</span>
									</p>
								</li>
							</a>
						</c:forEach>
					</ul>
				</div>
				</c:if>
			</div>
		</div>
		<c:if test="${pager.pageCount>1}">
		<div class="fenye">
			<span class="fenye-bottle" id="left">&lt;</span>
			<input type="hidden" value="${pager.pageCount}" id="pageCount">
			<c:forEach begin="1" end="${pager.pageCount}" step="1" var="h">
				<span class="fenye-bottle" id="center" onclick="topage(${h})">${h}</span>
			</c:forEach>
			<span class="fenye-bottle" id="right">&gt;</span>
		</div>
		</c:if>
		<c:import url="foot.jsp"></c:import>
	</body>
     <script>
		 $(function () {
		 	//拿到所有的span标签
			 let page=document.querySelectorAll("#center");
			 //把1字的颜色改了
			 page[0].style.color="#008cff";
		 })
	 </script>
</html>