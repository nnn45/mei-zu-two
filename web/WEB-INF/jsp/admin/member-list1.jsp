<%--
  Created by IntelliJ IDEA.
  User: asus
  Date: 2023/4/9
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    <meta charset="UTF-8">
    <title>用户管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8" />
    <link rel="stylesheet" href="../../../admin/css/font.css">
    <link rel="stylesheet" href="../../../admin/css/xadmin.css">
    <link rel="stylesheet" href="../../../admin/css/datatables.css">
    <script src="../../../js/jquery-3.5.1.min.js"></script>
    <script src="../../../admin/js/datatables.js"></script>
    <script src="../../../admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="../../../admin/js/xadmin.js"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="">首页</a>
            <a href="">演示</a>
            <a>
              <cite>导航元素</cite></a>
          </span>
    <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
        <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
</div>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body layui-table-body layui-table-main">
                    <table class="layui-table layui-form" id="myTable">
                        <thead>
                        <tr>
                            <th>用户id</th>
                            <th>用户名</th>
                            <th>邮箱</th>
                            <th>手机</th>
                            <th>密码</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${users}" var="u" varStatus="i">
                            <tr>
                                <td>${u.uid}</td>
                                <td>${u.uname}</td>
                                <td>${u.uemail}</td>
                                <td>${u.uphone}</td>
                                <td class="pwd">${u.upassword}</td>
                                <c:if test="${u.ustate == 0}" var="op">
                                <td class="td-status">
                                    <span class="layui-btn layui-btn-normal layui-btn-mini">已启用</span></td>
                                <td class="td-manage">
                                    <a onclick="member_stop(this,${u.uid})" href="javascript:;"  title="停用">
                                        <i class="layui-icon">&#xe601;</i>
                                    </a>
                                    </c:if>
                                    <c:if test="${not op}">
                                <td class="td-status">
                                    <span class="layui-btn layui-btn-normal layui-btn-mini layui-btn-disabled">已停用</span></td>
                                <td class="td-manage">
                                    <a onclick="member_stop(this,${u.uid})" href="javascript:;"  title="启用">
                                        <i class="layui-icon">&#xe62f;</i>
                                    </a>
                                    </c:if>
                                    <%--感觉不需要，前台用户自己修改就好--%>
                                    <%--<a title="编辑"  onclick="xadmin.open('编辑','/ht/user?v=editInfos&uid=${u.uid}',600,400)" href="javascript:;">
                                        <i class="layui-icon">&#xe642;</i>
                                    </a>--%>
                                    <a onclick="member_reset(this,${u.uid})" title="重置密码" href="javascript:;">
                                        <i class="layui-icon">&#xe631;</i>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    layui.use(['laydate','form'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;


        // 监听全选
        form.on('checkbox(checkall)', function(data){

            if(data.elem.checked){
                $('tbody input').prop('checked',true);
            }else{
                $('tbody input').prop('checked',false);
            }
            form.render('checkbox');
        });

        //执行一个laydate实例
        laydate.render({
            elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
            elem: '#end' //指定元素
        });


    });

    /*用户-停用*/
    function member_stop(obj,id){
        var tips = "";
        if($(obj).attr('title')=='停用'){
            tips="确认要停用吗？"
        }else {
            tips="确认要启用吗？";
        }
        layer.confirm(tips,function(index){
            var isUse = 0;//判断是否停用
            if($(obj).attr('title')=='停用'){

                //发异步把用户状态进行更改
                $(obj).attr('title','启用')
                $(obj).find('i').html('&#xe62f;');

                $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已停用');
                layer.msg('已停用!',{icon: 4,time:1000});
                isUse = -1;
            }else{
                $(obj).attr('title','停用')
                $(obj).find('i').html('&#xe601;');

                $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已启用');
                layer.msg('已启用!',{icon: 1,time:1000});
                isUse = 1;
            }
            $.ajax({
                url:"/ht/user",
                data:{
                    v:"updateState",
                    id,isUse
                }
            })
        });
    }
    /*用户-重置密码*/
    function member_reset(obj,id){
        layer.confirm('确认要重置密码吗？',function(index){;
            $.ajax({
                url:"/ht/user",
                data:{
                    v:"resetPwd",
                    id
                }
            })
            $(obj).parents("tr").find(".pwd").html('a123456');
            layer.msg('重置成功!',{icon: 1,time:1000});
        });
    }

    /**
     * 自带分页，搜索
     */
    $(document).ready(function () {

        $('#myTable').DataTable({
            language: {
                "sProcessing": "处理中...",
                "sLengthMenu": "显示 _MENU_ 项结果",
                "sZeroRecords": "没有匹配结果",
                "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                "sInfoPostFix": "",
                "sSearch": "搜索:",
                "sUrl": "",
                "sEmptyTable": "表中数据为空",
                "sLoadingRecords": "载入中...",
                "sInfoThousands": ",",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "上页",
                    "sNext": "下页",
                    "sLast": "末页"
                },
                "oAria": {
                    "sSortAscending": ": 以升序排列此列",
                    "sSortDescending": ": 以降序排列此列"
                }
            }
        });
    });
</script>
</html>