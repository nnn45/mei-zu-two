<%--
  Created by IntelliJ IDEA.
  User: asus
  Date: 2023/4/9
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta charset="utf-8">
		<title>魅族后台登录</title>
		<%--引入页签logo--%>
		<link href="https://login.flyme.cn/favicon.ico" rel="shortcut icon" />
		<link rel="stylesheet" href="../../css/login.css">
		<link rel="stylesheet" href="../../css/foot2.css"/>
		<script src="../../../js/jquery-3.5.1.min.js"></script>
		<script src="../../../admin/js/stopExecutionOnTimeout.js" type="text/javascript"></script>
		<script>
			function closea() {
				let div=document.querySelector(".tip-box");
				console.log(div)
				div.style.display="none";
			}
			$(function () {
				$(".header-logo").click(function () {
					location.href = "/index";
				})
			})
		</script>
		<%--实现鼠标跟随动画效果--%>
		<style>
			.content {
				position: absolute;
				width: 100%;
				height: 100%;
			}
			/*实现鼠标跟随动画效果*/
			canvas{
				margin: 0;
				padding: 0;
				display: block;
				touch-action: none;
				position: absolute;
				pointer-events: none;/*canvas遮挡页面元素，导致元素点击失效*/
			}
		</style>
   </head>
	<body>
		<div class="content">
			<div class="header clarefix">
				<a class="header-logo">
					<img src="../../img/reisterimg/mei.png" alt="">
				</a>
			</div>
			<div class="banner-box">
				<div class="rgbox">
					<form class="min-form" action="/htLogin" id="formId" method="post">
						<div class="tap-tile"><a>后台登录</a></div>
						<div class="tip-box" style="display:${display};">
						  <span class="err-ico"></span>
						  <span class="tip-font">${error}</span>
						  <span class="close-ico" onclick="closea()"></span>
					   </div>
						<div class="phone">
							<!-- 手机号码 -->
							<div class="phone-row">
								<input type="hidden" name="v" value="login">
								<input type="text" id="input1" placeholder="手机号码" name="htphone"/>
							</div>
						</div>
						<div class="password-row">
							<!-- 密码 -->
							<div class="password">
								<input type="password" id="input2" placeholder="密码" name="htpwd"/>
							</div>
						</div>
							<!-- 登录按钮 -->
						<a class="register-btn" style="text-align: center" onclick="document:formId.submit()">登录</a>
						<%--<div class="toLogin">
							<a href="/user?v=Register">注册</a>
						</div>--%>
					</form>
				</div>
			</div>
		</div>
		<%--引入鼠标跟随的动画效果--%>
		<canvas></canvas>
		<!-- 底部 -->
		<c:import url="../foot2.jsp"></c:import>
	</body>
	<script>
	document.querySelector("#input1").addEventListener("input",function () {
	let div=document.querySelector(".tip-box");
	console.log(div)
	div.style.display="none";
	})
	document.querySelector("#input2").addEventListener("input",function () {
	let div=document.querySelector(".tip-box");
	console.log(div)
	div.style.display="none";
	})

	$("body").keydown(function () {
			 if (event.keyCode===13){
			 	$(".register-btn").click();
			 }
		 })
	</script>
	<script>
		var canvas = document.querySelector('canvas');
		canvas.height = window.innerHeight;
		canvas.width = window.innerWidth;
		c = canvas.getContext('2d');

		window.addEventListener('resize', function () {
			canvas.height = window.innerHeight;
			canvas.width = window.innerWidth;

			initCanvas();
		});

		var mouse = {
			x: undefined,
			y: undefined };

		window.addEventListener('mousemove',
				function (event) {
					mouse.x = event.x;
					mouse.y = event.y;
					drawCircles();
				});

		window.addEventListener("touchmove",
				function (event) {
					let touch = event.touches[0];
					mouse.x = touch.clientX;
					mouse.y = touch.clientY;
					drawCircles();
				});


		function Circle(x, y, radius, vx, vy, rgb, opacity, birth, life) {
			this.x = x;
			this.y = y;
			this.radius = radius;
			this.minRadius = radius;
			this.vx = vx;
			this.vy = vy;
			this.birth = birth;
			this.life = life;
			this.opacity = opacity;

			this.draw = function () {
				c.beginPath();
				c.arc(this.x, this.y, this.radius, Math.PI * 2, false);
				c.fillStyle = 'rgba(' + rgb + ',' + this.opacity + ')';
				c.fill();
			};

			this.update = function () {
				if (this.x + this.radius > innerWidth || this.x - this.radius < 0) {
					this.vx = -this.vx;
				}

				if (this.y + this.radius > innerHeight || this.y - this.radius < 0) {
					this.vy = -this.vy;
				}

				this.x += this.vx;
				this.y += this.vy;

				this.opacity = 1 - (frame - this.birth) * 1 / this.life;

				if (frame > this.birth + this.life) {
					for (let i = 0; i < circleArray.length; i++) {
						if (this.birth == circleArray[i].birth && this.life == circleArray[i].life) {
							circleArray.splice(i, 1);
							break;
						}
					}
				} else {
					this.draw();
				}
			};

		}

		var circleArray = [];

		function initCanvas() {
			circleArray = [];
		}

		var colorArray = [
			'355,85,80',
			'9,80,100',
			'343,81,45'];


		function drawCircles() {
			for (let i = 0; i < 6; i++) {
				let radius = Math.floor(Math.random() * 4) + 2;
				let vx = Math.random() * 2 - 1;
				let vy = Math.random() * 2 - 1;
				let spawnFrame = frame;
				let rgb = colorArray[Math.floor(Math.random() * colorArray.length)];
				let life = 100;
				circleArray.push(new Circle(mouse.x, mouse.y, radius, vx, vy, rgb, 1, spawnFrame, life));

			}
		}

		var frame = 0;
		function animate() {
			requestAnimationFrame(animate);
			frame += 1;
			c.clearRect(0, 0, innerWidth, innerHeight);
			for (let i = 0; i < circleArray.length; i++) {
				circleArray[i].update();
			}

		}

		initCanvas();
		animate();

		// This is just for demo purposes :
		for (let i = 1; i < 110; i++) {
			(function (index) {
				setTimeout(function () {
					mouse.x = 100 + i * 10;
					mouse.y = 100;
					drawCircles();
				}, i * 10);
			})(i);
		}
	</script>
</html>