<%--
  Created by IntelliJ IDEA.
  User: Anny
  Date: 2023/4/11
  Time: 10:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>订单详情</title>
    <link rel="stylesheet" href="../../../css/base.css"/>
    <script type="text/javascript">
        //清除CSS缓存
        document.write('<link rel="stylesheet" type="text/css" href="../../../css/orderdetail.css?v=' + Math.random() + '"/>');
    </script>
    <script src="../../../js/jquery-3.5.1.min.js"></script>
    <script>
        var total=0;
    </script>
    <style>
        .bigbox {
            float: none;
            width: 85%;
            min-height: 203px;
            margin-bottom: 40px;
        }
        .bigbox .editBox {
            margin-left: 60%;
        }
        .bigbox .address {
            margin-left: 60%;
        }
    </style>
</head>
<body>
    <div class="bigbox">
    <!--- 订单详情 ------------------------->
    <div id="right-top">
        <a>订单详情</a>
    </div>
    <div class="order-state">
        订单状态：<span class="os"></span>
        <c:if test="${order.ostate >=2}">
            <div>快递单号：<span>${order.cnum}</span></div>
        </c:if>
    </div>
    <div class="title cleanfix">&nbsp;&nbsp;&nbsp;&nbsp;订单号：<span style="color: #666">${order.oid}</span></div>
    <table class="righttablebox">
        <tr>
            <td class="product" colspan="2">供应商：魅族</td>
            <td >单价</td>
            <td >数量</td>
            <td >优惠</td>
            <td >小计</td>
            <td >售后</td>
        </tr>
        <c:forEach items="${htod}" var="od">
            <tr>
                <td style="width: 231px;">
                    <a>
                        <img src="${od.cphoto}"/>
                    </a>
                </td>
                <td class="product" style="width: 302px;">
                                <span>
                                    <a>${od.gname}
                                        <br>${od.dversion}&nbsp;&nbsp;${od.dcolor}<br>
                                    </a>
                                </span>
                </td>
                <td class="price" style="width:90px;">￥${od.dprice}</td>
                <td class="count" style="width: 66px;">${od.count}</td>
                <td class="preferential" style="width: 178px;">--</td>
                <td class="black-two" style="width: 93px;">￥${od.dprice * od.count}</td>
                <td class="after-sales" style="width: 67px;">--</td>
            </tr>
            <script>
                total+=${od.dprice * od.count};
            </script>
        </c:forEach>
        <tr>
            <td class="ticket-line" colspan="7">
                <div class="ticket-line-item">
                    <span>发票类型：</span>
                    <span>电子发票</span>
                    <span><p style="position: absolute;top: -1px;left: 3px;">？</p></span>
                    <span>发票抬头：</span>
                    <span>${add.aname}</span>
                </div>
            </td>
        </tr>
    </table>
    <div class="item-makeorder">
        <ul>
            <li><span>商品总计</span>
                <span>￥<i class="hj1"></i></span>
            </li>
            <li><span >回购金抵扣</span>
                <span class="bg1">-￥0</span>
            </li>
            <li><span>礼品卡抵扣</span>
                <span class="bg1">-￥0</span>
            </li>
            <li><span>运费</span>
                <span>￥0</span>
            </li>
            <li class="end">
                <span>应付</span>
                <span class="bg1">￥<i class="hj2"></i></span>
            </li>
        </ul>
    </div>
    <div class="editBox">
        <span>收货人信息  </span>
    </div>
    <div class="address">
        <p>收货人：${add.aname}</p>
        <p>地址：${add.sheng}${add.shi}${add.qu}${add.jie}${add.adetail}</p>
        <p>电话：${add.dzphone}</p>
    </div>
</div>
</body>
<script>
    document.querySelector(".hj1").innerHTML=total;
    document.querySelector(".hj2").innerHTML=total;
    <c:if test="${order.ostate==0}">
    document.querySelector(".os").innerHTML="待支付";
    </c:if>
    <c:if test="${order.ostate==-1}">
    document.querySelector(".os").innerHTML="已取消";
    </c:if>
    <c:if test="${order.ostate==1}">
    document.querySelector(".os").innerHTML="待发货";
    </c:if>
    <c:if test="${order.ostate==2}">
    document.querySelector(".os").innerHTML="已发货";
    </c:if>
</script>
</html>
