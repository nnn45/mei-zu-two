<%--
  Created by IntelliJ IDEA.
  User: asus
  Date: 2023/4/9
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title>订单列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8"/>
    <link rel="stylesheet" href="../../../admin/css/font.css">
    <link rel="stylesheet" href="../../../admin/css/xadmin.css">
    <link rel="stylesheet" href="../../../admin/css/datatables.css">
    <script src="../../../js/jquery-3.5.1.min.js"></script>
    <script src="../../../admin/js/datatables.js"></script>
    <script src="../../../admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="../../../admin/js/xadmin.js"></script>
</head>
<script>
    let ctotal;
</script>
<body>
<div class="x-nav">
            <span class="layui-breadcrumb">
                <a>后台管理</a>
                <a>订单管理</a>
                <a>
                    <cite>订单列表</cite></a>
            </span>
    <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right"
       onclick="location.reload()" title="刷新">
        <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
    </a>
</div>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body ">
                    <table class="layui-table layui-form" id="myTable">
                        <thead>
                        <tr>
                            <th>排序</th>
                            <th>订单号</th>
                            <th>数量</th>
                            <th>收货人</th>
                            <th>订单状态</th>
                            <th>收货地址</th>
                            <th style="text-align: center;">操作</th>
                        </tr>
                        </thead>
                        <tbody>

                        <c:forEach items="${list}" var="order" varStatus="obj">
                            <script>
                                ctotal=0;
                            </script>
                            <c:forEach items="${order.ods}" var="ods">
                                <script>
                                    ctotal+=${ods.count};
                                </script>
                            </c:forEach>
                            <tr>
                                <td>${obj.index+1}</td>
                                <td>${order.oid}</td>
                                <td class="ccount${obj.index+1}"></td>
                                <td>${order.address.aname}&nbsp;&nbsp;${order.address.dzphone}</td>
                                <td style="text-align: center;">
                                <c:if test="${order.ostate==-1}">
                                    订单已取消
                                </c:if>
                                <c:if test="${order.ostate==0}">
                                    待付款
                                </c:if>
                                <c:if test="${order.ostate==1}">
                                    <span style="color: red">待发货</span>
                                </c:if>
                                <c:if test="${order.ostate==2}">
                                    运送中
                                </c:if>
                                <c:if test="${order.ostate==9}">
                                    <span style="color: #ffba3b">订单已完成</span>
                                </c:if>
                                </td>
                                <td>${order.address.sheng}
                                    ${order.address.shi}
                                    ${order.address.qu}
                                    ${order.address.jie}
                                    ${order.address.adetail}
                                </td>
                                <td class="td-manage" style="text-align: center;">
                                    <a title="查看" onclick="xadmin.open('订单详情','/ht/order?v=orderDHT&oid=${order.oid}&aid=${order.aid}')"
                                       href="javascript:;">详情 </a>
                                    <c:if test="${order.ostate==1}">
                                        |<a title="发货"
                                           onclick="xadmin.open('确认发货','/ht/order?v=toAdd&oid=${order.oid}',600,400)"
                                           href="javascript:;">
                                            发货
                                        </a>
                                    </c:if>
                                </td>
                            </tr>
                            <script>
                                document.querySelector(".ccount${obj.index+1}").innerHTML=ctotal;
                            </script>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    layui.use(['laydate', 'form'],
        function () {
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
            });

            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
            });
        });

    $(document).ready(function () {

        $('#myTable').DataTable({
            language: {
                "sProcessing": "处理中...",
                "sLengthMenu": "显示 _MENU_ 项结果",
                "sZeroRecords": "没有匹配结果",
                "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                "sInfoPostFix": "",
                "sSearch": "搜索:",
                "sUrl": "",
                "sEmptyTable": "表中数据为空",
                "sLoadingRecords": "载入中...",
                "sInfoThousands": ",",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "上页",
                    "sNext": "下页",
                    "sLast": "末页"
                },
                "oAria": {
                    "sSortAscending": ": 以升序排列此列",
                    "sSortDescending": ": 以降序排列此列"
                }
            }
        });
    });

</script>

</html>