<%--
  Created by IntelliJ IDEA.
  User: asus
  Date: 2023/4/9
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title>分类管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="../../../admin/css/font.css">
    <link rel="stylesheet" href="../../../admin/css/xadmin.css">
    <script src="../../../admin/js/datatables.js"></script>
    <link rel="stylesheet" href="../../../admin/css/layer.css">
    <link rel="stylesheet" href="../../../admin/css/layui.css">
    <script type="text/javascript" src="../../../admin/js/layui.js"></script>
    <script type="text/javascript" src="../../../admin/js/xadmin.js"></script>
    <script src="../js/jquery-3.4.1.js"></script>
    <script src="/js/jquery-3.5.1.min.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<%session.removeAttribute("errorName");%>
<body>
<table  class="layui-table" lay-size="lg">
    <tr>
        <td>类别编号</td>
        <td>类别名称</td>
        <td>操作</td>
    </tr>
    <c:forEach items="${goodsSort}" var="g">
        <tr>
            <td>${g.sid}</td>
            <td>${g.sname}</td>
            <td>
                <a title="编辑" onclick="xadmin.open('编辑','/ht/goodssort?v=goodssortUpdate&sid=${g.sid}',600,400)" href="javascript:;">
                    <i class="layui-icon" style="font-size: 22px">&#xe642;</i></a>
            </td>
        </tr>
    </c:forEach>
</table>


<script>
    /*
            String name = req.getParameter("name");
        String desc = req.getParameter("desc");
        double price = Double.parseDouble(req.getParameter("price"));
        Integer stock = Integer.parseInt(req.getParameter("stock"));
        String img = req.getParameter("img");
     */
    function imageUpload() {
        let formData = new FormData();
        formData.append("img", document.getElementById("image").files[0]);
        $.ajax({
            url: "/upload/img",
            type: "POST",
            data: formData,
            /**
             *必须false才会自动加上正确的Content-Type
             */
            contentType: false,
            /**
             * 必须false才会避开jQuery对 formdata 的默认处理
             * XMLHttpRequest会对 formdata 进行正确的处理
             */
            processData: false,
            success: data => {
                console.log(data)
                $("#imagedir").val(data.path);
                console.log("图路径:" + $("#imagedir").val());
                let path = "/upload/" + ($("#imagedir").val());
                $("#upimg").children("img").attr("src", path)
            },
            error: function (data) {

            }
        });
    }
    /*新增分类*/
    /*function add() {
        let index = layer.load(1, {
            shade: [0.3, '#fff'] //0.1透明度的白色背景
        });
        let name = $("#name").val();
        let desc = $("#desc").val();
        let price = $("#price").val();
        let stock = $("#stock").val();
        let img = $("#imagedir").val();
        if (name === "" || desc === "") {
            layer.msg("请输入！");
            return false;
        }
        let isNum = /^[0-9]*$ /;

        if (isNum.test(price) || isNum.test(stock) || stock < 0 || price < 0) {
            layer.msg("价格或者库存有误！！");
            return false;
        }
        $.post("/admin/product/add", {
            name,
            desc,
            price,
            stock,
            img
        }, data => {
            layer.close(index);
            if (data.status === 200) {
                layer.msg("商品添加成功！");
            } else {
                layer.msg("商品添加失败！");
            }
        })
    }*/
</script>
</body>
</html>