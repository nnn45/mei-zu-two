<%--
  Created by IntelliJ IDEA.
  User: asus
  Date: 2023/4/9
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    <meta charset="UTF-8">
    <title>商品添加</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="../../../css/ht_details.css">
    <link rel="stylesheet" href="../../../admin/css/layer.css">
    <link rel="stylesheet" href="../../../admin/css/layui.css">
    <script type="text/javascript" src="../../../admin/js/layui.js"></script>
    <script src="../../../js/jquery-3.5.1.min.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
  <div style="padding: 30px">
    <div style="padding-top:20px;background-color:white;padding-bottom: 20px">
     <form class="layui-form" action="/ht/product" method="post" enctype="multipart/form-data">
         <input type="hidden" name="v" value="productAdd">
         <div class="layui-form-item">
             <label class="layui-form-label">商品分类<span class="x-red">*</span></label>
             <div class="layui-input-block" style="width: 190px;">
                 <select name="sid" lay-verify="required" >
                     <option value=""></option>
                     <c:forEach items="${goodsSorts}" var="gs">
                     <option value="${gs.sid}">${gs.sname}</option>
                     </c:forEach>
                 </select>
             </div>
         </div>
        <div class="layui-form-item">
           <label class="layui-form-label">商品名<span class="x-red">*</span></label>
           <div class="layui-input-block">
              <input style="width: 190px;" type="text" name="gname" required  lay-verify="required" placeholder="请输入商品名" autocomplete="off" class="layui-input">
           </div>
        </div>
        <div class="layui-form-item">
           <label class="layui-form-label">商品详情<span class="x-red">*</span></label>
           <div class="layui-input-inline">
               <input type="text" name="gdetail" required lay-verify="required" placeholder="请输入商品详情" autocomplete="off" class="layui-input">
           </div>
        </div>
         <div class="layui-form-item">
             <label class="layui-form-label">商品优惠</label>
             <div class="layui-input-inline">
                 <input type="text" name="gdiscount" placeholder="请输入商品优惠" autocomplete="off" class="layui-input">
             </div>
         </div>
         <div class="layui-form-item">
             <label class="layui-form-label">商品主图<span class="x-red">*</span></label>
             <div class="upload">+
                 <input type="file" required lay-verify="required" name="gphoto" accept="image/*">
                 <img id="myimg" class="img1" width="100" height="100" src="../../../img/maingphoto/plus.png">
             </div>
         </div>
         <div class="layui-form-item">
            <div class="layui-input-block">
               <button class="layui-btn" lay-submit="" lay-filter="formDemo">新增</button>
            </div>
        </div>
     </form>
    </div>
  </div>
<script>
    let reader=new FileReader();

    let file=document.querySelector("input[name='gphoto']");
    file.addEventListener("change",function () {
        let one=event.target.files[0];//获取选中文件
        reader.readAsDataURL(one);//读取文件
    });
    reader.onload=function (){//读取后自动执行
        document.querySelector(".img1").src=reader.result;
    };
    layui.use('form', function(){
        var form = layui.form;
    });
</script>
</body>
</html>