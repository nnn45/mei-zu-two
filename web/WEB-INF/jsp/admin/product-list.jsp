<%--
  Created by IntelliJ IDEA.
  User: asus
  Date: 2023/4/9
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title>商品列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8"/>
    <link rel="stylesheet" href="../../../admin/css/font.css">
    <link rel="stylesheet" href="../../../admin/css/xadmin.css">
    <link rel="stylesheet" href="../../../admin/css/datatables.css">
    <link rel="stylesheet" href="../../../admin/css/layer.css">
    <link rel="stylesheet" href="../../../admin/css/layui.css">
    <script type="text/javascript" src="../../../admin/js/layui.js"></script>
    <script type="text/javascript" src="../../../admin/lib/layui/layui.js" charset="utf-8"></script>
    <script src="../../../js/jquery-3.5.1.min.js"></script>
    <script src="../../../admin/js/datatables.js"></script>
    <script type="text/javascript" src="../../../admin/js/xadmin.js"></script>
    <style>
        .layui-table td, .layui-table th{
            min-width: 10px;
            max-width: 200px;
        }
        li,ul{
            list-style: none;
            cursor: pointer;
        }
        th{
            white-space: nowrap;
        }
        .fenlei{
            margin-top: 5px;
            /*height: 50px;*/
            width: 100%;
            /*overflow: hidden;*/
            /*background-color: #00C3F5;*/
            margin-bottom: 5px;
        }
        .fenlei ul li{
            display: inline-block;
            height: 30px;
            text-align: center;
            line-height: 30px;
            padding: 10px 20px;
            /*background-color: #00FF00;*/
            border-radius: 5%;
            color: black;
            margin-left: 20px;
        }
    </style>
    <%session.removeAttribute("errorName");%>
</head>
<body>
<div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a><cite>商品列表</cite></a>
            </span>
    <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right"
       onclick="location.reload()" title="刷新">
        <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
    </a>
</div>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <form class="layui-form layui-col-space5" action="/ht/product" method="post" style="margin-top: 20px">
                    <div class="layui-inline layui-show-xs-block" style="margin-left: 15px">
                        <input type="hidden"name="v" value="selectMH">
                        <input type="text" name="productname"  placeholder="请输入商品名" autocomplete="off" class="layui-input">
                    </div>
                    <div class="layui-inline layui-show-xs-block">
                        <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                    </div>
                </form>
                 <div class="fenlei">
                     <ul>
                         <c:forEach items="${goodsSort}" var="d">
                         <li onclick="dianji(${d.sid})">${d.sname}</li>
                         </c:forEach>
                     </ul>
                 </div>
                <div class="layui-card-body">
                    <table class="layui-table layui-form" id="myTable">
                        <thead>
                        <tr>
                            <th>商品图片</th>
                            <th>商品名称</th>
                            <th>商品状态</th>
                            <td>上下架</td>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${goods}" var="p" varStatus="v">
                            <tr>
                                <td >
                                    <img src="../${p.gphoto}" style="width: 50px;height: 50px;">
                                </td>
                            <td>${p.gname}</td>
                            <input type="hidden" id="content" value="${p.gstate}">
                                <c:if test="${p.gstate == 1}" var="op">
                                <td class="td-status">
                                    <span class="layui-btn layui-btn-normal layui-btn-mini">已上架</span></td>
                                <td class="td-manage">
                                    <a onclick="member_stop(this,${p.gid})" href="javascript:;" title="下架">
                                        <i class="layui-icon">&#xe601;</i>
                                    </a>
                                </td>
                                </c:if>
                                <c:if test="${not op}">
                                <td class="td-status">
                                    <span class="layui-btn layui-btn-normal layui-btn-mini layui-btn-disabled">已下架</span>
                                </td>
                                <td class="td-manage">
                                    <a onclick="member_stop(this,${p.gid})" href="javascript:;"  title="上架">
                                        <i class="layui-icon">&#xe62f;</i>
                                    </a>
                                </td>
                                </c:if>
                                <td class="td-manage" style="text-align: center;">
                                    <a title="查看" onclick="xadmin.open('查看','/ht/product?v=productEdit&gid=${p.gid}',900,600)" href="javascript:;">
                                        <i class="layui-icon" style="font-size: 20px">&#xe63c;</i></a>
                                    <a title="编辑" onclick="xadmin.open('编辑','/ht/product?v=productUpdate&gid=${p.gid}',800,600)" href="javascript:;">
                                        <i class="layui-icon" style="font-size: 20px">&#xe642;</i></a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    /*给不同分类添加监听器*/
    let lilist=document.querySelectorAll("li");
    lilist[0].style.backgroundColor="#393d49";
    lilist[0].style.color="white";
    for (let i=0;i<lilist.length;i++){
        lilist[i].addEventListener("click",dianji(sid));
    }
    /*不同分类的点击方法*/
    function dianji(sid){
        lilist.forEach(r=>{
            r.style.backgroundColor="white";
            r.style.color="black";
        })
        let s=event.currentTarget;
        s.style.backgroundColor="#393d49";
        s.style.color="white";
        $.ajax({
            url:"/ht/product",
            type:"post",
            data:{
                "v":"selectProduct",
                sid
            },
            dataType:"json",
            success:(s=>{
                let box=document.querySelector("tbody");
                box.innerHTML="";
                let html=``;
                s.goods.forEach(a=>{
                    html+=`<tr>
                                <td style="text-align: center;">
                                    <img src="../\${a.gphoto}" style="width: 50px;height: 50px;">
                                </td>
                            <td style="text-align: center">\${a.gname}</td>`;
                    if (a.gstate==1){
                        html+=`<td class="td-status">
                                    <span class="layui-btn layui-btn-normal layui-btn-mini">已上架</span></td>
                                <td class="td-manage">
                                    <a onclick="member_stop(this,\${a.gid})" href="javascript:;" title="下架">
                                        <i class="layui-icon">&#xe601;</i>
                                    </a>`;
                    }else {
                        html+=`<td class="td-status">
                                    <span class="layui-btn layui-btn-normal layui-btn-mini layui-btn-disabled">已下架</span></td>
                                <td class="td-manage">
                                    <a onclick="member_stop(this,\${a.gid})" href="javascript:;"  title="上架">
                                        <i class="layui-icon">&#xe62f;</i>
                                    </a>`;
                    }
                    html+=` <td class="td-manage" style="text-align: center;">
                                    <a title="查看" onclick="xadmin.open('查看','/ht/product?v=productEdit&gid=\${a.gid}',900,600)" href="javascript:;">
                                        <i class="layui-icon" style="font-size: 20px">&#xe63c;</i></a>
                                    <a title="编辑" onclick="xadmin.open('编辑','/ht/product?v=productUpdate&gid=\${a.gid}',800,600)" href="javascript:;">
                                        <i class="layui-icon" style="font-size: 20px">&#xe642;</i></a>
                                </td>
                              </tr>`;
                })
                box.innerHTML=html;
            })
        })
    }

    layui.use(['laydate', 'form'],
        function () {
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
            });

            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
            });
        });
     //商品上架-下架
     function member_stop(obj,id){
         var tips = "";
         if($(obj).attr('title')=='下架'){
             tips="确认要下架吗？"
         }else {
             tips="确认要上架吗？";
         }
         layer.confirm(tips,function(index){
             var gstate = -1;//判断是否下架
             if($(obj).attr('title')=='下架'){
                //发异步把商品状态进行更改
                $(obj).attr('title','上架')
                $(obj).find('i').html('&#xe62f;');
                $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已下架');
                layer.msg('已下架!',{icon: 4,time:1000});
                gstate = -1;
                 $.ajax({
                     url:"/ht/product",
                     data:{v:"selectProduct",id,gstate}
                 })
             }else{
                 /*判断该商品是否有详情*/
                $.ajax({
                    url: "/ht/product",
                    type: "post",
                    data: {
                        v: "upOrDown",id
                    },
                    dataType: "json",
                    success: function (resp) {
                        if (resp.flag < 0) {
                            layer.msg('请添加详情再上架!', {icon: 4, time: 1000});
                            gstate = -1;
                        } else {
                            $(obj).attr('title', '下架')
                            $(obj).find('i').html('&#xe601;');
                            $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已上架');
                            layer.msg('已上架!', {icon: 1, time: 1000});
                            gstate = 1;
                        }
                        $.ajax({
                            url:"/ht/product",
                            data:{v:"selectProduct", id,gstate}
                        })
                    }
                })
             }

         });
     }
</script>
<%--每次叉掉子窗口，刷新父窗口--%>
<%--<script>
    layui.use(['form', 'layer'], function() {
        var form = layui.form;
        var layer = layui.layer;

    });
    $(function() {
        //关闭弹窗
        $(document).on('click', '.layui-layer-close ',
            function() {
                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                parent.location.reload();//刷新父页面，注意一定要在关闭当前iframe层之前执行刷新
                parent.layer.close(index); //再执行关闭
            });
    });
</script>--%>
</html>