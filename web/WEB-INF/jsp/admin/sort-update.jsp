<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2023/4/14
  Time: 10:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>类别名称修改</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="../../../admin/css/font.css">
    <link rel="stylesheet" href="../../../admin/css/xadmin.css">
    <script type="text/javascript" src="../../../admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="../../../admin/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]--></head>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row">
        <form class="layui-form" action="">
            <div class="layui-form-item">
                <label for="L_gsname" class="layui-form-label">
                    <span class="x-red">*</span>类别名称</label>
                <div class="layui-input-inline">
                    <input type="text" id="L_gsname" name="gsname" value="${goodsSort.sname}" required="" lay-verify="nikename" autocomplete="off" class="layui-input"></div>
            </div>
            <div style="color: red">
                ${errorName}
            </div>
            <div class="layui-form-item">
                <label for="L_gsname" class="layui-form-label"></label>
                <button class="layui-btn" lay-filter="add" lay-submit="">修改</button>
            </div>
        </form>
    </div>
</div>
</body>
<script>layui.use(['form', 'layer'],
    function() {
        $ = layui.jquery;
        var form = layui.form,
            layer = layui.layer;

        //自定义验证规则
        form.verify({
            nikename: function(value) {
                if (/^\s*$/.test(value)) {
                    return '类别名称不能为空';
                }
            }
        });

        //监听提交
        form.on('submit(add)',
            function(data) {
                console.log(data.field);//当前容器的全部表单字段，名值对形式：{name: value}
                console.log(data.field.gsname)
                $.post({
                    url:"/ht/goodssort",
                    data:{
                        v:"goodsSortUpdates",
                        "gsname":data.field.gsname
                    }
                })
                //发异步，把数据提交给php
                layer.alert("修改成功", {
                        icon: 6,
                        yes:function () {
                            setTimeout(function () {
                                parent.location.reload();//父页面刷新
                            },300)
                        }
                    },
                    function() {
                        // 获得frame索引
                        var index = parent.layer.getFrameIndex(window.name);
                        //关闭当前frame
                        parent.layer.close(index);
                    });
                return false;//阻止表单跳转。如果需要表单跳转，去掉这段即可。

            });

    });</script>

</html>
