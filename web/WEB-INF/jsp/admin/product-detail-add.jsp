<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/4/14
  Time: 14:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>新增商品详情</title>
    <link rel="stylesheet" href="../../../css/ht_details.css">
    <link rel="stylesheet" href="../../../admin/css/font.css">
    <link rel="stylesheet" href="../../../admin/css/xadmin.css">
    <link rel="stylesheet" href="../../../admin/css/datatables.css">
    <link rel="stylesheet" href="../../../admin/css/layer.css">
    <link rel="stylesheet" href="../../../admin/css/layui.css">
    <script type="text/javascript" src="../../../admin/js/layui.js"></script>
    <script type="text/javascript" src="../../../admin/lib/layui/layui.js" charset="utf-8"></script>
    <script src="../../../js/jquery-3.5.1.min.js"></script>
    <script src="../../../admin/js/datatables.js"></script>
    <script type="text/javascript" src="../../../admin/js/xadmin.js"></script>
</head>
<body>
    <c:if test="${not empty addError}" var="noE">
        <h1 style="margin-top: 100px">${addError}</h1>
    </c:if>
    <c:if test="${!noE}">
        <form class="layui-form" method="post" action="/ht/productEdit?v=detailAdd" enctype="multipart/form-data">
            <input type="hidden" name="gid" value="${gid}">
            <input type="hidden" name="sid" value="${sid}">
            <div class="layui-form-item" style="margin-top: 10px;">
                <label class="layui-form-label">价格</label>
                <div class="layui-input-inline">
                    <input type="number" name="dprice" required lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>
            <c:if test="${sid==1}">
                <div class="layui-form-item">
                    <label class="layui-form-label">版本</label>
                    <div class="layui-input-block" style="width: 190px;">
                        <select name="dversion" lay-verify="required" >
                            <option value=""></option>
                            <c:forEach items="${editVersion}" var="ev">
                                <option value="${ev}">${ev}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </c:if>
            <div class="layui-form-item">
                <label class="layui-form-label">颜色</label>
                <div class="layui-input-block" style="width: 190px;">
                    <input type="text" name="dcolor" required lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item" style="display:flex">
                <label class="layui-form-label">图片上传</label>
                <div class="upload">
                    <input type="file" required lay-verify="required" name="gphoto1" accept="image/*">
                    <img class="img1" width="100" height="100" src="../../../img/maingphoto/plus.png">
                </div>
                <div class="upload">
                    <input type="file" required lay-verify="required" name="gphoto2" accept="image/*">
                    <img class="img2" width="100" height="100" src="../../../img/maingphoto/plus.png">
                </div>
                <div class="upload">
                    <input type="file" required lay-verify="required" name="gphoto3" accept="image/*">
                    <img class="img3" width="100" height="100" src="../../../img/maingphoto/plus.png">
                </div>
                <div class="upload">
                    <input type="file" required lay-verify="required" name="gphoto4" accept="image/*">
                    <img class="img4" width="100" height="100" src="../../../img/maingphoto/plus.png">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">新增</button>
                </div>
            </div>
        </form>
    </c:if>
</body>
<script>
    layui.use('form', function(){
        var form = layui.form;
    });

let reader1=new FileReader();
let file1=document.querySelector("input[name='gphoto1']");
file1.addEventListener("change",function () {
let one=event.target.files[0];//获取选中文件
reader1.readAsDataURL(one);//读取文件
});
reader1.onload=function (){//读取后自动执行
document.querySelector(".img1").src=reader1.result;
};

let reader2=new FileReader();
let file2=document.querySelector("input[name='gphoto2']");
file2.addEventListener("change",function () {
let one=event.target.files[0];//获取选中文件
reader2.readAsDataURL(one);//读取文件
});
reader2.onload=function (){//读取后自动执行
document.querySelector(".img2").src=reader2.result;
};

let reader3=new FileReader();
let file3=document.querySelector("input[name='gphoto3']");
file3.addEventListener("change",function () {
let one=event.target.files[0];//获取选中文件
reader3.readAsDataURL(one);//读取文件
});
reader3.onload=function (){//读取后自动执行
document.querySelector(".img3").src=reader3.result;
};

let reader4=new FileReader();
let file4=document.querySelector("input[name='gphoto4']");
file4.addEventListener("change",function () {
let one=event.target.files[0];//获取选中文件
reader4.readAsDataURL(one);//读取文件
});
reader4.onload=function (){//读取后自动执行
document.querySelector(".img4").src=reader4.result;
};
</script>
</html>
