<%--
  Created by IntelliJ IDEA.
  User: asus
  Date: 2023/4/9
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>确认发货(添加快递单号)</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="../../../admin/css/font.css">
        <link rel="stylesheet" href="../../../admin/css/xadmin.css">
        <script type="text/javascript" src="../../../admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="../../../admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
            <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
            <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]--></head>

    <body>
        <div class="layui-fluid">
            <div class="layui-row">
                <form class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">配送物流</label>
                        <div class="layui-input-inline">
                            <select id="shipping" name="shipping" class="valid">
                                <option value="shunfeng">顺丰物流</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">支付方式</label>
                        <div class="layui-input-inline">
                            <select name="contrller">
                                <option>支付宝</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label for="C_number" class="layui-form-label">快递单号</label>
                        <div class="layui-input-inline">
                            <input type="text" id="C_number" name="number" required="" autocomplete="off" class="layui-input">
                        </div>
                        <div class="layui-form-mid layui-word-aux">
                            <span class="x-red">*</span>
                        </div>
                    </div>
                    <input type="hidden" name="oid" value="${oid}">
                    <div class="layui-form-item">
                        <label class="layui-form-label"></label>
                        <button class="layui-btn" lay-filter="add" lay-submit="">确认发货</button>
                    </div>
                </form>
            </div>
        </div>
        <script>
            layui.use(['form', 'layer'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;

                //监听提交
                form.on('submit(add)',
                function(data) {
                    console.log(data);
                    //发异步，把数据提交给后台
                    $.ajax({
                        url:"/ht/order",
                        type:"post",
                        data:{
                            "v":"surePost",
                            "oid":data.field.oid,
                            "number":data.field.number
                        },
                        dataType:"json",
                        success:function (){
                            layer.alert("确认成功", {
                                    icon: 6,
                                    yes:function () {
                                        setTimeout(function () {
                                            parent.location.reload();//父页面刷新
                                        },300)
                                    }
                            },
                            function() {
                                // 获得frame索引
                                var index = parent.layer.getFrameIndex(window.name);
                                //关闭当前frame
                                parent.layer.close(index);
                            });
                        }
                    })
                    return false;
                });
            });
        </script>
    </body>

</html>