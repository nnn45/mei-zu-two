<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/4/13
  Time: 21:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>商品信息修改</title>
    <link rel="stylesheet" href="../../../admin/css/layer.css">
    <link rel="stylesheet" href="../../../admin/css/layui.css">
    <script type="text/javascript" src="../../../admin/js/layui.js"></script>
    <script src="../js/jquery-3.4.1.js"></script>
    <script src="/js/jquery-3.5.1.min.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .upload {
            margin: 5px;
            width: 100px;
            height: 100px;
            border: solid 1px #ccc;
            border-radius: 4px;
            font-size: 60px;
            line-height: 100px;
            text-align: center;
            color: #ddd;
            position: relative;
            float: left;
        }

        .upload input {
            position: absolute;
            width: 100px;
            height: 100px;
            top: 0px;
            left: 0px;
            opacity: 0;
            /*设置纯透明*/
            z-index: 100;
            cursor: pointer;
        }

        .upload #myimg {
            width: 100%;
            height: 100%;
            border: none;
            position: absolute;
            top: 0px;
            left: 0px;
            z-index: 99;
        }
        .upload p{
            font-size: 12px;
            height: 20px;
            line-height: 20px;
            color: #515151;
        }
    </style>
</head>
<body>
<div style="padding: 30px">
    <div style="padding-top:20px;background-color:white;padding-bottom: 20px">
        <form class="layui-form" action="/ht/product" method="post" enctype="multipart/form-data" >
            <input type="hidden" name="v" value="productUpdates">
            <input type="hidden" name="gid" value="${goods.gid}">
            <div class="layui-form-item">
                <label class="layui-form-label">商品名</label>
                <div class="layui-input-block">
                    <input style="width: 190px;" type="text" name="gname" required  lay-verify="required"  autocomplete="off" class="layui-input" value="${goods.gname}">
                </div>
                <div style="color: red">
                    ${errorName}
                </div>
               <input class="abc-input" type="hidden" value="${abc}">
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">商品详情</label>
                <div class="layui-input-inline">
                    <input type="text" name="gdetail" required lay-verify="required" autocomplete="off" class="layui-input" value="${goods.gdetail}">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">商品优惠</label>
                <div class="layui-input-inline">
                    <input type="text" name="gdiscount" autocomplete="off" class="layui-input" value="${goods.gdiscount}">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">商品主图</label>
                <div class="upload">+
                    <input type="file" name="uheadshot" accept="image/*">
                    <input type="hidden" name="photo" value="../${goods.gphoto}">
                    <img id="myimg" class="img1" width="100" height="100" src="../${goods.gphoto}">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">修改</button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
<script>
    let reader=new FileReader();

    let file=document.querySelector("input[name='uheadshot']");
    file.addEventListener("change",function () {
        let one=event.target.files[0];//获取选中文件
        reader.readAsDataURL(one);//读取文件
    });
    reader.onload=function (){//读取后自动执行
        document.querySelector(".img1").src=reader.result;
    };

    let abc = document.querySelector(".abc-input").value;
    if (abc!=null && abc!=""){
        window.parent.location.reload();
        window.parent.layer.closeAll();
    }
</script>

</html>
