<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/4/13
  Time: 8:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>商品详情</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="../../../admin/css/font.css">
    <link rel="stylesheet" href="../../../admin/css/xadmin.css">
    <link rel="stylesheet" href="../../../admin/css/datatables.css">
    <link rel="stylesheet" href="../../../admin/css/layer.css">
    <link rel="stylesheet" href="../../../admin/css/layui.css">
    <script type="text/javascript" src="../../../admin/js/layui.js"></script>
    <script type="text/javascript" src="../../../admin/lib/layui/layui.js" charset="utf-8"></script>
    <script src="../../../js/jquery-3.5.1.min.js"></script>
    <script src="../../../admin/js/datatables.js"></script>
    <script type="text/javascript" src="../../../admin/js/xadmin.js"></script>
    <![endif]-->
    <style>
        tr{
            margin: 5px;
        }
        td{
            padding: 2px 20px;
        }
    </style>
</head>
<body>
    <div>
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right"
           onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
        </a>
        <table class="layui-table" lay-even lay-skin="nob">
            <tr>
                <td>详情编号</td>
                <td>商品名称</td>
                <td>商品价格</td>
                <td>颜色</td>
                <td>版本</td>
                <td>操作</td>
            </tr>
            <c:forEach items="${details}" var="e">
            <tr>
                <td>${e.did}</td>
                <td>${e.gname}</td>
                <td>${e.dprice}</td>
                <td>${e.dcolor}</td>
                <td>${e.dversion}</td>
                <td>
                    <div class="layui-btn-group">
                        <button type="button" class="layui-btn" onclick="xadmin.open('商品详情编辑','/ht/productEdit?v=detailEdit&did=${e.did}')" href="javascript:;">编辑</button>
                    </div>
                </td>
            </tr>
            </c:forEach>
        </table>
        <div>
            <button class="layui-btn layui-btn-primary layui-border-green" style="float: right;width: 120px;margin-right: 25px" onclick="xadmin.open('商品详情新增','/ht/productEdit?gid=${gid}&sid=${sid}')">新增详情</button>
        </div>
    </div>
</body>
</html>
