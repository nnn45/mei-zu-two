<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/4/14
  Time: 14:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>商品详情修改</title>
    <link rel="stylesheet" href="../../../css/ht_details.css"><%--图片上传的样式 必须写在admin/css的引入前面--%>
    <link rel="stylesheet" href="../../../admin/css/font.css">
    <link rel="stylesheet" href="../../../admin/css/xadmin.css">
    <link rel="stylesheet" href="../../../admin/css/datatables.css">
    <link rel="stylesheet" href="../../../admin/css/layer.css">
    <link rel="stylesheet" href="../../../admin/css/layui.css">
    <script type="text/javascript" src="../../../admin/js/layui.js"></script>
    <script type="text/javascript" src="../../../admin/lib/layui/layui.js" charset="utf-8"></script>
    <script src="../../../js/jquery-3.5.1.min.js"></script>
    <script src="../../../admin/js/datatables.js"></script>
    <script type="text/javascript" src="../../../admin/js/xadmin.js"></script>
    <style>
        .layui-input-block select{
            height: 38px;
            width: 100%;
            display: block;
            border: none;
        }
        h3{
            padding: 20px 20px 20px;
            color: #00C3F5;
        }
    </style>
</head>
<body>
<form class="layui-form" action="/ht/productEdit" method="post" enctype="multipart/form-data" >
    <input type="hidden" name="v" value="detailUpdate">
    <input type="hidden" name="did" value="${htDid}">
    <div class="layui-form-item">
        <label class="layui-form-label">商品价格</label>
        <div class="layui-input-inline">
            <input type="number" name="dprice" value="${editDetail.dprice}" required lay-verify="required" autocomplete="off" class="layui-input">
        </div>
    </div>
    <c:if test="${not empty editVersion && editVersion.size()>1}">
    <div class="layui-form-item">
        <label class="layui-form-label">版本</label>
        <div class="layui-input-block" style="width: 190px;">
            <select name="dversion" lay-verify="required">
                <c:forEach items="${editVersion}" var="ev">
                <option value="${ev.dversion}" ${editDetail.dversion==ev.dversion?"selected":""}>${ev.dversion}</option>
                </c:forEach>
            </select>
        </div>
    </div>
    </c:if>
    <div class="layui-form-item">
        <label class="layui-form-label">颜色</label>
        <div class="layui-input-block" style="width: 190px;">
            <input type="text" name="dcolor" value="${editDetail.dcolor}" required lay-verify="required" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">多图片上传</label>
        <c:forEach items="${htPhotos}" var="hp" varStatus="i">
        <div class="upload">+
            <input type="file" name="photo${i.index+1}"  accept="image/*">
            <input type="hidden" name="img${i.index+1}" value="${hp.photo}">
            <img id="myimg" class="img${i.index+1}" width="100" height="100" src="${hp.photo}">
        </div>
        </c:forEach>
    </div>
    <div ><input type="hidden" class="error-or" value="${error}"></div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button  class="layui-btn" lay-submit="" lay-filter="formDemo">修改</button>
        </div>
    </div>
</form>
</body>
<script>
    let reader1=new FileReader();
    let file1=document.querySelector("input[name='photo1']");
    file1.addEventListener("change",function () {
    let one=event.target.files[0];//获取选中文件
    reader1.readAsDataURL(one);//读取文件
    });
    reader1.onload=function (){//读取后自动执行
    document.querySelector(".img1").src=reader1.result;
    };

    let reader2=new FileReader();
    let file2=document.querySelector("input[name='photo2']");
    file2.addEventListener("change",function () {
    let one=event.target.files[0];//获取选中文件
    reader2.readAsDataURL(one);//读取文件
    });
    reader2.onload=function (){//读取后自动执行
    document.querySelector(".img2").src=reader2.result;
    };

    let reader3=new FileReader();
    let file3=document.querySelector("input[name='photo3']");
    file3.addEventListener("change",function () {
    let one=event.target.files[0];//获取选中文件
    reader3.readAsDataURL(one);//读取文件
    });
    reader3.onload=function (){//读取后自动执行
    document.querySelector(".img3").src=reader3.result;
    };

    let reader4=new FileReader();
    let file4=document.querySelector("input[name='photo4']");
    file4.addEventListener("change",function () {
    let one=event.target.files[0];//获取选中文件
    reader4.readAsDataURL(one);//读取文件
    });
    reader4.onload=function (){//读取后自动执行
    document.querySelector(".img4").src=reader4.result;
    };
</script>
</html>
