<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta charset="utf-8">
    <title>地址管理</title>
    <link href="//store.res.meizu.com/layout/img/favicon-90c2e618ff.ico" rel="shortcut icon" />
    <link rel="stylesheet" href="../../css/foot.css"/>
    <link rel="stylesheet" href="../../css/base.css"/>
    <link rel="stylesheet" href="../../css/top.css"/>
    <link rel="stylesheet" href="../../css/address.css"/>
</head>
<style>
    .address-div ul li span a{
        cursor: pointer;
    }
    .address-div ul li span a:first-child:hover{
        color: #008cff;
        border: solid 1px #008cff;
    }
    .nanniu{
        width: 70px;
        height: 20px;
        background-color: #008cff;
        color: white;
        cursor:pointer;
        margin-left: 40px;
    }
</style>
<script>


  onload=function (){
      let li=document.querySelectorAll('.li1');
      for (let i=0;i<li.length;i++) {
          li[i].index = i;
          if (li[i].index<0) {
          } else {
              li[i].onmouseenter = function () {
                  li[i].querySelector('.bssss').style.display = 'block';
              }
              li[i].onmouseleave = function () {
                  li[i].querySelector('.bssss').style.display = 'none';
              }
          }

      }
  }

   /*function Display(d){
       if (d == 0) {

           let basss=document.querySelectorAll(".bssss")
           basss[basss.length-1].style.display = "block";
           let nanniu=document.querySelectorAll(".nanniu")
           nanniu[nanniu.length-1].style.display = "block";
           let as=document.querySelectorAll(".as")
           as[as.length-1].style.display="none";
            onmouseleave=function (){
                basss[basss.length-1].style.display="none";
            }

       }*/

    //删除
    function del(id){
        if (confirm("您确定要删除这条地址记录？")){
            $.ajax({
                url: "/addressServlet",
                type: "post",
                data: {
                    "v":"dodDeleteAddress",
                    id
                },
                dataType:"json",
                success:function (resp){
                    $(".i-yyDZ").html(resp.cout);
                    $(".i-hxDZ").html(10-resp.cout);
                    document.getElementById(id).remove();
                }
            })
        }

    }
    //修改查询单条数据回显到address-form中。
    function update(id){
        let bao=document.querySelectorAll('.a-baocun');
        $.ajax({
            url:"/addressServlet",
            type:"post",
            data:{
                "v":"selectOneAddress",
                id
            },
            dataType:"json",
            success:function (resp){
                     console.log(resp);
                    $("#username").val(resp.aname);
                    $("#userphone").val(resp.dzphone);
                    $("#userdizhi").val(resp.adetail);
                    if (resp.set_default==1){
                        document.querySelector("#default").checked=true;
                    }else{
                        document.querySelector("#default").checked=false;
                    }
                let addressbv=document.querySelector("#addressbv");
                    console.log(addressbv.childNodes[1].childNodes[0].textContent=resp.sheng)
                let sheng=addressbv.childNodes[0].options[addressbv.childNodes[0].selectedIndex];
                let shi=addressbv.childNodes[1].options[addressbv.childNodes[1].selectedIndex];
                let qu=addressbv.childNodes[2].options[addressbv.childNodes[2].selectedIndex];
                let jie=addressbv.childNodes[3].options[addressbv.childNodes[3].selectedIndex];
                console.log("jie::",jie)
                sheng.innerText=resp.sheng;
                shi.innerText=resp.shi;
                qu.innerText=resp.qu;
                jie.innerText=resp.jie;
               /* console.log(addressbv.childNodes)
                console.log(addressbv.childNodes[0].childNodes)
                console.log(addressbv.childNodes[0].childNodes[0].textContent)
                console.log(addressbv.childNodes[0].childNodes[0].textContent)*/
                // addressbv.childNodes[0].childNodes[0].textContent=resp.sheng;
                // addressbv.childNodes[1].childNodes[0].textContent=resp.shi;
                // addressbv.childNodes[2].childNodes[0].textContent=resp.qu;
                // addressbv.childNodes[3].childNodes[0].textContent=resp.jie;
            }
        })
        bao[1].style.zIndex=60000;
    }
    //添加
    function add() {
        myoninput();
        myphone();
        myzhixiashi();
        mydizhi();
        console.log("cqazwsxedcrfvtgn:",${adressNum})
        if (myoninput()==true&&myphone()==true&&myzhixiashi()==true&&mydizhi()==true){
            let name = document.querySelector("#username").value;
            let phone = document.querySelector("#userphone").value;
            let addressbv = document.querySelector("#addressbv");
            let sheng = addressbv.childNodes[0].options[addressbv.childNodes[0].selectedIndex].value;
            let shi = addressbv.childNodes[1].options[addressbv.childNodes[1].selectedIndex].value;
            let qu = addressbv.childNodes[2].options[addressbv.childNodes[2].selectedIndex].value;
            let jie = addressbv.childNodes[3].options[addressbv.childNodes[3].selectedIndex].value;
            let dizhi = document.querySelector("#userdizhi").value;
            let set_default = 0;
            if (document.querySelector("#default").checked==true) {
                set_default = 1;
            }
            $.ajax({
                url: "/addressServlet",
                type: "post",
                data: {
                    "v": "doInsertAddress",
                    name,
                    phone,
                    sheng,
                    shi,
                    qu,
                    jie,
                    dizhi,
                    set_default
                },
                dataType: "json",
                success: function (resp) {

                    //回显数据
                    $(".i-yyDZ").html(resp.cckey);
                    $(".i-hxDZ").html(10-resp.cckey);
                    let ul = document.querySelector("#ul");
                    let abc=JSON.parse(resp.akey)
                    ul.innerHTML = "";
                    let html1 = ``;
                    console.log("ddddddd",resp.dzNUm);
                    if(resp.dzNUm<10){
                        abc.forEach(a => {
                            html1 += `
                     <li class="li1" id="\${a.aid}">
                                    <input class="aid" type="hidden" value="\${a.aid}"/>
                                    <input class="uid" type="hidden" value="\${a.uid}"/>
                                    <span id="ming15">\${a.aname}</span>
                                    <span id="zhi35">\${a.sheng}\${a.shi}\${a.qu}\${a.jie}\${a.adetail}</span>
                                    <span id="haoma25">\${a.dzphone}</span>
                                    <span id="cz10">
										<a id="update" onclick="update(\${a.aid})">修改</a>
										<a id="delete" onclick="del(\${a.aid})">删除</a>
									</span>`;
                            if (a.set_default==1) {
                                html1 += `<span id="morenzhi15">
                                        <i><img src="../../img/dingdan_img/默认值.png" >默认地址</i>
                                    </span>
                                </li>`
                            }else{
                                html1+=`<span id="morenzhi15">
                                        <i class="bssss" style="display: none;"><div class="nanniu"  onclick="xuanzhemoren(\${a.aid})">设为默认</div></i>
                                    </span>
                                </li>`
                            }
                        })
                        ul.innerHTML = html1;
                        let li=document.querySelectorAll('.li1');
                        for (let i=0;i<li.length;i++) {
                            li[i].index = i;
                            if (li[i].index<0) {
                            } else {
                                li[i].onmouseenter = function () {
                                    li[i].querySelector('.bssss').style.display = 'block';
                                }
                                li[i].onmouseleave = function () {
                                    li[i].querySelector('.bssss').style.display = 'none';
                                }
                            }

                        }
                        document.querySelector("#username").value="";
                        document.querySelector("#userphone").value="";
                        let addressbv = document.querySelector("#addressbv");
                        addressbv.childNodes[0].options[addressbv.childNodes[0].selectedIndex].innerHTML="省/直辖市";
                        addressbv.childNodes[1].options[addressbv.childNodes[1].selectedIndex].innerHTML="城市";
                        addressbv.childNodes[2].options[addressbv.childNodes[2].selectedIndex].innerHTML="区/县";
                        addressbv.childNodes[3].options[addressbv.childNodes[3].selectedIndex].innerHTML="乡镇/街道";
                        document.querySelector("#userdizhi").value="";
                        document.querySelector("#default").checked=false;
                    }else{
                        abc.forEach(a => {
                            html1 += `
                     <li class="li1" id="\${a.aid}">
                                    <input class="aid" type="hidden" value="\${a.aid}"/>
                                    <input class="uid" type="hidden" value="\${a.uid}"/>
                                    <span id="ming15">\${a.aname}</span>
                                    <span id="zhi35">\${a.sheng}\${a.shi}\${a.qu}\${a.jie}\${a.adetail}</span>
                                    <span id="haoma25">\${a.dzphone}</span>
                                    <span id="cz10">
										<a id="update" onclick="update(\${a.aid})">修改</a>
										<a id="delete" onclick="del(\${a.aid})">删除</a>
									</span>`;
                            if (a.set_default==1) {
                                html1 += `<span id="morenzhi15">
                                        <i><img src="../../img/dingdan_img/默认值.png" >默认地址</i>
                                    </span>
                                </li>`
                            }else{
                                html1+=`<span id="morenzhi15">
                                        <i class="bssss" style="display: none;"><div class="nanniu"  onclick="xuanzhemoren(\${a.aid})">设为默认</div></i>
                                    </span>
                                </li>`
                            }
                        })
                        ul.innerHTML = html1;
                        let li=document.querySelectorAll('.li1');
                        for (let i=0;i<li.length;i++) {
                            li[i].index = i;
                            if (li[i].index<0) {
                            } else {
                                li[i].onmouseenter = function () {
                                    li[i].querySelector('.bssss').style.display = 'block';
                                }
                                li[i].onmouseleave = function () {
                                    li[i].querySelector('.bssss').style.display = 'none';
                                }
                            }

                        }
                        let zcc=document.querySelector(".zcc");
                        zcc.style.display="block";
                        zcc.innerHTML=`<div class="tishi">
				<i class="i-zcc">×</i>
				<div class="sx-zcc">您添加的地址数量已超过上限，请管理您的地址！</div>
				<button class="zcc-btn">确定</button>
			</div>`;
                        let cha=document.querySelector(".i-zcc");
                cha.addEventListener("click",function (){
                    document.querySelector(".zcc").style.display="none";
                });
                document.querySelector(".zcc-btn").addEventListener("click",function (){
                    document.querySelector(".zcc").style.display="none";
                })

                    }
                }


            })
        }


    }
    //修改
    function xiugai() {
        myoninput();
        myphone();
        myzhixiashi();
        mydizhi();
        if (myoninput() == true && myphone() == true && myzhixiashi() == true && mydizhi() == true) {
        let bao = document.querySelectorAll('.a-baocun');
        bao[1].style.zIndex = -1;
        let name = document.querySelector("#username").value;
        let phone = document.querySelector("#userphone").value;
        let addressbv = document.querySelector("#addressbv");
        let sheng = addressbv.childNodes[0].options[addressbv.childNodes[0].selectedIndex].value;
        let shi = addressbv.childNodes[1].options[addressbv.childNodes[1].selectedIndex].value;
        let qu = addressbv.childNodes[2].options[addressbv.childNodes[2].selectedIndex].value;
        let jie = addressbv.childNodes[3].options[addressbv.childNodes[3].selectedIndex].value;
        let dizhi = document.querySelector("#userdizhi").value;
        let set_default = 0;
        if (document.querySelector("#default").checked == true) {
            set_default = 1;
        }
        $.ajax({
            url: "/addressServlet",
            type: "post",
            data: {
                "v": "doUpdateAddress",
                name,
                phone,
                sheng,
                shi,
                qu,
                jie,
                dizhi,
                set_default
            },
            dataType: "json",
            success: function (resp) {
                let ul = document.querySelector("#ul");
                ul.innerHTML = "";
                let html1 = ``;
                resp.forEach(a => {
                    html1 += `
                     <li class="li1" id="\${a.aid}">
                                    <input class="aid" type="hidden" value="\${a.aid}"/>
                                    <input class="uid" type="hidden" value="\${a.uid}"/>
                                    <span id="ming15">\${a.aname}</span>
                                    <span id="zhi35">\${a.sheng}\${a.shi}\${a.qu}\${a.jie}\${a.adetail}</span>
                                    <span id="haoma25">\${a.dzphone}</span>
                                    <span id="cz10">
										<a id="update" onclick="update(\${a.aid})">修改</a>
										<a id="delete" onclick="del(\${a.aid})">删除</a>
									</span>`;
                    if (a.set_default == 1) {
                        html1 += `<span id="morenzhi15">
                                        <i><img src="../../img/dingdan_img/默认值.png" >默认地址</i>
                                    </span>
                                </li>`
                    } else {
                        html1 += `<span id="morenzhi15">
                                        <i class="bssss" style="display: none;"><div class="nanniu"  onclick="xuanzhemoren(\${a.aid})">设为默认</div></i>
                                    </span>
                                </li>`
                    }
                })
                ul.innerHTML = html1;
                let li = document.querySelectorAll('.li1');
                for (let i = 0; i < li.length; i++) {
                    li[i].index = i;
                    if (li[i].index<0) {
                    } else {
                        li[i].onmouseenter = function () {
                            li[i].querySelector('.bssss').style.display = 'block';
                        }
                        li[i].onmouseleave = function () {
                            li[i].querySelector('.bssss').style.display = 'none';
                        }
                    }

                }
                document.querySelector("#username").value="";
                document.querySelector("#userphone").value="";
                let addressbv = document.querySelector("#addressbv");
                addressbv.childNodes[0].options[addressbv.childNodes[0].selectedIndex].innerHTML="省/直辖市";
                addressbv.childNodes[1].options[addressbv.childNodes[1].selectedIndex].innerHTML="城市";
                addressbv.childNodes[2].options[addressbv.childNodes[2].selectedIndex].innerHTML="区/县";
                addressbv.childNodes[3].options[addressbv.childNodes[3].selectedIndex].innerHTML="乡镇/街道";
                document.querySelector("#userdizhi").value="";
                document.querySelector("#default").checked=false;
            }

        })
    }
    }
   //设置默认值点击事件
    function xuanzhemoren(id){
        $.ajax({
            url:"/addressServlet",
            type:"post",
            data:{
                "v":"updateMorenzhi",
                id
            },
            dataType:"json",
            success:function (resp){
                let ul = document.querySelector("#ul");
                ul.innerHTML = "";
                let html1 = ``;
                resp.forEach(a => {
                    html1 += `
                     <li class="li1" id="\${a.aid}">
                                    <input class="aid" type="hidden" value="\${a.aid}"/>
                                    <input class="uid" type="hidden" value="\${a.uid}"/>
                                    <span id="ming15">\${a.aname}</span>
                                    <span id="zhi35">\${a.sheng}\${a.shi}\${a.qu}\${a.jie}\${a.adetail}</span>
                                    <span id="haoma25">\${a.dzphone}</span>
                                    <span id="cz10">
										<a id="update" onclick="update(\${a.aid})">修改</a>
										<a id="delete" onclick="del(\${a.aid})">删除</a>
									</span>`;
                    if (a.set_default==1) {
                        html1 += `<span id="morenzhi15">
                                        <i><img src="../../img/dingdan_img/默认值.png" >默认地址</i>
                                    </span>
                                </li>`;
                    }else{
                        html1+=`<span id="morenzhi15">
                                        <i class="bssss" style="display: none;"><div class="nanniu"  onclick="xuanzhemoren(\${a.aid})">设为默认</div></i>
                                    </span>
                                </li>`;
                    }
                })
                ul.innerHTML = html1;
                let li=document.querySelectorAll('.li1');
                for (let i=0;i<li.length;i++) {
                    li[i].index = i;
                    if (li[i].index<0) {
                    } else {
                        li[i].onmouseenter = function () {
                            li[i].querySelector('.bssss').style.display = 'block';
                        }
                        li[i].onmouseleave = function () {
                            li[i].querySelector('.bssss').style.display = 'none';
                        }
                    }

                }

            }


        })
    }
    //验证
    //判断名字方法
    function myoninput(){
        let name=document.querySelector("#username").value;
        let nmerror=document.querySelector("#nm");
        let nul=/^\s*$/;
        let mingzi=/^(?:[\u4e00-\u9fa5]+)(?:●[\u4e00-\u9fa5]+)*$|^[a-zA-Z0-9]+\s?[\.·\-()a-zA-Z]*[a-zA-Z]+$/;
        //判断名字
        if (!nul.test(name)){
            if (mingzi.test(name)){
                nmerror.style.display="none";
                return true;
            }else {
                nmerror.style.display="block";
                nmerror.innerHTML="不能存在特殊字符跟空格";
                return false;
            }
        }else {
            nmerror.style.display="block";
            nmerror.innerHTML="必填";
            return false;
        }
    }
    //判断手机号码方法
    function myphone(){
        let phone=document.querySelector("#userphone").value;
        let pherror=document.querySelector("#ph");
        let nul=/^\s*$/;
        let shouji=/^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\d{8}$/;
        //判断手机号码
        if (!nul.test(phone)){
            if (shouji.test(phone)){
                pherror.style.display="none";
                return true;
            }else {
                pherror.style.display="block";
                pherror.innerHTML="手机号码格式错误";
                return false;
            }
        }else {
            pherror.style.display="block";
            pherror.innerHTML="必填";
            return false;
        }
    }

    //判断省市区/直辖市
    function myzhixiashi(){
        let addressbv = document.querySelector("#addressbv");
        let sel=addressbv.childNodes[0];
       // console.log(sel)
        sel.onchange=function (){
           // console.log("selectzhing:>>"+event.target.value);
            let sheng=event.target.value;
            let ssqjerror=document.querySelector("#ssqj");
            /*let nul=/^\s*$/;*/
            //判断省市区/直辖市
            if (sheng!='省/直辖市'){
                ssqjerror.style.display="none";
                return true;
            }else {
                ssqjerror.style.display="block";
                ssqjerror.innerHTML="请选择所在的省/市/区/直辖市";
                return false;
            }
        }
        let sheng = addressbv.childNodes[0].options[addressbv.childNodes[0].selectedIndex].value;
        let ssqjerror=document.querySelector("#ssqj");
        /*let nul=/^\s*$/;*/
        //判断省市区/直辖市
        if (sheng!='省/直辖市'){
            ssqjerror.style.display="none";
            return true;
        }else {
            ssqjerror.style.display="block";
            ssqjerror.innerHTML="请选择所在的省/市/区/直辖市";
            return false;
        }
    }
    //判断地址详情方法
    function mydizhi(){
        let dizhi=document.querySelector("#userdizhi").value;
        let dzerror=document.querySelector("#xqDZ");
        let nul=/^\s*$/;
        //判断详情地址
        if (!nul.test(dizhi)){
            dzerror.style.display="none";
            return true;
        }else {
            dzerror.style.display="block";
            dzerror.innerHTML="请详细填写地址";
            return false;
        }
    }
</script>
<style>
    span#nm{
        position: absolute;
        left: 102px;
        color: red;
    }
    span#ph{
        position: absolute;
        left: 564px;
        color: red;
    }
    span#ssqj{
        position: absolute;
        left: 95px;
        top: 28px;
        color: red;
    }
    span#xqDZ{
        position: absolute;
        left: 95px;
        color: red;
    }
    input::-webkit-input-placeholder {
        /* WebKit browsers，webkit内核浏览器 */
        color: #c6c6c6;
        font-size: 12px;
    }
</style>
<body>

<!-- ----------------头部--------------------------- -->
<c:import url="top.jsp"></c:import>
<div class="zcc" style="display: none"></div>
<hr>
<div id="body-box">
    <div class="body-one">
        <a href="/index">首页 >&nbsp;</a>
        <a href="#">我的商城 >&nbsp;</a>
        <a href="#">地址管理</a>
    </div>
    <div class="body-two">
        <!--- 订单中心左 -->
        <div class="body-left">
            <a class="a-one">
                <img src="../../img/dingdan_img/订单中心.png"/>&nbsp;&nbsp;订单中心
            </a>
            <a href="/order?v=show" class="a-two">我的订单</a>
            <a href="#" class="a-two">我的回购单</a>

            <a class="a-one">
                <img src="../../img/dingdan_img/个人中心.png"/>&nbsp;&nbsp;个人中心
            </a>
            <a href="/addressServlet" class="a-two" style="color: #008cff">地址管理</a>
            <a href="#" class="a-two">我的收藏</a>
            <a href="#" class="a-two">消息提醒</a>
            <a href="#" class="a-two">建议反馈</a>

            <a class="a-one">
                <img src="../../img/dingdan_img/资产中心.png"/>&nbsp;&nbsp;资产中心
            </a>
            <a href="#" class="a-two">我的优惠券</a>
            <a href="#" class="a-two">我的红包</a>
            <a href="#" class="a-two">我的回购金</a>

            <a href="#" class="a-one">
                <img src="../../img/dingdan_img/服务中心.png"/>&nbsp;&nbsp;服务中心
            </a>
            <a href="#" class="a-two">退款/退换货跟踪</a>
            <a href="#" class="a-two">以旧换新</a>
        </div>
        <!-- ===================================================================================================--->
        <!--- 订单中心右 -->
        <div class="body-right">
            <div class="address-div">
                <div class="div-addres-XG">
                    <span id="span-one">修改收货地址</span>
                    <span id="span-two">
									(您目前已有地址
									<i class="i-yyDZ">${count.count}</i>
									个,最多还可添加
									<i class="i-hxDZ">${10-count.count}</i>
									个)
                    </span>
                </div>
                <form class="address-form">
                    <!-- 修改地址时要用的地址id-->
                    <input class="addressId" type="hidden" value="123456789"/>
                    <div class="div-address-centre">
                        <div id="div-name-phone">
                            <div id="name">
                                <span>收货人姓名<i style="color: #ff0101;">*</i></span>
                                <input type="text" id="username"  placeholder="长度不超过15个字" maxlength="15" oninput="myoninput()"/>
                                <span id="nm" style="display:none;"></span>
                            </div>
                            <div id="phone">
                                <span>收获人手机号<i style="color: #ff0101;">*</i></span>
                                <input type="text" id="userphone" placeholder="请输入11位的手机号" maxlength="11" oninput="myphone()"/>
                                <span id="ph" style="display: none;"></span>
                            </div>
                        </div>
                        <%----------------------省市区乡镇--------------------------------%>
                        <div class="dome-main">
                            <span>收货人地址<i style="color: red;">*</i></span>
                            <div id="addressbv" class="city-picker-select"></div>
                            <span id="ssqj" style="display: none"></span>
                        </div>
                        <%---------------------------------------省市区乡镇---------------------------------%>
                        <div id="dizhi">
                            <span>详细地址<i style="color: red;">*&nbsp;&nbsp;</i></span>
                            <input type="text" id="userdizhi" placeholder="请输入不少于4不超过50个字的详细地址，例如：路名，门牌号" minlength="4" maxlength="50" oninput="mydizhi()"/>
                            <span id="xqDZ" style="display: none;"></span>
                        </div>
                        <div id="opreat" style="position: relative">
                            <label for="default">
                                <input id="default" type="checkbox"/>
                                设为默认
                            </label>
                            <a class="a-baocun" style="z-index: 5;" onclick="add()">保存</a>
                            <a class="a-baocun" style="position: absolute;top: 0;right: 0;z-index:-1;" onclick="xiugai()">修改</a>
                        </div>
                    </div>
                </form>
                <div class="have-address">
                    <div>已有地址</div>
                </div>
                <div class="listHead">
                    <span id="name15">收货人姓名</span>
                    <span id="address35">收货人地址</span>
                    <span id="phone25">收货人手机号</span>
                    <span id="caozuo10">操作</span>
                </div>
                <ul id="ul">
                    <c:forEach items="${add}" var="a">
<%--                        onmouseenter="Display(${a.set_default})"--%>
                                <li class="li1" id="${a.aid}" >
                                    <input class="aid" type="hidden" value="${a.aid}"/>
                                    <input class="uid" type="hidden" value="${a.uid}"/>
                                    <span id="ming15">${a.aname}</span>
                                    <span id="zhi35">${a.sheng}${a.shi}${a.qu}${a.jie}${a.adetail}</span>
                                    <span id="haoma25">${a.dzphone}</span>
                                    <span id="cz10">
										<a id="update" onclick="update(${a.aid})">修改</a>
										<a id="delete" onclick="del(${a.aid})">删除</a>
									</span>
                                    <c:if test="${a.set_default==1}">
                                        <span id="morenzhi15">
                                            <i class="as"><img src="../../img/dingdan_img/默认值.png" >默认地址</i>
                                    </span>
                                    </c:if>
                                    <c:if test="${a.set_default==0}">
                                    <span id="morenzhi15" >
                                        <i class="bssss" style="display: none;"><div class="nanniu"  onclick="xuanzhemoren(${a.aid})">设为默认</div></i>
                                    </span>
                                    </c:if>
                                </li>

                    </c:forEach>
                   <%-- <li>
                        <input class="addressId" type="hidden" value="123456"/>
                        <input class="isOId" type="hidden" value="0"/>
                        <span id="ming15">唐浩</span>
                        <span id="zhi35">湖南省长沙市天心区赤岭路街道学府路101</span>
                        <span id="haoma25">152****6399</span>
                        <span id="cz10">
										<a id="update" href="">修改</a>
										<a id="delete" href="">删除</a>
									</span>
                        <span id="morenzhi15">
										<img src="../../img/dingdan_img/默认值.png" >
										<i>默认地址</i>
									</span>
                    </li>--%>
                </ul>

            </div>
        </div>
    </div>
</div>

<!----------------------------底部--------------------------------------------------->
<c:import url="foot.jsp"></c:import>
<script src="/js/jquery-3.5.1.min.js"></script>
<script src="/js/streets-data.min.js"></script>
<script src="/js/cityPicker-2.0.5.js"></script>
<script>
    $(function () {
        //原生城市-联动
        var select = $('.city-picker-select').cityPicker({
            dataJson: cityData,
            renderMode: false,
            storage: false,
            autoSelected: true,
            level: 4,
            onChoiceEnd: function() {
                console.log('选中的值是：',this.values)
            }
        });
        // 设置城市
        //select.setCityVal(', , , ');
    });
</script>
</body>
</html>