<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
	<head>
		<meta charset="utf-8">
		<title>首页</title>
		<link href="//store.res.meizu.com/layout/img/favicon-90c2e618ff.ico" rel="shortcut icon" />
		<link rel="stylesheet" href="../../css/base.css" />
		<link rel="stylesheet" href="../../css/foot.css" />
		<link rel="stylesheet" href="../../css/meizu.css" />
		<link rel="stylesheet" href="../../css/top.css">
		<script src="../../js/jquery-3.5.1.min.js"></script>
		<script>history.go(1);</script>
	</head>
	<body>

		<div class="box">
			<c:import url="top.jsp"></c:import>
		   <div class="imgbox" style="cursor: pointer">
			<ul class="imglist-lunbo">
				<li><img src="../../img/轮播图片/1.jpg" id="img" /></li>
				<li><img src="../../img/轮播图片/2.jpg" id="img" /></li>
				<li><img src="../../img/轮播图片/3.jpg" id="img" /></li>
				<li><img src="../../img/轮播图片/4.jpg" id="img" /></li>
				<li><img src="../../img/轮播图片/5.jpg" id="img" /></li>
				<li><img src="../../img/轮播图片/6.jpg" id="img" /></li>
				<li><img src="../../img/轮播图片/7.jpg" id="img" /></li>
				<li><img src="../../img/轮播图片/8.jpg" id="img" /></li>
				<li><img src="../../img/轮播图片/1.jpg" id="img" /></li>
			</ul>

			<ul class="bottom-little">
				<li id="yuan"></li>
				<li id="yuan"></li>
				<li id="yuan"></li>
				<li id="yuan"></li>
				<li id="yuan"></li>
				<li id="yuan"></li>
				<li id="yuan"></li>
				<li id="yuan"></li>
			</ul>
		  </div>
		</div>
		<script>
			/*轮播图*/
			var esico = document.querySelectorAll("#yuan");
			var eicolist = document.querySelector('.bottom-little');
			var eimglist = document.querySelector('.imglist-lunbo');
			var left = 0;
			var timer;
			run();

			function run() {
				if (left <= -20480) {
					left = 0;
				}
				var m = Math.floor(-left / 2560);
				eimglist.style.marginLeft = left + 'px';
				var n = (left % 2560 == 0) ? n = 6000 : n = 1;
				left -= 20;
				icochange(m);
				timer = setTimeout(run, n);
			}
			function icochange(m) {
				for (let index = 0; index < esico.length; index++) {
					esico[index].style.backgroundColor = '';
				}
				if (m < esico.length) {
					esico[m].style.backgroundColor = 'red';
				}
			}
			function imgchange(n) {
				let lt = -(n * 2560)
				imglist.style.marginLeft = lt + 'px';
				left = lt;
			}
			eicolist.onclick = function() {
				var tg = event.target;
				let ico = tg.innerHTML - 1;
				imgchange(ico);
				icochange(ico);
			}

		</script>
		<div class="boxTwo">
			<ul class="photo clarefix">
				<li><img src="../../img/boxTwo/1.jpg" />
					<p>1 元超前订</p>
					<p>享 36 个月超长质保</p>
				</li>
				<li><img src="../../img/boxTwo/2.png" />
					<p>全民共创</p>
					<p>热爱无界 我有一个朋友</p>
				</li>
				<li><img src="../../img/boxTwo/3.png" />
					<p>渴物众筹</p>
					<p>好物频道</p>
				</li>
				<li><img src="../../img/boxTwo/4.jpg" />
					<p>以旧换新</p>
					<p>限时补贴</p>
				</li>
			</ul>

		</div>
		<c:forEach items="${goodsSort}" var="b" varStatus="i">
			<h3>${b.sname}</h3>
			<div class="boxThree">
				<img src="${b.sphoto}" />
			</div>

				<c:if test="${i.index==0}">
		      <div class="boxFour">
			      <div class="list clarefix">
						<c:forEach items="${b.list}" var="c" varStatus="j">
							<c:if test="${j.index==0}"><div class="listrow1"></c:if>
							<c:if test="${j.index==0||j.index==1}">
								<a href="/detail?did=${c.did}" style="cursor: pointer" target="_blank">
						           <span class="product-font">
							       <span class="goods-name">${c.gname}</span>
							       <span class="goods-desc">限时6期免息</span>
							       <span class="goods-price">
								      <i>￥</i>
								      ${c.dprice}
								      <em>起</em>
							       </span>
						           </span>
									<img src="${c.gphoto}" />
									<span class="product-red">免息</span>
									</a>
							</c:if>
						<c:if test="${j.index==1}"></div><div class="listrow2"></c:if>
							<c:if test="${j.index>1}">

									<a href="/detail?did=${c.did}" style="cursor: pointer" target="_blank">
										<img src="${c.gphoto}" />
										<span class="product-font">
							<span class="goods-name">${c.gname}</span>
							<span class="goods-price">
								<i>￥</i>
								${c.dprice}
								<em>起</em>
							</span>
						</span>
									</a>

							</c:if>
						</c:forEach>
						</div>
					</div>
			  </div>
					</c:if>
					<c:if test="${i.index>0}">
				   <div class="boxSix">
						<ul class="clarefix">

						<c:forEach items="${b.list}" var="c">
							<li>
							<a href="/detail?did=${c.did}" style="cursor: pointer" target="_blank">
								<img src="${c.gphoto}" />
								<span class="product-font">
							<span class="goods-name">${c.gname}</span>
							<span class="goods-price">
								<i>￥</i>
								${c.dprice}
								<em>起</em>
							</span>
						   </span>
							</a>
							</li>
						</c:forEach>
						</ul>
				   </div>
						</c:if>
					</c:forEach>


		<h3>社区热帖</h3>
		<div class="boxNine">
			<ul class="clarefix">
				<li><a>
						<div class="photo1"><img src="../../img/社区热帖/1.png" /></div>
						<div class="photo2-font"><img src="../../img/社区热帖/1.jpg" /><span
								class="comment-username">醉刘的摄影狮</span></div>
						<p>魅友大会 2022</p><span class="comment-tip">大会全程回顾</span>
					</a></li>
				<li><a>
						<div class="photo1"><img src="../../img/社区热帖/2.png" /></div>
						<div class="photo2-font"><img src="../../img/社区热帖/2.jpg" /><span class="comment-username">一楽拉面</span>
						</div>
						<p>魅友的 19 个故事</p><span class="comment-tip">那些数不清的无界人生和热爱往事</span>
					</a></li>
				<li><a>
						<div class="photo1"><img src="../../img/社区热帖/3.png" /></div>
						<div class="photo2-font"><img src="../../img/社区热帖/3.jpg" /><span class="comment-username">萬_金龍</span>
						</div>
						<p>魅友大会 2022</p><span class="comment-tip">大会全程回顾</span>
					</a></li>
				<li><a>
						<div class="photo1"><img src="../../img/社区热帖/4.png" /></div>
						<div class="photo2-font"><img src="../../img/社区热帖/4.jpg" /><span class="comment-username">萬_金龍</span>
						</div>
						<p>魅友大会 2022</p><span class="comment-tip">大会全程回顾</span>
					</a></li>
				<li><a>
						<div class="photo1"><img src="../../img/社区热帖/5.png" /></div>
						<div class="photo2-font"><img src="#" /><span class="comment-username">萬_金龍</span></div>
						<p>魅友大会 2022</p><span class="comment-tip">大会全程回顾</span>
					</a></li>
				<li><a>
						<div class="photo1"><img src="../../img/社区热帖/6.png" /></div>
						<div class="photo2-font"><img src="#" /><span class="comment-username">萬_金龍</span></div>
						<p>魅友大会 2022</p><span class="comment-tip">大会全程回顾</span>
					</a></li>
				<li><a>
						<div class="photo1"><img src="../../img/社区热帖/7.png" /></div>
						<div class="photo2-font"><img src="#" /><span class="comment-username">萬_金龍</span></div>
						<p>魅友大会 2022</p><span class="comment-tip">大会全程回顾</span>
					</a></li>
				<li><a>
						<div class="photo1"><img src="../../img/社区热帖/8.png" /></div>
						<div class="photo2-font"><img src="#" /><span class="comment-username">萬_金龍</span></div>
						<p>魅友大会 2022</p><span class="comment-tip">大会全程回顾</span>
					</a></li>
			</ul>

		</div>
		<h3>Flyme</h3>
		<div class="boxTen">
			<ul class="clarefix">
				<li class="first">
					<div class="goods-box"><a>
							<div class="first-comment"><img src="../../img/Flyme/1.png" /></div>
						</a></div>
				</li>
				<li>
					<div class="goods-box"><a>
							<div class="comment"><img src="../../img/Flyme/2.jpg" /></div>
							<p>Flyme 隐私安全白皮书</p>
						</a></div>
				</li>
				<li class="last">
					<div class="goods-box"><a>
							<div class="comment"><img src="../../img/Flyme/3.jpg" /></div>
							<p>跨越使用小障碍</p>
						</a></div>
				</li>
				<li>
					<div class="goods-box"><a>
							<div class="comment"><img src="../../img/Flyme/4.png" /></div>
							<p>全新导入滤镜</p>
						</a></div>
				</li>
				<li>
					<div class="goods-box"><a>
							<div class="comment"><img src="../../img/Flyme/5.png" /></div>
							<p>全面提升续航能力</p>
						</a></div>
				</li>
				<li>
					<div class="goods-box"><a>
							<div class="comment"><img src="../../img/Flyme/6.jpg" /></div>
							<p>不得不知的小功能</p>
						</a></div>
				</li>
				<li>
					<div class="goods-box"><a>
							<div class="comment"><img src="../../img/Flyme/7.jpg" /></div>
							<p>快速挽救手机数据</p>
						</a></div>
				</li>
			</ul>
		</div>

		<!-- <h3>精彩视频</h3>
		<div class="boxEleven"></div>-->
		<c:import url="foot.jsp"></c:import>


	</body>

</html>
