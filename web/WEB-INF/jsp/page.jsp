<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 循环总页数 -->
<c:choose>
    <%-- 计算页码列表的开始值begin和结束值end
         规则如下：
         -------------------------------------------------------
         1、 总页数不足5页 		begin=1 end=最大页
         -------------------------------------------------------
         2、 总页数大于5页		begin=当前页-2   end=当前页+2  （保证前后都有两页，5条）
             2-1、begin<1   		begin=1 	   end=5
             2-2、begin<4    		begin=1  (前面的连贯出现)
             2-3、end>最大页   		begin=最大页-5  end=最大页
             2-4、end>最大页-4 	end=最大页  （后面的连贯出现）
         -------------------------------------------------------
         3、begin>2
                 前面出现第1页和第2页  以及...
         -------------------------------------------------------
         4、end<最大页-2
                 后面出现...以及倒数2页
         -------------------------------------------------------
    --%>

<%--1、 总页数不足5页 begin=1 end=最大页 --%>
    <c:when test="${pager.pageCount<=5 }">
        <c:set var="begin" value="1"/>
        <c:set var="end" value="${pager.pageCount }"/>
    </c:when>
    <c:otherwise>
<%--begin=当前页-2   end=当前页+2  （保证前后都有两页，5条） --%>
        <c:set var="begin" value="${pager.pageNo-2 }"/>
        <c:set var="end" value="${pager.pageNo + 2}"/>
        <%--2-1、begin<1   		begin=1 end=5 --%>
        <c:if test="${begin < 1 }">
            <c:set var="begin" value="1"/>
            <c:set var="end" value="5"/>
        </c:if>
        <%--2-2、begin<4    		begin=1  (前面的连贯出现) --%>
        <c:if test="${begin <= 4 }">
            <c:set var="begin" value="1"/>
        </c:if>
        <%--2-3、end>最大页   		begin=最大页-5  end=最大页 --%>
        <c:if test="${end > pager.pageCount }">
            <c:set var="begin" value="${pager.pageCount-5 }"/>
            <c:set var="end" value="${pager.pageCount }"/>
        </c:if>
        <%--2-4、end>最大页-4 		end=最大页  （后面的连贯出现）   --%>
        <c:if test="${end > pager.pageCount-4 }">
            <c:set var="end" value="${pager.pageCount }"/>
        </c:if>
    </c:otherwise>
</c:choose>
<%--3、begin>2	  前面出现第1页和第2页  以及... --%>
<c:if test="${begin>2 }">
    <a href='${searchParams}no=1&size=${pager.pageSize}'>1</a>
    <a href='${searchParams}no=2&size=${pager.pageSize}'>2</a>
    ...
</c:if>

<%--从开始begin  循环到结束end --%>
<c:forEach var="x" begin="${begin }" end="${end }" step="1">
    <a href='${searchParams}no=${x }&size=${pager.pageSize}' ${pager.pageNo==x?"class='current'":"" }>
        ${x }
        <input type="hidden" name="no" value="${x}"/>
    </a>
</c:forEach>
<%--从开始begin  循环到结束end --%>

<%--4、end<最大页-2 	后面出现...以及倒数2页 --%>
<c:if test="${end<pager.pageCount-2 }" var="b">
    ...
    <a href='${searchParams}no=${ pager.pageCount-1}&size=${pager.pageSize}'>${ pager.pageCount-1}</a>
    <a href='${searchParams}no=${ pager.pageCount}&size=${pager.pageSize}'>${ pager.pageCount}</a>
</c:if>
