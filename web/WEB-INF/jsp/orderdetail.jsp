<%--
  Created by IntelliJ IDEA.
  User: Anny
  Date: 2023/3/29
  Time: 19:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>订单详情-魅族商城</title>
    <link rel="shortcut icon" href="../../img/订单确认/favicon-90c2e618ff.ico" />
    <link rel="stylesheet" href="../../css/top.css"/>
    <link rel="stylesheet" href="../../css/base.css"/>
    <link rel="stylesheet" href="../../css/foot.css"/>
<%--    <link rel="stylesheet" href="../../css/dingdan.css">--%>
    <script type="text/javascript">
        //清除CSS缓存
        document.write('<link rel="stylesheet" type="text/css" href="../../css/orderdetail.css?v=' + Math.random() + '"/>');
    </script>
    <script src="../../js/jquery-3.5.1.min.js"></script>
    <script>
       var total=0;
       function sure(oid){
           location.href="/order?v=sure&oid="+oid;
       }
       function qvXiao(oid) {
           location.href="/order?v=cancel&oid="+oid;
       }
    </script>
</head>
<body>
    <c:import url="top.jsp"></c:import>
    <div id="body-box">
        <div class="body-one">
            <a href="http://localhost:8080/allServlet?v=meiZu">首页 >&nbsp;</a>
            <a href="/index">我的商城 >&nbsp;</a>
            <a href="/order?v=show">我的订单></a>
            <a style="color: #00c3f5">订单${order.oid}</a>
        </div>
        <div class="body-two">
            <!--- 订单中心左 ------------------------------------------------------------>
            <div class="body-left">
                <a class="a-one">
                    <img src="../../img/dingdan_img/订单中心.png"/>&nbsp;&nbsp;订单中心
                </a>
                <a href="/order?v=show" class="a-two" style="color: #008cff">我的订单</a>
                <a class="a-two">我的回购单</a>

                <a class="a-one">
                    <img src="../../img/dingdan_img/个人中心.png"/>&nbsp;&nbsp;个人中心
                </a>
                <a href="/addressServlet" class="a-two">地址管理</a>
                <a class="a-two">我的收藏</a>
                <a class="a-two">消息提醒</a>
                <a class="a-two">建议反馈</a>

                <a class="a-one">
                    <img src="../../img/dingdan_img/资产中心.png"/>&nbsp;&nbsp;资产中心
                </a>
                <a class="a-two">我的优惠券</a>
                <a class="a-two">我的红包</a>
                <a class="a-two">我的回购金</a>

                <a class="a-one">
                    <img src="../../img/dingdan_img/服务中心.png"/>&nbsp;&nbsp;服务中心
                </a>
                <a class="a-two">退款/退换货跟踪</a>
                <a class="a-two">以旧换新</a>
            </div>
            <div class="bigbox">
                <!--- 订单中心右第一部分 ------------------------->
                <div id="right-top">
                    <a>订单详情</a>
                </div>
                <div class="order-state">
                    订单状态：<span class="os"></span>
                    <c:if test="${not empty order.cnum}">
                        <div>快递单号：<span>${order.cnum}</span></div>
                    </c:if>
                    <div class="buttons">
                        <button class="sure" onclick="sure(${order.oid})">立即付款</button>
                        <button class="cancel" onclick="qvXiao(${order.oid})">取消订单</button>
                    </div>
                </div>
                <div class="title cleanfix">&nbsp;&nbsp;&nbsp;&nbsp;订单号：<span style="color: #666">${order.oid}</span></div>
                <table class="righttablebox">
                    <tr>
                        <td class="product" colspan="2">供应商：魅族</td>
                        <td >单价</td>
                        <td >数量</td>
                        <td >优惠</td>
                        <td >小计</td>
                        <td >售后</td>
                    </tr>
                    <c:forEach items="${ods}" var="ods">
                    <tr>
                            <td style="width: 231px;"><a><img src="${ods.cphoto}"/></a></td>
                            <td class="product" style="width: 302px;">
                                <span>
                                    <a>${ods.gname}
                                        <br>${ods.dversion}&nbsp;&nbsp;${ods.dcolor}<br>
                                    </a>
                                </span>
                            </td>
                            <td class="price" style="width:90px;">￥${ods.dprice}</td>
                            <td class="count" style="width: 66px;">${ods.count}</td>
                            <td class="preferential" style="width: 178px;">--</td>
                            <td class="black-two" style="width: 93px;">￥${ods.dprice * ods.count}</td>
                            <td class="after-sales" style="width: 67px;">--</td>
                        </tr>
                        <script>
                            total+=${ods.dprice*ods.count};
                        </script>
                    </c:forEach>
                    <tr>
                        <td class="ticket-line" colspan="7">
                            <div class="ticket-line-item">
                                <span>发票类型：</span>
                                <span>电子发票</span>
                                <span><p style="position: absolute;top: -1px;left: 3px;">？</p></span>
                                <span>发票抬头：</span>
                                <span>${add.aname}</span>
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="item-makeorder">
                    <ul>
                        <li><span>商品总计</span>
                            <span>￥<i class="hj1"></i></span>
                        </li>
                        <li><span >回购金抵扣</span>
                            <span class="bg1">-￥0</span>
                        </li>
                        <li><span>礼品卡抵扣</span>
                            <span class="bg1">-￥0</span>
                        </li>
                        <li><span>运费</span>
                            <span>￥0</span>
                        </li>
                        <li class="end">
                            <span>应付</span>
                            <span class="bg1">￥<i class="hj2"></i></span>
                        </li>
                    </ul>
                </div>
                <div class="editBox">
                    <span>收货人信息  </span>
                </div>
                <div class="address">
                    <p>收货人：${add.aname}</p>
                    <p>地址：${add.sheng}${add.shi}${add.qu}${add.jie}${add.adetail}</p>
                    <p>电话：${add.dzphone}</p>
                </div>
            </div>
        </div>
    </div>
    <c:import url="foot.jsp"></c:import>
</body>
<script>
    document.querySelector(".hj1").innerHTML=total;
    document.querySelector(".hj2").innerHTML=total;
    <c:if test="${order.ostate==0}">
        document.querySelector(".os").innerHTML="待支付";
        $(".buttons").css("display","block");
    </c:if>
    <c:if test="${order.ostate==-1}">
    document.querySelector(".os").innerHTML="已取消";
    </c:if>
    <c:if test="${order.ostate==1}">
    document.querySelector(".os").innerHTML="待发货";
    </c:if>
    <c:if test="${order.ostate==2}">
    document.querySelector(".os").innerHTML="已发货";
    </c:if>
</script>
</html>
