<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- 底部区域开始 -->

		<div class="footer">
			<div class="footer-link">
				<div class="wrap">
					<div class="row clearfix">
						<ul class="service">
							<li><a href="#"><img src="../../img/service1.png"><span>满80免运费</span></a></li>
							<li><a href="#"><img src="../../img/service2.png"><span>100+ 城市次日送达</span></a></li>
							<li><a href="#"><img src="../../img/service3.png"><span>7天无理由退货</span></a></li>
							<li><a href="#"><img src="../../img/service4.png"><span>15天换货保障</span></a></li>
							<li><a href="#"><img src="../../img/service5.png"><span>1年免费保修</span></a></li>
							<li><a href="#"><img src="../../img/service6.png"><span>上门快修</span></a></li>
						</ul>
						<div class="onlive-server">
								<span>周一至周日 8:00-24:00</span>
								<p class="tel">400-788-3333</p>
								<a href="#" class="onlive-btn">在线客服</a>
							</div>
					</div>
						<hr>
						<div class="co">
							<div class="row clearfix">
								<div class="footer-info">
									<div class="info-left">
										<ul>
											<li><a href="#">了解魅族</a></li>
											<li><a href="#">加入我们</a></li>
											<li><a href="#">联系我们</a></li>
											<li><a href="#">线下专卖店</a></li>
											<li><a href="#">线上销售授权名单</a></li>
											<li><a href="#">隐私政策</a></li>
											<li><a href="#">涉网络暴力有害信息举报</a></li>
											<li><a href="#">涉未成年人网络有害信息举报</a></li>
											<li><a href="#">涉养老诈骗有害信息举报</a></li>
											<li class="no-border">
												<a href="#" class="language">
													<i class="icon-logo"></i>
													简体中文
												</a>
												<div class="modal-div">
													<ul class="modal-list">
														<li>
															<a href="">
																<i style="background-image:url(../../img/lang-logos.png);background-position: 0 0;"></i>
																<span>简体中文</span>
															</a>
														</li>
														<li>
															<a href="">
																<i style="background-image:url(../../img/lang-logos.png);background-position: 0 -20px;"></i>
																<span>繁體中文</span>
															</a>
														</li>
														<li>
															<a href="">
																<i style="background-image:url(../../img/lang3.png);background-position: 0 0;"></i>
																<span>English</span>
															</a>
														</li>
														<li>
															<a href="">
																<i style="background-image:url(../../img/lang-logos.png);background-position: 0px -80px;"></i>
																<span>Русский</span>
															</a>
														</li>
													</ul>
												</div>
											</li> 
										</ul>
										<div class="clearfix"></div>
										<div class="certificate">
											©2023 Meizu Telecom Equipment Co., Ltd. All Rights Reserved.
											<a href="">粤ICP备13003602号-2</a>
											<a href="">合字B2-20170010 </a>
											<a href="">营业执照</a>
											<a href="">法律声明</a>
											<a href="">粤公网安备 44049102496009 号</a>
										</div>
									</div>
									<div class="subscribe">
										<a href="">
											<img src="../../img/sub-img1.png"/>
											<i class="sub-img" style="background-size: 96%;"></i>
										</a>
										<span></span>
										<a href="" class="weixin">
											<img src="../../img/sub-img2.jpg"/>
											<i class="sub-img" style="background-size: 96%;"></i>
										</a>
										<span></span>
										<a href="">
											<i class="sub-img" style="background-size: 96%;"></i>
										</a>
										<span></span>
										<a href="">
											<i class="sub-img" style="background-size: 96%;"></i>
										</a>
										<span></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 底部区域结束 -->
<script src="../../js/foot.js"></script>
		
		