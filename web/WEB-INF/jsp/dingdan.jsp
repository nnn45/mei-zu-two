<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta charset="utf-8" />
		<title>订单</title>
		<link href="//store.res.meizu.com/layout/img/favicon-90c2e618ff.ico" rel="shortcut icon" />
		<link rel="stylesheet" href="../../css/foot.css"/>
		<link rel="stylesheet" href="../../css/base.css"/>
		<link rel="stylesheet" href="../../css/top.css"/>
<%--		<link  rel="stylesheet" href="../../css/dingdan.css"/>--%>
		<script type="text/javascript">
			//清除CSS缓存
			document.write('<link rel="stylesheet" type="text/css" href="../../css/dingdan.css?v=' + Math.random() + '"/>');
		</script>
		<script src="../../js/jquery-3.5.1.min.js"></script>
		<script>
			var total;
			function toGoods(did){
				open("/detail?did="+did);
			}
		</script>
</head>
	<body>
		<!-- ----------------头部--------------------------- -->
		<c:import url="top.jsp"></c:import>
		<div id="body-box">
			<div class="body-one">
				<a href="/index">首页 >&nbsp;</a>
				<a>我的商城 >&nbsp;</a>
				<a>我的订单</a>
			</div>
			<div class="body-two">
				<!--- 订单中心左 ------------------------------------------------------------>
				<div class="body-left">
					<a class="a-one">
						<img src="../../img/dingdan_img/订单中心.png"/>&nbsp;&nbsp;订单中心
					</a>
					<a href="/order" class="a-two" style="color: #008cff">我的订单</a>
					<a class="a-two">我的回购单</a>

					<a class="a-one">
						<img src="../../img/dingdan_img/个人中心.png"/>&nbsp;&nbsp;个人中心
					</a>
					<a href="/addressServlet" class="a-two">地址管理</a>
					<a class="a-two">我的收藏</a>
					<a class="a-two">消息提醒</a>
					<a class="a-two">建议反馈</a>

					<a class="a-one">
						<img src="../../img/dingdan_img/资产中心.png"/>&nbsp;&nbsp;资产中心
					</a>
					<a class="a-two">我的优惠券</a>
					<a class="a-two">我的红包</a>
					<a class="a-two">我的回购金</a>

					<a class="a-one">
						<img src="../../img/dingdan_img/服务中心.png"/>&nbsp;&nbsp;服务中心
					</a>
					<a class="a-two">退款/退换货跟踪</a>
					<a class="a-two">以旧换新</a>
				</div>
				<!-- 订单中心左结束 --->
				<!--- 订单中心右 ------------------------------------------------------------>
				<div class="body-right">
					<div class="right-box">
						<!--- 订单中心右第一部分 ------------------------->
						<div id="right-top">
							<a href="/order?v=show&s=99" class="order99">全部订单</a><i>|</i>
							<a href="/order?v=show&s=0" class="order0">待付款</a><i>|</i>
							<a href="/order?v=show&s=1" class="order1">待发货</a><i>|</i>
							<a href="/order?v=show&s=2" class="order2">已发货</a><i>|</i>
							<a href="/order?v=show&s=9" class="order9">其他</a>
						</div>
						<!--- 订单中心右第二部分 ------------------------->
						<div id="right-body">
							<ul id="right-ul">
								<li id="li-one">
									<select>
										<option>近三个月的订单</option>
										<option>全部订单</option>
									</select>
									订单明细
								</li>
								<li class="li-two">售后</li>
								<li class="li-two">金额</li>
								<li class="li-two">状态</li>
								<li class="li-two">操作</li>
							</ul>
						</div>
						<!--- 订单中心右第三部分 -->
						<div class="right-Order">
							<div class="datable">
								<c:if test="${empty orders}" var="none">
									<div class="none">您暂无此类订单，赶快去下单吧</div>
								</c:if>
								<c:if test="${not none}">
									<!-- 订单forEach=========================================== -->
									<c:forEach items="${orders}" var="orders">
										<script>
											total=0;
										</script>
										<table>
											<tr class="trtop">
												<!-- 订单头 -->
												<td class="tdtop" colspan="4">
													<div class="divtop">
														下单时间:
														<span class="time">${orders.otime}</span>
														&nbsp;订单号:
														<span class="DDhao">${orders.oid}</span>
														<a class="atop"><img src="../../img/dingdan_img/联系客服.png" width="18px" height="17px"/>&nbsp;联系客服</a>
													</div>
												</td>
											</tr>
											<!-- 订单体 -->
											<tr class="trbody">
												<!-- 商品详情部分 -->
												<td class="tdbodyone">
													<div class="dadiv">
														<!-- 订单详情forEach部分 -------------------->
														<c:forEach items="${orders.ods}" var="ods">
															<div class="xiaodiv" onclick="toGoods(${ods.did})">
																<img src="${ods.cphoto}"/>
																<a href="#">${ods.gname}&nbsp;${ods.dcolor}&nbsp;${ods.dversion}</a>
																<p>￥${ods.dprice}&nbsp;&nbsp;×&nbsp;${ods.count}</p>
															</div>
															<script>
																total+=${ods.dprice}*${ods.count}
															</script>
														</c:forEach>
														<!-- 订单详情forEach部分 -------------------->
													</div>
												</td>
												<!-- 价格部分 -->
												<td class="tdbodytwo">
													<div class="zongjia">￥&nbsp;
														<span class="total${orders.oid}"></span>
													</div>
												</td>
												<!-- 状态部分 -->
												<td class="tdbodytwo state${orders.oid}">
													<div class="zhuangtai${orders.oid}"></div>
												</td>
												<!-- 操作部分 -->
												<td class="tdbodytwo">
													<div class="fukuan fk${orders.oid}">
														<button class="bOne" onclick="toPay(${orders.oid})">立即付款</button>
													</div>
													<div class="dingdan dd${orders.oid}">
														<button class="bTwo" onclick="cancel(${orders.oid})">取消订单</button>
													</div>
													<div class="xiangqing">
														<a target="_blank" href="/orderdetail?v=show&oid=${orders.oid}&aid=${orders.aid}">查看详情</a>
													</div>
												</td>
											</tr>
											<script>
												document.querySelector(".zongjia .total${orders.oid}").innerHTML=total;
												function toPay(oid){
													let amount=document.querySelector(".total"+oid).innerHTML;
													open("/paymentServlet?oid="+oid+"&total="+amount);
												}
												<c:if test="${orders.ostate==0}">
												document.querySelector(".zhuangtai${orders.oid}").innerHTML="待支付";
												$(".fk${orders.oid}").css("display","block");
												$(".dd${orders.oid}").css("display","block");
												</c:if>
												<c:if test="${orders.ostate==1}">
												document.querySelector(".zhuangtai${orders.oid}").innerHTML="待发货";
												</c:if>
												<c:if test="${orders.ostate==2}">
												document.querySelector(".zhuangtai${orders.oid}").innerHTML="已发货";
												</c:if>
												<c:if test="${orders.ostate==-1}">
												document.querySelector(".zhuangtai${orders.oid}").innerHTML="已取消";
												</c:if>
												function cancel(oid) {
													if (confirm("您确定要取消该订单吗？")){
														$.ajax({
															url:"/order",
															type:"post",
															data:{
																"v":"cancelA",
																oid
															},
															dataType:"json",
															success:function (resp){
																if(resp.flag===-1){
																	$(".fk"+resp.oid).css("display","none");
																	$(".dd"+resp.oid).css("display","none");
																	let html=`<div class="zhuangtai">已取消</div>`;
																	$(".state"+resp.oid).html(html);
																}
															}
														});
													}
												}
											</script>
										</table>
									</c:forEach>
									<!-- 订单forEach=========================================== -->
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!----------------------------底部--------------------------------------------------->
		<c:import url="foot.jsp"></c:import>
	</body>
	<script>
			$(".order${s}").css("color","#008cff");
			$(".order${s}").css("padding-bottom","10px");
			$(".order${s}").css("border-bottom","solid 2px #008cff");

	</script>
</html>
