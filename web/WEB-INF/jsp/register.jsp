<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta charset="utf-8">
		<title>注册</title>
		<%--引入页签logo--%>
		<link href="https://login.flyme.cn/favicon.ico" rel="shortcut icon" />
		<link rel="stylesheet" href="../css/register.css">
		<link rel="stylesheet" href="../css/foot2.css"/>
		<script src="../js/jquery-3.5.1.min.js"></script>
	</head>
	<body>
		<div class="content">
			<div class="header clarefix">
				<a class="header-logo">
					<img src="../../img/reisterimg/mei.png" alt="">
				</a>
			</div>
			<div class="banner-box">
				<div class="rgbox" >
					<form class="min-form" action="/user" method="post" onsubmit="return check()">
						<div class="tap-tile"><a>注册 Flyme 账号</a></div>
						<div class="tip-box">
							<span class="err-ico"></span>
							<span class="tip-font"></span>
							<span class="close-ico"></span>
						</div>
						<div class="phone">
							<div class="phone-row">
								<input type="hidden" name="v" value="Register">
								<input class="right" id="name1" type="text" placeholder="手机号码" name="uphone" maxlength="11">
								</div>
								
							</div>
							<div class="password-row">
								<div class="password">
								<input type="password" id="input1" placeholder="密码(超过6位数，最少含一个字母)" name="upassword">
								</div>
							</div>
							<div class="password-row2">
								<div class="password">
								<input type="password" id="input2" placeholder="确认密码">
								</div>
							</div>
							<input type="submit" class="register-btn" value="立即注册">
							<div class="toLogin">
								<a href="/user">登录</a>
							</div>
					</form>
				</div>
			</div>
		</div>
		<c:if test="${errorRegister<0}">
			<script>
				document.querySelector(".tip-font").innerHTML=`该手机号已被注册`;
				document.querySelector(".tip-box").style.display = "block";
				document.querySelector(".close-ico").onclick = function () {
					document.querySelector(".tip-box").style.display = "none";
				}
			</script>
		</c:if>
		<!-- 底部 -->
		<c:import url="foot2.jsp"></c:import>
	</body>

	<script type="text/javascript">
		onload=function(){
			
			let right=document.querySelector(".right");
			let phone=document.querySelector(".phone-row");
			right.onfocus=function(){
				phone.style.outline=1+"px solid #387aff"; 
			}
			right.onblur=function(){
				phone.style.outline="none";
			}
		}
		$("body").keydown(function () {
			if (event.keyCode===13){
				$(".register-btn").click();
			}
		})
		//注册表单验证
		function check() {
			let uphone=document.querySelector(".right").value;
			let div=document.querySelector(".tip-box");
			let upassword=document.querySelector("#input1").value;
			let password=document.querySelector("#input2").value;
			//回显内容
			let text=document.querySelector(".tip-font");
			let reg=/^\s*$/;

			if (!(reg.test(uphone)||reg.test(upassword)||reg.test(password))) {
				if (/^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\d{8}$/.test(uphone)){
					if (upassword==password){
						if (/^(?=.*\d)(?=.*[A-z])[\da-zA-Z]{6,15}$/.test(upassword)){
							return true;
						}else {
							text.innerHTML=`密码不符合规范（6-15）位含字母`;
							div.style.display = "block";
							document.querySelector(".close-ico").onclick = function () {
								div.style.display = "none";
							}
							return false;
						}
					}else {
						text.innerHTML=`两次输入密码不一致`;
						div.style.display = "block";
						document.querySelector(".close-ico").onclick = function () {
							div.style.display = "none";
						}
						return false;
					}
				}else {
					text.innerHTML=`请输入正确的手机号`;
					div.style.display = "block";
					document.querySelector(".close-ico").onclick = function () {
						div.style.display = "none";
					}
					return false;
				}
			}else {
				text.innerHTML=`内容不能为空`;
				div.style.display = "block";
				document.querySelector(".close-ico").onclick = function () {
					div.style.display = "none";
				}
				return false;
			}
		}
		document.querySelector("#input1").addEventListener("input",function () {
			let div=document.querySelector(".tip-box");
			console.log(div)
			div.style.display="none";
		})
		document.querySelector("#input2").addEventListener("input",function () {
			let div=document.querySelector(".tip-box");
			console.log(div)
			div.style.display="none";
		})
		document.querySelector("#name1").addEventListener("input",function () {
			let div=document.querySelector(".tip-box");
			console.log(div)
			div.style.display="none";
		})
	</script>

</html>