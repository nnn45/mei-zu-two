<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta charset="utf-8">
		<title>详情页</title>
		<link href="//store.res.meizu.com/layout/img/favicon-90c2e618ff.ico" rel="shortcut icon" />
		<!-- 引入取出默认样式的css -->
		<link rel="stylesheet" href="../../css/base.css" />
		<!-- 引入公共底部css样式 -->
		<link rel="stylesheet" href="../../css/foot.css" />
		<!-- 引入公共头部css样式 -->
		<link rel="stylesheet" href="../../css/top.css" />
		<!-- 引入详情页样式 -->
		<link rel="stylesheet" href="../../css/detail.css" />
		<%--引入jquery--%>
		<script src="../../js/jquery-3.5.1.min.js"></script>
		<script type="text/javascript" src="../../js/detail.js"></script>

		<script>
			$(function () {
				lunbo();/*左边详情图片轮播*/
				/*给所有颜色选项绑定点击事件，并回显图片等等*/
				$(".yanse").click(function () {
					$("[name=count]").val(1);//重置count
					var p = $(".mod-money").html();
					$("#total_price").html(`<i>￥</i> `+p);//重置count后重置价格
					var banben =  $("[name=banben]").val();
					var color = $("[name=yanse]").val($(this).html());
					var yanse = color.val();
					$.ajax({
						url:"/detail",
						type:"post",
						data:{
							"v":"colRefreshPho",
							yanse,
							banben
						},
						dataType:"json",
						success:function (resp){
							console.log("resp:", resp.flag);
							if(resp.flag!=-1){
								$(".de_buy").css("display","none");
								$(".buy-action").css("display","flex");
								let html=``;
								resp.photo.forEach(p=>{
									html +=`<div class="swiper-slide item active">
										<img src="\${p.photo}" alt="">
									</div>`;
								})
								$(".swiper-wrapper").html(html);
								lunbo();
								let html2 = `<img src="\${resp.photo[0].photo}" class="desc-img">
										<div class="desc-info">
											<span class="detial-info">已选：\${resp.shopname}、\${resp.version}、\${resp.color}、官方标配（不含充电器）、全款、<span class="amount">1</span>件</span>
										</div>`
								$(".detail-desc").html(html2);
								let html3 = `已选：\${resp.shopname}、\${resp.version}、\${resp.color}、官方标配（不含充电器）、全款、<span class=amount>1</span>件`;
								$(".buy-info").html(html3);
								if(resp.colorPrice != null){
									let html =`<small>￥</small>
							<span class="mod-money">\${resp.colorPrice.dprice}</span>`;
									$(".sell-price .mod-price").html(html);

									let html2 = `<strong id="total_price">
												<i>￥</i>
												\${resp.colorPrice.dprice}
											</strong>`
									$(".c-wp").html(html2)

									let html3 = `<i>￥</i>\${resp.colorPrice.dprice}`
									$(".vm-price").html(html3)
								}
							}else {//没有该商品详情，变成“补货中”状态
								$(".de_buy").css("display","block");
								$(".buy-action").css("display","none");
							}
						}
					})
				})
				/*给所有版本选项绑定点击事件，并回显价格等等*/
				$(".banben").click(function () {
					$("[name=count]").val(1);//重置count
					var version =  $("[name=banben]").val($(this).html());
					var yanse = $("[name=yanse]").val();
					var banben = version.val();
					var mingzi = $("[name=mingzi]").val()
					$.ajax({
						url:"/detail",
						type:"post",
						data:{
							"v":"verRefreshPri",
							yanse,
							banben
						},
						dataType:"json",
						success:function (resp){
							if(resp.flag!=-1){
								$(".de_buy").css("display","none");
								$(".buy-action").css("display","flex");
								let html =`<small>￥</small>
							<span class="mod-money">\${resp.price.dprice}</span>`;
								$(".sell-price .mod-price").html(html);

								let html2 = `<strong id="total_price">
												<i>￥</i>
												\${resp.price.dprice}
											</strong>`
								$(".c-wp").html(html2)

								let html3 = `<i>￥</i>\${resp.price.dprice}`
								$(".vm-price").html(html3)
								let html4 = `已选：\${resp.shopname}、\${banben}、\${resp.color}、官方标配（不含充电器）、全款、<span class=amount>1</span>件`;
								$(".detial-info").html(html4);
								$(".buy-info").html(html4);
							}else {
								$(".de_buy").css("display","block");
								$(".buy-action").css("display","none");
								let html4 = `已选：\${mingzi}、\${banben}、\${yanse}、官方标配（不含充电器）、全款、<span class=amount>1</span>件`;
								$(".detial-info").html(html4);
								$(".buy-info").html(html4);
							}
						}
					})
				})

				/*给加入购物车绑定点击事件============*/
				$(".add-cart").click(function (){
					var gcount = $("[name=count]").val();
					var uid = $("[name=yonghu]").val();
					if(uid.length>0){
						$.ajax({
							url:"/cart",
							type:"post",
							data:{
								"v":"insertCart",
								gcount
							},
							dataType:"json",
							success:function (resp) {
								if(resp.r>0){
									console.log("resp.s:",resp.s)
									$(".cart-number").html(resp.s);
									$("#beijing").fadeIn("300");
									$("#wen").html("加入购物车成功！");
								}else {
									$("#beijing").fadeIn("300");
									$("#wen").html("加入购物车失败！");
								}
							}
						})
					}else {
						$("#beijing").fadeIn("300");
						$("#wen").html("您还没有登录！");
					}

				})
				/*给提示框绑定点击事件 消失===========*/
				$("#beijing").click(function (){
					$(this).fadeOut("300");
				})
				/*给“立即购买”绑定点击事件============*/
				$(".btn-buy").click(function () {
					var count = $("[name=count]").val();
					<c:if test="${empty user}" var="uop">
						location.href = "/user";//跳登录页
					</c:if>
					<c:if test="${not uop}">
						location.href = "/orderSureServlet?v=excutrr&count="+count;	//跳订单确认页,携带数量，did在session
					</c:if>
				})
				/*详情图片轮播===============*/
				function lunbo(){
					var items = document.querySelectorAll(".item");//图片
					var points = document.querySelectorAll(".bullet")//点
					var left = document.querySelector(".btn-prev");
					var right = document.querySelector(".btn-next");
					var all = document.querySelector(".wrap")
					var index = 0;
					var time = 0;//定时器跳转参数初始化


					//清除active方法
					var clearActive = function () {
						for (i = 0; i < items.length; i++) {
							items[i].className = 'item';
						}
						for (j = 0; j < points.length; j++) {
							points[j].className = 'bullet';
						}
					}

					//改变active方法
					var goIndex = function () {
						clearActive();
						items[index].className = 'item active';
						points[index].className = 'bullet active'
					}
					//左按钮事件
					var goLeft = function () {
						if (index == 0) {
							index = points.length-1;
						} else {
							index--;
						}
						goIndex();
					}

					//右按钮事件
					var goRight = function () {
						if (index < points.length-1) {
							index++;
						} else {
							index = 0;
						}
						goIndex();
					}


					//绑定点击事件监听
					left.addEventListener('click', function () {
						goLeft();
					})

					right.addEventListener('click', function () {
						goRight();
					})
					//给所有圆点绑定点击事件
					for(i = 0;i < points.length;i++){
						points[i].addEventListener('click',function(){
							var pointIndex = this.getAttribute('data-index')
							index = pointIndex;
							goIndex();
						})
					}
				}
			})
		</script>
	</head>
	<body>
		<%--隐藏域--%>
		<input type="hidden" name="yanse" value="${color[0].dcolor}"><%--颜色--%>
		<input type="hidden" name="banben" value="${version[0].dversion}"><%--版本--%>
		<input type="hidden" name="mingzi" value="${good.gname}"><%--商品名--%>
		<input type="hidden" name="yonghu" value="${user.uid}"><%--用户编号--%>

		<!--------------------- 提示框 -------------------------->
		<div id="beijing" style="display: none">
			<div id="hei"></div>
			<div id="context">
				<dl>
					<dd>提示：</dd>
					<dd id="wen">账号或密码错误！</dd>
				</dl>
			</div>
		</div>

		<!-- 头部区域开始 -->
		<c:import url="top.jsp"></c:import>
		<!-- 头部区域结束 -->

		<!-- 详情页内容开始 -->
		<!-- 下面是模板标签，不可删除 -->
		<div class="wapper page-detail">
			<div class="fast-nav" style="display: none">
				<div class="-container">
					<div class="-buy">
					</div>
					<ul class="-ulist">
						<li class="-item">
							<a href="#" class="-link">概述</a>
						</li>
						<li class="-item">
							<a href="#" class="-link">参数</a>
						</li>
					</ul>
					<span class="-name">魅族 18s Pro</span>
					<!-- 模板化 -->
					<ul class="-product">
						<!-- <li style="display: none;">
							<a href="#" class="prop">魅族 18s Pro</a>
						</li> -->
						<li>
							<a href="#" class="prop">魅族 18s</a>
						</li>
						<li>
							<a href="#" class="prop">【套餐】魅族 18s</a>
						</li>
						<li>
							<a href="#" class="prop">【套餐】魅族 18s Pro</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="row container clarefix" id="row">
				<div class="previe">
					<div class="preview-container">
						<div class="swiper-container">
							<div class="swiper-wrapper">
								<c:forEach items="${photos}" var="p">
									<div class="swiper-slide item active"><%--第一个div <div class="swiper-slide item active">--%>
										<img src="${p.photo}" alt="">
									</div>
								</c:forEach>
							</div>
							<div class="swiper-btn btn-prev"></div>
							<div class="swiper-btn btn-next"></div>
							<div class="swiper-wp">
								<div class="swiper-pagination">
									<span class="bullet bullet-active active" data-index = 0></span>
									<span class="bullet" data-index = 1></span>
									<span class="bullet" data-index = 2></span>
									<span class="bullet" data-index = 3></span>
								</div>
							</div>
						</div>
					</div>
					<!--  -->
					<div class="preview-action">

					</div>
				</div>
				<div class="property">
					<div class="property-hd">
						<div class="flex-title">
							<h1>${good.gname}</h1>
						</div>
						<p class="mod-info">
							<font color="#E03D3E">
								<c:if test="${not empty good.gdiscount}" var="op">
									${good.gdiscount}<br>
								</c:if>
							</font>
							${good.gdetail}
							<a>
								<font color="#008cff">去估价>></font>
							</a>
						</p>
					</div>
					<div class="property-sell clarefix">
						<div class="sell-price">
							<div class="mod-price">
								<small>￥</small>
								<span class="mod-money">${price.dprice}</span>
							</div>
						</div>
						<div class="sell-list">
							<div class="sell-morebuy clarefix">
								<div class="vm-entry">
									<span>加价购</span>
								</div>
								<div class="dd">
									<span>另加
										<em>89</em>
										元起，即可换购超值商品
									</span>
									<a href="javaScript:void(0);" class="vm-more">立即加购</a>
								</div>
							</div>
						</div>
					</div>
					<div class="property-service">
						<div class="property-service">
							<div class="service-suda">
								<span class="metatit">配送到</span>
								<div class="site">
									<div class="site-selector">
										<div class="text">
											广东省&nbsp;&nbsp;&nbsp;&nbsp;广州市&nbsp;&nbsp;&nbsp;
											<img src="../../img/detial/下拉三角.png" />
										</div>
									</div>
								</div>
							</div>
							<div class="property-set">

								<c:if test="${not empty version && version.size()>1}" var="v"><%--集合为空，集合对象属性都为null怎么判断--%>
								<div class="set-sale">
									<span class="metatit">版本</span>
									<div class="dd version">
										<c:forEach items="${version}" var="v">
											<c:if test="${not empty v.dversion}" var="op">
											<a class="" href="javaScript:void(0);" title="${v.dversion}"><%--选中，类名为selected--%>
												<span class="banben">${v.dversion}</span>
											</a>
											</c:if>
										</c:forEach>
									</div>
								</div>
								</c:if>
								<div class="set-sale">
									<span class="metatit">颜色</span>
									<div class="dd color">
										<c:forEach items="${color}" var="c">
											<c:if test="${not empty c.dcolor}" var="op">
											<a class="sale-img" href="javaScript:void(0);" title="${c.dcolor}">
												<span class="yanse">${c.dcolor}</span>
											</a>
											</c:if>
										</c:forEach>
									</div>
								</div>
							</div>
							<div class="property-control">
								<span class="metatit">选择数量</span>
								<div class="mod-control">
									<a href="javaScript:void(0);" title="减少" class="minus disabled">-</a>
<%--										<input type="text" value="1" name="count">--%>
										<input type="number" value="1" name="count" oninput="if(value>5)value=5;if(value.length>1)value=value.slice(0,1);if(value<1)value=1" />
									<a href="javaScript:void(0);" title="增加" class="plus">+</a>
								</div>
							</div>
							<div class="property-buy">
								<div class="buy-quantity">
									<div class="vm-count clearfix">
										<div class="c-list">
											<%--需要回显--%>
											<span class="buy-info">
												已选：${good.gname}、${version[0].dversion}、${color[0].dcolor}、官方标配（不含充电器）、全款、<span class="amount">1</span>件
											</span>
										</div>
										<div class="c-wp">
											<strong id="total_price">
												<i>￥</i>
												${price.dprice}
											</strong>
										</div>
									</div>
								</div>
								<div class="buy-action">
									<a href="javaScript:void(0);" class="btn-buy">立即购买</a>
									<a href="javaScript:void(0);" class="add-cart">加入购物车</a>
								</div>
								<div class="de_buy" style="display:none;">
									<a href="javaScript:void(0);" class="replenish">补货中</a>
								</div>
								<div class="prod-service">
									<span class="show-desc">
										<span>
											<img src="https://openfile.meizu.com/group1/M00/08/63/Cgbj0WAAFxeAGOxfAAABz1oxsQI077.png"
												style="height:17px;margin-right:8px;margin-top:1px;">
											花呗分期
										</span>
										<b>花呗分期是由蚂蚁花呗联合魅族商城共同推出的“先购物后付款”的支付体验，支持预支蚂蚁花呗额度，免息期最长可达 41 天，可选择分 3、6、12
											期还款。</b>
									</span>
									<span class="show-desc">
										<span>
											<img src="https://openfile.meizu.com/group1/M00/08/63/Cgbj0WAAFxeAGOxfAAABz1oxsQI077.png"
												style="height:17px;margin-right:8px;margin-top:1px;">
											顺丰发货
										</span>
										<b>该商品顺丰发货，顺丰无法到达的地区会安排转寄其他快递。</b>
									</span>
									<span class="show-desc">
										<span>
											<img src="https://openfile.meizu.com/group1/M00/08/63/Cgbj0WAAFxeAGOxfAAABz1oxsQI077.png"
												style="height:17px;margin-right:8px;margin-top:1px;">
											以旧换新
										</span>
										<b>免费上门取件，至高0元换新机</b>
									</span>
									<span class="show-desc">
										<span>
											<img src="https://openfile.meizu.com/group1/M00/08/63/Cgbj0WAAFxeAGOxfAAABz1oxsQI077.png"
												style="height:17px;margin-right:8px;margin-top:1px;">
											7天无理由退货
										</span>
										<b>
											自您的订单签收7个自然日之内(含)，在商品完好（手机/手表产品未激活)的情况下，可进行无理由退货。注：手机开机连接移动或WIFI网络后会被激活，手表扫码配对会被激活。
										</b>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- 内容详情图片展示区 -->
			<div class="simple row">
				<div class="simple-hd container"></div>
				<div class="simple-bd container">
					<div class="introduce">
						<a href="#">
							<img
								src="//openfile.meizu.com/group1/M00/0A/59/Cgbj0WPErieAZx2IAAjhAAdTKYI273.jpg" alt="">
						</a>
						<img
							src="//openfile.meizu.com/group1/M00/08/ED/Cgbj0WFQIzWAeccCAA4LnTprJiQ000.jpg" alt="">
					</div>

				</div>
			</div>

			<!-- 顶部粘性定位 购买信息提示栏 -->
			<div class="detial-tab detial-float" id="app">
				<div class="fix-container">
					<div class="detail-desc">
						<img src="${photos[0].photo}"
							class="desc-img">
						<div class="desc-info">
							<span class="detial-info">已选：${good.gname}、${version[0].dversion}、${color[0].dcolor}、官方标配（不含充电器）、全款、<span class="amount">1</span>件</span>
						</div>
					</div>
					<div class="shortcut">
						<div class="short-com">
							<div class="mod-buy">
								<a href="javaScript:void(0);" class="btn btn-buy">
									立即购买
								</a>
							</div>
							<div class="mod-total">
								<em class="vm-price">
									<i>￥</i>
									${price.dprice}
								</em>
								<em class="vm-huabei">
									或低至
									<em>￥</em>
									291.58 x 12 期
								</em>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 详情页内容结束 -->
		
		<!-- 返回顶部 -->
		<div class="site-gotop">
			<a href="#" class="gotop-suggest" title="建议反馈"></a>
			<div class="gotop-arrow" title="回到顶部" onclick="pageScroll()"></div>
		</div>

		<!-- 底部 -->
		<div class="foot">
			<c:import url="foot.jsp"></c:import>
		</div>
		
		<!-- 详情页左侧图片滚动粘性定位 -->
		<script>
			//获得页面卷曲的内容
			//获得浏览器的内部高度
			let ScreenTop = window.innerHeight;
			//获取滚动元素的高度
			let divTop = document.querySelector(".previe").offsetHeight;
			let tops = divTop - ScreenTop;
			if (tops <= 0) {
				document.querySelector(".previe").style.top = 0 + "px";
			} else {
				document.querySelector(".previe").style.top = -tops + 150 + "px";
			}
		</script>
		<!-- 商品详情菜单漂浮  当鼠标滚动到一定位置 会有一个导航漂浮起来 -->
		<script>
			/* "商品详情菜单漂浮"  当鼠标滚动到一定位置 会有一个导航漂浮起来   */
			window.onscroll = function() {
				//设置scrollTop > 300，即滚动条向下滚动，div的内容上方超出浏览器最顶部位置>300。
				//获取元素在页面的高度
				var scro = document.querySelector("#row").scrollHeight;
				var detial = document.querySelector(".detial-tab");
				let scrollTop = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop;
				if (scrollTop > scro) {							
					$("#app").css("top",0);
				} else {
					$("#app").css("top","-133px");
				}
				/* 返回顶部的盒子 ====*/
				if(scrollTop>=600){
					$(".site-gotop").css("display","block");
				}else{
					$(".site-gotop").css("display","");
				}
			}
		</script>
		
		
		<!-- 回到顶部 -->
		<script>
			function pageScroll(){
			    //把内容滚动指定的像素数（第一个参数是向右滚动的像素数，第二个参数是向下滚动的像素数）
			    window.scrollBy(0,-100);
			    //延时递归调用，模拟滚动向上效果
			    scrolldelay = setTimeout('pageScroll()',50);
			    //获取scrollTop值，声明了DTD的标准网页取document.documentElement.scrollTop，否则取document.body.scrollTop；因为二者只有一个会生效，另一个就恒为0，所以取和值可以得到网页的真正的scrollTop值
			    var sTop=document.documentElement.scrollTop+document.body.scrollTop;
			    //判断当页面到达顶部，取消延时代码（否则页面滚动到顶部会无法再向下正常浏览页面）
			    if(sTop==0) clearTimeout(scrolldelay);
			}
		</script>
		<!-- 详情图片轮播 -->
		<%--<script>
		    var items = document.querySelectorAll(".item");//图片
		    var points = document.querySelectorAll(".bullet")//点
		    var left = document.querySelector(".btn-prev");
		    var right = document.querySelector(".btn-next");
		    var all = document.querySelector(".wrap")
		    var index = 0;
		    var time = 0;//定时器跳转参数初始化


		    //清除active方法
		    var clearActive = function () {
		        for (i = 0; i < items.length; i++) {
		            items[i].className = 'item';
		        }
		        for (j = 0; j < points.length; j++) {
		            points[j].className = 'bullet';
		        }
		    }

		    //改变active方法
		    var goIndex = function () {
		        clearActive();
		        items[index].className = 'item active';
		        points[index].className = 'bullet active'
		    }
		    //左按钮事件
		    var goLeft = function () {
		        if (index == 0) {
		            index = points.length-1;
		        } else {
		            index--;
		        }
		        goIndex();
		    }

		    //右按钮事件
		    var goRight = function () {
		        if (index < points.length-1) {
		            index++;
		        } else {
		            index = 0;
		        }
		        goIndex();
		    }


		    //绑定点击事件监听
		    left.addEventListener('click', function () {
		        goLeft();
		    })

		    right.addEventListener('click', function () {
		        goRight();
		    })
			//给所有圆点绑定点击事件
		    for(i = 0;i < points.length;i++){
		        points[i].addEventListener('click',function(){
		            var pointIndex = this.getAttribute('data-index')
		            index = pointIndex;
		            goIndex();
		        })
		    }

		</script>
	--%>
	</body>
</html>
