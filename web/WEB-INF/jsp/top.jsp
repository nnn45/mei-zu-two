<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="../../js/jquery-3.5.1.min.js"></script>
<script>
	/*跳转搜索列表*/
	function toSearch(){
		let s=document.querySelector("input[name='search']").value;
		open("/search?search="+s);
	}
	/*头部悬浮商品*/
	function ChangGood(id) {
		$.ajax({
			url: "/index",
			type: "post",
			data: {
				"v": "goodList",
				id
			},
			dataType: "json",
			success: function (resp) {
				let goodsbox = document.querySelectorAll(".hov");
				console.log(resp);
				for (let i=0;i<goodsbox.length;i++) {
					let html = ``;
					resp.forEach(p => {
						html += `<li class="rem" onclick="Pork(\${p.did})">
                              <p><img src="\${p.gphoto}"/></p>
                              <p>\${p.gname}</p>
                              <p>￥\${p.dprice}</p>
                             </li>`;
					})
					goodsbox[i].innerHTML=html;
					
				}
			}
		})
	}
    $(".rem").on('click',Pork(id));
	function Pork(id) {
		open("/detail?did="+id);
	}
	/*跳转个人中心*/
	function toUserInfo() {
		open("/userinfo");
	}
	/*跳转订单管理*/
	function toOrder() {
		open("/order");
	}
	/*退出登录*/
	function outLogin() {
		location.href="/user";
	}
	/*跳登录页面*/
	function toLogin() {
		open("/user");
	}
	//后台登录
	function gohtLogin(){
		open("/htLogin");
	}
	/*跳注册页*/
	function toRegister() {
		open("/user?v=Register");
	}
	/*跳首页*/
	function toIndex(){
		open("/index");
	}

	$(function () {
		/*给头部购物车图标绑定点击事件*/
		$(".Img").click(function () {
			<c:if test="${empty user}" var="op">
				$("#beijing").fadeIn("300");
				$("#wen").html("您还没有登录！");
			</c:if>
			<c:if test="${not op}">
				open("/cart");
			</c:if>
		});
		/*给提示框绑定点击事件 消失===========*/
		$("#beijing").click(function (){
			$(this).fadeOut("300");
		});
		/*给“我的订单”绑定点击事件*/
		$(".myOrder").click(function () {
			<c:if test="${op}">
			$("#beijing").fadeIn("300");
			$("#wen").html("您还没有登录！");
			</c:if>
		});
	})

    //头部滑动速度

	// $(function(){
	// 	$(".fu").hover(function(){
	// 		$(".mask").stop().slideDown(100);
	// 	},function () {
	// 		$(".mask").stop().slideUp(100);
	// 	});
	// 	$(".app-fu").hover(function(){
	// 		$(".app-photo-div").stop().slideDown(100);
	// 	},function () {
	// 		$(".app-photo-div").stop().slideUp(100);
	// 	});
	// });
	/*头部购物车的删除方法*/
	function deleteCatt(cid) {
		var id = document.getElementById(cid);
		$.ajax({
			url: "/index",
			type: "post",
			data: {
				"v": "deleteCart",
				cid
			},
			dataType: "json",
			success: (resp => {
				document.getElementById(cid).remove();
				let ul = $(".cart-div-ul");
				ul.innerHTML = "";
				let html = ``;
				resp.cartLIst.forEach(s => {
					html += `<li class="cart-div-li" id="\${s.cid}">
                                    <div class="cart-font">
                                        <a class="cart-font-image">
                                            <img src="\${s.cphoto}" alt="">
                                        </a>
                                        <a class="cart-font-text">
                                            <p class="cart-font-text-row">\${s.gname}</p>
                                            <p class="cart-font-text-desc">\${s.dcolor}</p>
                                        </a>
                                        <a class="cart-font-right">
                                            <p class="cart-items-total">\${s.dprice} × \${s.count}</p>
                                            <div class="cart-items-delete" onclick="deleteCatt(\${s.cid})">删除</div>
                                        </a>
                                    </div>
                                </li>`;
				})
				if (resp.cartLIst.length!=0) {
					ul.innerHTML = html;
				}else {
					$(".cart-div").innerHTML=`<div class="cart-levitation">
										<img src="../../img/熊猫头像2.png" alt="">
										<span><p>您的购物车还没有商品,</p><p>赶紧去选购吧~</p></span>
										</div>`;
				}
				$(".cart-number").html(resp.s);
				$(".font").html(resp.s);
			})
		})
	}
	$(function () {
		/*头部购物车鼠标悬浮的回显*/
		$(".cart").hover(function () {
			$.ajax({
				url:"/index",
				type:"post",
				data:{
					"v":"selectAllCart"
				},
				dataType:"json",
				cache: "false",
				success:function (resp) {
					let ul=document.querySelector(".cart-div-ul");
					ul.innerHTML="";
					let html=``;
					resp.cart.forEach(c=>{
							html += `<li class="cart-div-li" id="\${c.cid}">
                                    <div class="cart-font">
                                        <a class="cart-font-image">
                                            <img src="\${c.cphoto}" alt="">
                                        </a>
                                        <a class="cart-font-text">
                                            <p class="cart-font-text-row">\${c.gname}</p>
                                            <p class="cart-font-text-desc">\${c.dcolor}</p>
                                        </a>
                                        <a class="cart-font-right">
                                            <p class="cart-items-total">\${c.dprice} × \${c.count}</p>
                                            <div class="cart-items-delete" onclick="deleteCatt(\${c.cid})">删除</div>
                                        </a>
                                    </div>
                                </li>`;
					})
					if (resp.cart!=null){
						ul.innerHTML=html;
					}else {
						$(".cart-triangle").innerHTML=`<div class="cart-levitation">
					<img src="../../img/熊猫头像2.png" alt="">
					<span><p>您的购物车还没有商品,</p><p>赶紧去选购吧~</p></span>
					</div>`;
					}
					$(".cart-number").html(resp.f);
					$(".font").html(resp.f);
				}
			})
		})

	})

</script>
<!--------------------- 提示框 -------------------------->
<div id="beijing" style="display: none">
	<div id="hei"></div>
	<div id="context">
		<dl>
			<dd>提示：</dd>
			<dd id="wen">账号或密码错误！</dd>
		</dl>
	</div>
</div>

<div class="boxOne">
	<div class="header">
		<div class="center">
			<div class="nav">
				<div class="meizuimg">
					<img onclick="toIndex()" style="cursor: pointer" src="img/mei.png" />
				</div>
				<div class="cart">
					<c:if test="${user!=null}" var="f">
					<img  class="Img" src="../../img/购物车.png" />
					<span class="cart-number">${b}</span>
					<div class="cart-triangle"></div>
						<c:if test="${not empty cartList}" var="h">
                        <div class="cart-div">
                            <div class="cart-div-text">最近加入的商品</div>
                            <ul class="cart-div-ul">
                                <c:forEach items="${cartList}" var="q">
                                   <li class="cart-div-li" id="${q.cid}">
                                    <div class="cart-font">
                                        <a class="cart-font-image" href="/detail?did=${q.did}">
                                            <img src="${q.cphoto}" alt="">
                                        </a>
                                        <a class="cart-font-text" href="/detail?did=${q.did}">
                                            <p class="cart-font-text-row">${q.gname}</p>
                                            <p class="cart-font-text-desc">${q.dcolor}</p>
                                        </a>
                                        <a class="cart-font-right">
                                            <p class="cart-items-total">${q.dprice} × ${q.count}</p>
                                            <div class="cart-items-delete" onclick="deleteCatt(${q.cid})">删除</div>
                                        </a>
                                    </div>
                                </li>
                                </c:forEach>
                            </ul>
                            <div class="cart-footer">
                                <span class="cart-total">共<a class="font" style="list-style: none;text-decoration: none;color: red">${b}</a>件商品</span>
                                <a class="me-btn" href="/cart">去购物车</a>
                            </div>
                        </div>
						</c:if>
						<c:if test="${not h}">
							<div class="cart-levitation">
								<img src="../../img/熊猫头像2.png" alt="">
								<span>
							<p>您的购物车还没有商品,</p>
							<p>赶紧去选购吧~</p>
						     </span>
							</div>
						</c:if>
					</c:if>
					<c:if test="${not f}">
                        <img src="../../img/购物车.png" />
                        <span class="cart-number">0</span>
                        <div class="cart-triangle"></div>
                        <div class="cart-levitation">
                            <img src="../../img/熊猫头像.png" alt="">
                            <span>
							<p>登录后可显示</p>
							<p>您账号中已加入的商品哦~</p>
						</span>
                        </div>
					</c:if>
				</div>
				<div class="user">
					<c:if test="${user!=null}" var="c">
								<span id="user-img">
								<img src="${user.uheadshot}" />
							   </span>
					</c:if>
					<c:if test="${not c}">
								<span id="user-img">
								<img src="../../img/头部图标/个人中心.png" />
							   </span>
					</c:if>
					<div class="triangle"></div>

					<c:if test="${user!=null}" var="h">
						<div class="levitation">
							<p style="cursor: pointer" onclick="toUserInfo()">个人中心</p>
							<p style="cursor: pointer" onclick="toOrder()">我的订单</p>
							<p style="cursor: pointer" onclick="gohtLogin()">后台管理</p>
							<p style="cursor: pointer" onclick="outLogin()">退出登录</p>
						</div>
					</c:if>
					<c:if test="${not h}">
						<div class="levitation">
							<p style="cursor: pointer" onclick="toLogin()">立即登录</p>
							<p style="cursor: pointer" onclick="toRegister()">立即注册</p>
							<p style="cursor: pointer" class="myOrder">我的订单</p>
							<p style="cursor: pointer" onclick="gohtLogin()">后台管理</p>
						</div>
					</c:if>

				</div>
				<div class="search">
					<input type="text" placeholder="魅族 18s" name="search" id="myinput"/>
					<span class="search-icon" onclick="toSearch()">
							<img src="../../img/搜索图标.png" />
					</span>
				</div>
				<ul class="clarefix">
					<li class="app-fu"><a>APP下载</a>
						<div class="app-photo-div">
							<div class="app-photo">
								<img src="../../img/header-app.jpg" />
							</div>
						</div>
					</li>
					<c:forEach  items="${goodsSorts}" var="g">
						<li class="fu" id="fukuan" onmouseenter="ChangGood(${g.sid})" style="cursor: pointer"><a>${g.sname}</a>
							<div class="mask">
								<ul class="hover hov">
								</ul>
							</div>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
</div>
<script>
	let myinpute=document.querySelector("#myinput");
	myinpute.addEventListener("keyup",function (event) {
		event.preventDefault();
		if (event.keyCode===13){
			document.querySelector(".search-icon").click();
		}
	})
</script>
