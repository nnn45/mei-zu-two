<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
	<head>
		<meta charset="utf-8">
		<title>付款页</title>
		<link href="//store.res.meizu.com/layout/img/favicon-90c2e618ff.ico" rel="shortcut icon" />
		<link rel="stylesheet" href="../../css/payment.css"/>
		<script src="../../js/jquery-3.5.1.min.js"></script>
		<script>history.go(1);</script>
		<script type="text/javascript">
			function test(obj){
				let div1=document.getElementById("zhedie-div");
				let input =document.getElementById("input");
				let i=111;
				if(div1.style.display=="block"){
					div1.style.display="none";
					input.style.bottom="0px";
				}else{
					div1.style.display="block";
					input.style.bottom="-28px";
					div1.style.zIndex=i;
					input.style.zIndex=i;
				}
			}
			//倒计时
			onload=function (){
				let t=document.querySelector("#i-time");
				let time=1800;//30分钟换算成1800秒
				setInterval(function (){
					time=time-1;
					let minute=parseInt(time/60);
					t.innerHTML=minute;
				},1000);

			}
		</script>
	</head>
	<body>
		
		<div class="top-header">
			<div class="harder-one">
				<a id="a-i-span" href="#">常见问题</a><i id="i-span-a">|</i>
				<span id="span-a-i">你好，欢迎使用支付宝付款！</span>
			</div>
		</div>
		<div class="body-header-box">
			<div class="body-header">
				<img id="zfb-img" src="../../img/fukuan_img/支付宝.png"/>
				<span id="zfb-sapn">我的收银台</span>
			</div>
		</div>
		<hr />
		<div class="box-body">
			<div class="body-body">
				<!-- ======================= -->
				<div class="zhedie">
					<div id="zhedie-top">
						<div id="topone"> 
							<span>正在使用即时到账交易<i id="i-wen">[?]</i></span>&emsp;
							<span>交易将在<i id="i-time">29</i>分钟后关闭，请及时付款！</span>
						</div>
						<div id="toptwo">
							<span>魅族-订单</span><span>${orrder}</span>
						</div>
						<div id="dianji">
							<input id="input" type="submit" onclick="test(this)" value="订单详情"/>
						</div>
						<div id="idprice"><strong>${total}.00</strong><span>元</span> </div>
						<div id="zhedie-div">
							<div class="zhedie-xq"><span class="span1"> 收款方：</span>&emsp;<span>*族</span></div>
							<div class="zhedie-xq"><span class="span1"> 订单号：</span>&emsp;<span>${orrder}</span></div>
							<div class="zhedie-xq"><span class="span1"> 商品名称：</span><span>魅族-订单号${orrder}</span></div>
							<div class="zhedie-xq"><span class="span1"> 交易金额：</span><span>${total}.00</span></div>
						</div>
					</div>
					<div class="saoma">
						<div id="saoma-left">
							<p style="margin-top: 102px;margin-bottom: 10px; font-size: 12px;">扫一扫付款&emsp;(元)</p>
							<p id="strong-p">${total}.00</p>
							<div>
								<img src="../../img/fukuan_img/扫码支付.png"/>
							</div>
							<a style="text-decoration:underline; font-size: 12px; color: #a1a1a1;">首次使用请下载手机支付宝</a>
						</div>
						<div id="saoma-right">
							<img src="../../img/fukuan_img/登录账户付款.png"/>
							<button onclick="sure(${orrder})">确认付款</button>
							<button onclick="qvXiao(${orrder})">取消付款</button>
						</div>
					</div>
				</div>
				
				<!-- ============================= -->
			</div>
		</div>
		<div class="box-footer">
			<div class="footer-one">
				<a href="#">ICP证：合字B2-20190046</a>
			</div>
		</div>
		<div class="footer-img">
			<img src="../../img/fukuan_img/付款页底部.png"/>
		</div>
	</body>
	<script>
		function qvXiao(oid) {
			location.href="/order?v=cancel&oid="+oid;
		}
		function sure(oid) {
			location.href="/order?v=sure&oid="+oid;
		}
	</script>
</html>