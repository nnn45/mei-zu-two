<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--底部区域开始 -->
		<div class="footer">
			<div class="footerInner">
				<div class="serves">
					<div class="innerLink" style="float: left;width: 445px;">
						<a>关于魅族</a> <span></span>
						<a>工作机会</a> <span></span> 
						<a>联系我们</a> <span></span> 
						<a>法律声明</a>  <span></span> 
						<a>常见问题</a> <span></span> 
						<div class="language" style="position: relative;">
							简体中文&nbsp;&nbsp;&nbsp;
							<a class="English" style="display:none;">English</a>
						</div>
					</div>
					<div class="serve" style="float: left;margin-left: 36px;">
						<span style="margin-right: 10px; float: left;">客服热线&nbsp;</span>
						<span style="margin-right: 20px; float: left;">400-788-3333&nbsp;</span>
						<a> 在线客服</a>
					</div>
					<div class="subscribe">
						<a><i class="sub-img1"></i></a>
						<a class="weixin">
							<img src="../../img/buttomImg/weixin.png"/>
							<i class="sub-img2"></i>
						</a>
						<a><i class="sub-img3"></i></a>
					</div>
				</div>
				<div class="copyrightWrap">
					<span>©2023 Meizu Telecom Equipment Co., Ltd. All rights reserved.</span>
					<a href="http://www.miitbeian.gov.cn/" style="width: 166px;">备案号: 粤ICP备13003602号-4</a>
					<a href="http://www2.res.meizu.com/zh_cn/images/common/com_licence.jpg" style="width: 178.5px;">经营许可证编号: 粤B2-20130198</a>
					<a href="http://www2.res.meizu.com/zh_cn/images/common/com_licence.jpg" style="width: 48px;">营业执照</a>
			</div>
			</div>
		</div>
<!-- 底部区域结束 -->