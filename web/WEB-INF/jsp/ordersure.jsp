<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta charset="utf-8">
		<title>确认订单—魅族商城</title>
		<link href="//store.res.meizu.com/layout/img/favicon-90c2e618ff.ico" rel="shortcut icon" />
		<!-- 引入页签图片（标题前的小图片） -->
		<link rel="shortcut icon" href="../../img/订单确认/favicon-90c2e618ff.ico" />
		<!-- 引入取出默认样式的css -->
		<link rel="stylesheet" href="../../css/base.css"/>
		<!-- 引入css样式 -->
		<link rel="stylesheet" href="../../css/foot.css"/>
		<link rel="stylesheet" href="../../css/ordersure.css"/>
		<script src="../js/jquery-3.5.1.min.js"></script>
		<script>
			onload=function(){
				tianjia();
				guanbi();
				yanzhenTJ();
			}

			//添加(按钮)
			function tianjia(){
				let add=document.querySelector(".add-add-box");
				let addboxs=document.querySelector(".addboxs");
				add.addEventListener("click",function (){
					addboxs.style.display="block";
				});
				let xinzeng=document.querySelectorAll(".sure");
				xinzeng[1].style.zIndex=-1;

			}

			//关闭按钮
			function guanbi(){
				//点击叉【×】
				let b=document.querySelector(".addboxs");
				document.querySelector(".close").addEventListener("click",function(){
					document.querySelector("#errormz").style.display="none";
					document.querySelector("#errordh").style.display="none";
					document.querySelector("#errorZXS").style.display="none";
					document.querySelector("#errorJD").style.display="none";
					/*======*/
					document.querySelector("#username").value="";
					document.querySelector("#userphone").value="";
					let addresParent = document.querySelector("#addresParent");
					addresParent.childNodes[0].options[addresParent.childNodes[0].selectedIndex].innerHTML="省/直辖市";
					addresParent.childNodes[1].options[addresParent.childNodes[1].selectedIndex].innerHTML="城市";
					addresParent.childNodes[2].options[addresParent.childNodes[2].selectedIndex].innerHTML="区/县";
					addresParent.childNodes[3].options[addresParent.childNodes[3].selectedIndex].innerHTML="乡镇/街道";
					document.querySelector("#useraddress").value="";
					/*==*/
					let xiugaiwan = document.querySelectorAll(".sure");
					xiugaiwan[1].style.zIndex = -1;
					b.style.display="none";
				});
				//点击取消
				let quxiao=document.querySelectorAll(".close");
				quxiao[1].addEventListener("click",function (){
					document.querySelector("#errormz").style.display="none";
					document.querySelector("#errordh").style.display="none";
					document.querySelector("#errorZXS").style.display="none";
					document.querySelector("#errorJD").style.display="none";
					/*======*/
					document.querySelector("#username").value="";
					document.querySelector("#userphone").value="";
					let addresParent = document.querySelector("#addresParent");
					addresParent.childNodes[0].options[addresParent.childNodes[0].selectedIndex].innerHTML="省/直辖市";
					addresParent.childNodes[1].options[addresParent.childNodes[1].selectedIndex].innerHTML="城市";
					addresParent.childNodes[2].options[addresParent.childNodes[2].selectedIndex].innerHTML="区/县";
					addresParent.childNodes[3].options[addresParent.childNodes[3].selectedIndex].innerHTML="乡镇/街道";
					document.querySelector("#useraddress").value="";
					/*==*/
					let xiugaiwan = document.querySelectorAll(".sure");
					xiugaiwan[1].style.zIndex = -1;
					b.style.display='none';
				})
			}

			//新增地址
			function insert() {
				myname();
				mydianhua();
				mysheng();
				myXQdizhi();
				if (myname() == true && mydianhua() == true && mysheng() == true && myXQdizhi() == true) {
				let name = document.querySelector("#username").value;
				let phone = document.querySelector("#userphone").value;
				let addresParent = document.querySelector("#addresParent");
				let shen = addresParent.childNodes[0].options[addresParent.childNodes[0].selectedIndex].value;
				let shi = addresParent.childNodes[1].options[addresParent.childNodes[1].selectedIndex].value;
				let qu = addresParent.childNodes[2].options[addresParent.childNodes[2].selectedIndex].value;
				let jie = addresParent.childNodes[3].options[addresParent.childNodes[3].selectedIndex].value;
				let dizi = document.querySelector("#useraddress").value;
				let set_default = 1;
				$.ajax({
					url: "/orderSureServlet",
					type: "post",
					data: {
						"v": "insertOrderSure",
						name,
						phone,
						shen,
						shi,
						qu,
						jie,
						dizi,
						set_default
					},
					dataType: "JSON",
					success: function (resp) {
						console.log("新增：：", resp)
						//点击确定关闭新增窗口
						let addboxs = document.querySelector(".addboxs");
						addboxs.style.display = "none";
						//数据回显
						let ul = document.querySelector(".ul");
						let add = JSON.parse(resp.add);
						ul.innerHTML = "";
						let htmlxz = ``;
						let htmlxzk = `<li class="add-add-box" >
						<div class="jiahao"></div>
						<div class="add-address">
							添加新地址
						</div>
					</li>`;
						add.forEach(r => {

							htmlxz += `<li class="a1" id="\${r.aid}" onclick="reviseMR(\${r.aid})">
							<div class="addtop">
								<p class="p1">\${r.aname}</p>
								<p class="p2">\${r.dzphone}</p>
							</div>
							<p class="xiangxi">\${r.sheng}\${r.shi}\${r.qu}\${r.jie}\${r.adetail}</p>`;
							if (r.set_default==1){
								htmlxz +=`<img id="\${r.set_default}" class="gouxuan"  src="../../img/订单确认/选中.png">`
							}
							htmlxz +=`<div class="changehover">
								<a onclick="xiugai(\${r.aid})">修改</a>&nbsp;&nbsp;&nbsp;
								<a onclick="delet(\${r.aid})">删除</a>
							</div>`;
							htmlxz +=`</li>`;
						})
						if (add.length<10){
							ul.innerHTML = htmlxz + htmlxzk;
						}else {
							ul.innerHTML = htmlxz;
						}
						tianjia();
						document.querySelector("#username").value="";
						document.querySelector("#userphone").value="";
						let addresParent = document.querySelector("#addresParent");
						addresParent.childNodes[0].options[addresParent.childNodes[0].selectedIndex].innerHTML="省/直辖市";
						addresParent.childNodes[1].options[addresParent.childNodes[1].selectedIndex].innerHTML="城市";
						addresParent.childNodes[2].options[addresParent.childNodes[2].selectedIndex].innerHTML="区/县";
						addresParent.childNodes[3].options[addresParent.childNodes[3].selectedIndex].innerHTML="乡镇/街道";
						document.querySelector("#useraddress").value="";
					}
				})
				}
				yanzhenTJ();
				let msg=document.querySelector(".msg");
				msg.style.display="none";

			}

			//修改地址
			function xiugai(id){
				console.log("00000000000000");
				let address=document.querySelector(".addboxs");
				address.style.display="block";
				let xiugaianniu=document.querySelectorAll(".sure");
				xiugaianniu[1].style.zIndex=6;
				$.ajax({
					url:"/orderSureServlet",
					type:"post",
					data:{
						"v":"hiuxianDZ",
						id
					},
					dataType: "JSON",
					success:function (resp) {
						$("#username").val(resp.aname);
						$("#userphone").val(resp.dzphone);
						$("#useraddress").val(resp.adetail);
						let addresParent=document.querySelector("#addresParent");
						let sheng=addresParent.childNodes[0].options[addresParent.childNodes[0].selectedIndex];
						let shi=addresParent.childNodes[1].options[addresParent.childNodes[1].selectedIndex];
						let qu=addresParent.childNodes[2].options[addresParent.childNodes[2].selectedIndex];
						let jiedao=addresParent.childNodes[3].options[addresParent.childNodes[3].selectedIndex];
						sheng.innerText=resp.sheng;
						shi.innerHTML=resp.shi;
						qu.innerHTML=resp.qu;
						jiedao.innerHTML=resp.jie;
					}
				})
				//阻止冒泡
				event.stopPropagation();
			}
			function xiugaiDZ(){
				myname();
				mydianhua();
				mysheng();
				myXQdizhi();
				if (myname() == true && mydianhua() == true && mysheng() == true && myXQdizhi() == true) {
					/*let queding=document.querySelectorAll(".sure");
					console.log(queding[0]);
					queding[1].style.zIndex=6;*/
					let mz = document.querySelector("#username").value;
					let dh = document.querySelector("#userphone").value;
					let addresParent = document.querySelector("#addresParent");
					let shen = addresParent.childNodes[0].options[addresParent.childNodes[0].selectedIndex].value;
					let shi = addresParent.childNodes[1].options[addresParent.childNodes[1].selectedIndex].value;
					let qu = addresParent.childNodes[2].options[addresParent.childNodes[2].selectedIndex].value;
					let jd = addresParent.childNodes[3].options[addresParent.childNodes[3].selectedIndex].value;
					let dz = document.querySelector("#useraddress").value;
					$.ajax({
						url: "/orderSureServlet",
						type: "post",
						data: {
							"v": "updateDZ",
							mz,
							dh,
							shen,
							shi,
							qu,
							jd,
							dz
						},
						dataType: "JSON",
						success: function (resp) {
							let ul = document.querySelector(".ul");
							ul.innerHTML = "";
							let html = ``;
							let html1 = `<li class="add-add-box" >
						<div class="jiahao"></div>
						<div class="add-address">
							添加新地址
						</div>
					</li>`;
							resp.forEach(r => {
								html += `<li class="a1" id="\${r.aid}" onclick="reviseMR(\${r.aid})">
							<div class="addtop">
								<p class="p1">\${r.aname}</p>
								<p class="p2">\${r.dzphone}</p>
							</div>
							<p class="xiangxi">\${r.sheng}\${r.shi}\${r.qu}\${r.jie}\${r.adetail}</p>`;
									if (r.set_default==1){
										html += `<img id="\${r.set_default}" class="gouxuan"  src="../../img/订单确认/选中.png">`;
									}
								html += `<div class="changehover">
								<a onclick="xiugai(\${r.aid})">修改</a>&nbsp;&nbsp;&nbsp;
								<a onclick="delet(\${r.aid})">删除</a>
							</div>`;
						html+=`</li>`;
							});
							if (resp.length<10){
								ul.innerHTML = html + html1;
							}else {
								ul.innerHTML = html;
							}

							tianjia();
							let xiugaiwan = document.querySelectorAll(".sure");
							xiugaiwan[1].style.zIndex = -1;

							document.querySelector("#username").value="";
							document.querySelector("#userphone").value="";
							let addresParent = document.querySelector("#addresParent");
							addresParent.childNodes[0].options[addresParent.childNodes[0].selectedIndex].innerHTML="省/直辖市";
							addresParent.childNodes[1].options[addresParent.childNodes[1].selectedIndex].innerHTML="城市";
							addresParent.childNodes[2].options[addresParent.childNodes[2].selectedIndex].innerHTML="区/县";
							addresParent.childNodes[3].options[addresParent.childNodes[3].selectedIndex].innerHTML="乡镇/街道";
							document.querySelector("#useraddress").value="";
						}

					})

					let addboxs = document.querySelector(".addboxs");
					addboxs.style.display = 'none';
				}
			}
			//点击li框修改默认值
			function reviseMR(id){
				console.log("1212121212");
				$.ajax({
					url:"/orderSureServlet",
					type:"post",
					data:{
						"v":"reviseMR",
						id
					},
					dataType:"json",
					success:function (resp){
						let ul = document.querySelector(".ul");
						ul.innerHTML = "";
						let html = ``;
						let html1 = `<li class="add-add-box" >
						<div class="jiahao"></div>
						<div class="add-address">
							添加新地址
						</div>
					</li>`;
						resp.forEach(r => {
							html += `<li class="a1" id="\${r.aid}" onclick="reviseMR(\${r.aid})">
							<div class="addtop">
								<p class="p1">\${r.aname}</p>
								<p class="p2">\${r.dzphone}</p>
							</div>
							<p class="xiangxi">\${r.sheng}\${r.shi}\${r.qu}\${r.jie}\${r.adetail}</p>`;
							if (r.set_default==1){
								html += `<img id="\${r.set_default}" class="gouxuan"  src="../../img/订单确认/选中.png">`;
							}
							html += `<div class="changehover">
								<a onclick="xiugai(\${r.aid})">修改</a>&nbsp;&nbsp;&nbsp;
								<a onclick="delet(\${r.aid})">删除</a>
							</div>`;
							html+=`</li>`;
						});
						if (resp.length<10){
							ul.innerHTML = html + html1;
						}else {
							ul.innerHTML = html;
						}
						tianjia();
					}
				})
			}

			//删除地址
			function delet(id){
				if (confirm("您确定要删除这条地址记录?")){
					$.ajax({
						url: "/orderSureServlet",
						type: "post",
						data: {
							"v":"deleteOrderSure",
							id
						},
						dataType:"json",
						success:function (resp) {
							//回显删除
							document.getElementById(id).remove();
						}
					})
				}
				/*let msg=document.querySelector(".msg");
				let a1=document.querySelectorAll(".a1");
				console.log("a1",a1);
				if (a1.length==1){
					msg.style.display="inline-block";
				}*/
				yanzhenTJ();
				//阻止冒泡
				event.stopPropagation();
			}
			//地址添加到十个就阻止添加。
			function yanzhenTJ(){
				let a1=document.querySelectorAll(".a1");
				let msg=document.querySelector(".msg");
				let li=document.querySelector(".add-add-box");
				console.log("a1长度：：",a1.length);
				if (a1.length==10){
					li.style.display="none";
				}else if(a1.length==1){
					msg.style.display="inline-block";
				}
			}

			//验证
			//收获人姓名
			function myname(){
				let name=document.querySelector("#username").value;
				let errormz=document.querySelector("#errormz");
				let nul=/^\s*$/;
				let mingzi=/^(?:[\u4e00-\u9fa5]+)(?:●[\u4e00-\u9fa5]+)*$|^[a-zA-Z0-9]+\s?[\.·\-()a-zA-Z]*[a-zA-Z]+$/;
				if (!nul.test(name)){
					if (mingzi.test(name)){
						errormz.style.display='none';
						return true;
					}else {
						errormz.style.display='block';
						errormz.innerHTML='不能存在特殊字符跟空格';
						return false;
					}
				}else {
					errormz.style.display='block';
					errormz.innerHTML='必填';
					return false;
				}
			}
			//收获人电话
			function mydianhua(){
				let phone=document.querySelector("#userphone").value;
				let errordh=document.querySelector("#errordh");
				let nul=/^\s*$/;
				let shouji=/^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\d{8}$/;
				if (!nul.test(phone)){
					if (shouji.test(phone)){
						errordh.style.display="none";
						return true;
					}else {
						errordh.style.display="block";
						errordh.innerHTML="手机号码格式错误";
						return false;
					}
				}else {
					errordh.style.display="block";
					errordh.innerHTML="必填";
					return false;
				}
			}
			//省市区直辖市
			function mysheng(){
					let addresParent=document.querySelector("#addresParent");
					let sel=addresParent.childNodes[0];
					sel.addEventListener("change",function (){
						shen=event.target.value;
						let errorZXS=document.querySelector("#errorZXS");
						if (shen!='省/直辖市'){
							errorZXS.style.display="none";
							return true;
						}else {
							errorZXS.style.display="block";
							errorZXS.innerHTML="请选择所在的省/市/区/直辖市";
							return false;
						}
					});
					let shen=addresParent.childNodes[0].options[addresParent.childNodes[0].selectedIndex].value;
						let errorZXS=document.querySelector("#errorZXS");
						if (shen!='省/直辖市'){
							errorZXS.style.display="none";
							return true;
						}else {
							errorZXS.style.display="block";
							errorZXS.innerHTML="请选择所在的省/市/区/直辖市";
							return false;
						}

			}
			//详情地址
			function myXQdizhi(){
				let dizhi=document.querySelector("#useraddress").value;
				let errordz=document.querySelector("#errorJD");
				let nul=/^\s*$/;
				if (!nul.test(dizhi)){
					errordz.style.display="none";
					return true;
				}else {
					errordz.style.display="block";
					errordz.innerHTML="请详细填写地址";
					return false;
				}
			}



			/* "返回顶部漂浮"  当鼠标滚动到一定位置 会有一个导航漂浮起来   */
			window.onscroll = function() {
				//设置scrollTop > 300，即滚动条向下滚动，div的内容上方超出浏览器最顶部位置>300。
				//获取元素在页面的高度
				let scrollTop = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop;

				/* 返回顶部的盒子 ====*/
				if(scrollTop>=600){
					$(".site-gotop").css("display","block");
				}else{
					$(".site-gotop").css("display","none");
				}
			}
			function pageScroll(){
				//把内容滚动指定的像素数（第一个参数是向右滚动的像素数，第二个参数是向下滚动的像素数）
				window.scrollBy(0,-100);
				//延时递归调用，模拟滚动向上效果
				scrolldelay = setTimeout('pageScroll()',50);
				//获取scrollTop值，声明了DTD的标准网页取document.documentElement.scrollTop，否则取document.body.scrollTop；因为二者只有一个会生效，另一个就恒为0，所以取和值可以得到网页的真正的scrollTop值
				let sTop=document.documentElement.scrollTop+document.body.scrollTop;
				console.log("sTop",sTop)
				//判断当页面到达顶部，取消延时代码（否则页面滚动到顶部会无法再向下正常浏览页面）
				if(sTop==0) clearTimeout(scrolldelay);
			}
			/* 下单并支付的点击事件，如果没有地址，则弹出提示“没有选择地址”*************************/
			$(function () {

				$(".paybottom").click(function () {
					let a1=document.querySelector(".a1");
					if (a1==null){
						$("#beijing").fadeIn("300");
						$("#wen").html("您还没有选择地址！");
					}else {
						window.location.replace("/order?v=create");//用户点击回退按钮时，将不会再跳转到该页面。
					}
				});
				/*给提示框绑定点击事件 消失===========*/
				$("#beijing").click(function (){
					$(this).fadeOut("300");
				});

			})
		</script>
	</head>
	<body>
		<div id="beijing" style="display: none">
			<div id="hei"></div>
			<div id="context">
				<dl>
					<dd>提示：</dd>
					<dd id="wen">账号或密码错误！</dd>
				</dl>
			</div>
		</div>
		<!-- 头部区域开始 -->
		<div class="header clearfix">
			<div class="mzcontainer">
				<div class="header-logo">
					<a target="_blank" href="/index" alt="魅族科技" class="logo-link">
						<img src="../../img/MEIZU.png"
							style="max-width: 125px;margin-top: 28px;"
						>
					</a>
				</div>
				<ul class="header-bread">
					<li class="bread-block">购物车</li>
					<li class="bread-block active">确认订单</li>
					<li class="bread-block">在线支付</li>
					<li class="bread-block">完成</li>
				</ul>
				<ul class="header-right">
					<li class="right-item">
						<a href="#" class="right-link">我的订单</a>
					</li>
					<li class="member sigin">
						<a href="#" class="member-link">
							<span class="member-username">${user.uname}</span>
							的商城
							<i class="member-triangle"></i><!-- 两个(上下)三角 -->
						</a>
						<ul class="member-downmenu">
							<li class="downmenu-item">
								<a href="#" class="downmenu-lint">地址管理</a>
							</li>
							<li class="downmenu-item">
								<a href="#" class="downmenu-lint">我的收藏</a>
							</li>
							<li class="downmenu-item">
								<a href="#" class="downmenu-lint">我的回购金</a>
							</li>
							<li class="downmenu-item">
								<a href="#" class="downmenu-lint">问题反馈</a>
							</li>
							<li class="downmenu-item">
								<a href="#" class="downmenu-lint  exit">退出</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- 头部区域结束 -->

		<!-- 主体区域开始 -->
		<div class="cart" id="cart">
			<!-- 地址部分 -->
			<div class="address">
				<p class="megtitle" style="display: inline-block;">收货人信息</p>
				<c:if test="${empty aa}">
					<p class="msg" style="color: red;display: inline-block;">${error}</p>
				</c:if>
				<ul class="ul">
					<c:forEach items="${address}" var="a" varStatus="j">
						<li class="a1" id="${a.aid}" onclick="reviseMR(${a.aid})">
							<div class="addtop">
								<p class="p1">${a.aname}</p>
								<p class="p2">${a.dzphone}</p>
							</div>
							<p class="xiangxi">${a.sheng}${a.shi}${a.qu}${a.jie}${a.adetail}</p>
							<c:if test="${a.set_default==1}">
								<img class="gouxuan" src="../../img/订单确认/选中.png">
							</c:if>
							<div class="changehover">
								<a onclick="xiugai(${a.aid})">修改</a>&nbsp;&nbsp;&nbsp;
								<a onclick="delet(${a.aid})">删除</a>
							</div>
						</li>
					</c:forEach>
					<%--<li class="a1">
						<div class="addtop">
							<p class="p1">苍玉藻</p>
							<p class="p2">19958369007</p>
						</div>
						<p class="xiangxi">湖南省株洲市天元区嵩山路街道黑龙江路金域天下3期</p>
						<img class="gouxuan" src="../../img/订单确认/选中.png">
						<div class="changehover"><a>修改</a>&nbsp;&nbsp;&nbsp;<a>删除</a></div>
					</li>--%>

					<li class="add-add-box"  onclick="xinzheng()">
						<div class="jiahao"></div>
						<div class="add-address">
							添加新地址
						</div>
					</li>
				</ul>

			</div>
			<!-- 商品详情部分 -->
			<div class="mzcontainer">
				<div class="protitle">确认订单信息</div>
				<!-- 商品详情表头 -->
				<table class="cart-header">
					<tbody>
						<tr style="background-color: #f6f8fb;">
							<td class="cart-col-select">
								<div class="select-title">
									供应商：魅族
								</div>
							</td>
							<td class="cart-col-price">单价</td>
							<td class="cart-col-number">数量</td>
							<td class="cart-col-total">小计</td>
							<td class="cart-col-edit-t">配送方式</td>
						</tr>
					</tbody>
				</table>
				<!-- 商品详情表体 -->
				<ul class="cart-list">
					<li class="cart-merchant">
						<table class="merchant-body">
							<c:forEach items="${cid}" var="c" varStatus="i">
								<tr class="cart-product">
									<td class="cart-col-select">
										<a class="product-link">
											<img src="${c.photo}" class="product-img"/>
										</a>
										<div class="product-link product-info">
											<p class="product-name">${c.gname}</p>
											<p class="product-desc">${c.dcolor}</p>
										</div>
									</td>
									<td class="cart-col-price">
										<p>
											<span class="product-price">￥${c.dprice}.00</span>
										<%--	<span class="ProductServlet-price orgin">￥${c.dprice}.00</span>--%>
										</p>
										<%--<div class="ProductServlet-discount">
										<span class="discount-title">
											限时折扣优惠:
											<span class="ProductServlet-price sub">-￥40.00</span>
										</span>
										</div>--%>
									</td>
									<td class="cart-col-number">
										<div class="number-adder">
											<div class="mz-adder">
												<div class="adder-num">${c.count}</div>
											</div>
										</div>
									</td>
									<td class="cart-col-total">
										<span class="product-price total">￥${(c.dprice)*c.count}.00</span>
									</td>
									<c:if test="${i.index==0}">
										<td class="cart-col-edit" rowspan="${len}">
											快递配送：运费<span>￥0.00</span>
										</td>
									</c:if>

								</tr>
							</c:forEach>
							<!-- 表尾 -->
							<tr class="allcount">
								<td class="fapiao">
									发票类型：电子发票
									<i>?</i>
									<a>修改</a>
									<p><span>发票抬头：默认为收货人姓名</span></p>
								</td>
								<td></td><td></td><td></td>
								<td class="heji">合计：<span>￥${hj}.00</span></td>
							</tr>
							<tr class="beizhulan">
								<td class="beizhutext">
									<span>备注</span>
									<input />
								</td>

							</tr>
						</table>
					</li>
				</ul>
			</div>
			<!-- 优惠抵扣 -->
			<div class="order-discount">
				<div class="order-header">使用优惠抵扣</div>
				<div><img src="../../img/订单确认/优惠.png"/></div>
			</div>
			<!-- 支付 -->
			<div class="paybox">
				<div class="order-header">选择支付方式</div>
				<div class="paybox-two">
					<img src="../../img/订单确认/支付方式.png"/>
				</div>
				<div class="paymessage">
					<div class="order-total-row">
						总金额：
						<p>￥${hj}.00</p>
					</div>
					<div class="order-total-row">
						运费
						<p>￥0.00</p>
					</div>
					<div class="line"></div>
					<div class="order-total-row">
						应付：
						<p class="shouldpay">￥${hj}.00</p>
					</div>
					<a><div class="paybottom" >
						下单并支付
					</div></a>
					<div>
						<img class="note" src="../../img/订单确认/商品提示.png">
					</div>
				</div>
			</div>

		</div>
		<!-- 主体区域结束 -->
		<div class="bobox">
			<div class="bottom">
				<c:import url="foot.jsp"></c:import>
			</div>
		</div>
		<!-- 地址栏 -->
		<div class="addboxs" style="display: none;">
			<div class="addr-add-box">
				<div class="close-tips">
					<div class="addr-title">添加新地址</div>
					<button class="close">×</button>
				</div>
				<div class="change">
					<div class="op">
						<div>
							收货人姓名
							<span>*</span>
						</div>
						<div>
							<input type="text" id="username" placeholder="姓名长度不超过15个文字" maxlength="15" oninput="myname()">
						</div>
						<div id="errormz"></div>
						<div>
							收货人手机号
							<span>*</span>
						</div>
						<div>
							<input type="text" id="userphone" placeholder="请输入11位手机号" maxlength="11" oninput="mydianhua()">
						</div>
						<div id="errordh"></div>
					</div>
					<div class="op">
						<div>
							收货人地址
							<span>*</span>
						</div>
						<div>
							<div id="addresParent" class="city-picker-select"></div>
						</div>
						<div id="errorZXS"></div>
					</div>
					<div class="op">
						<div class="xiangxi-a">
							详细地址
							<span>*</span>
						</div>
						<div class="xiangxi-add">
							<input type="text" id="useraddress" placeholder="请输入不少于4不超过50个字的详细地址，例如：路名，门牌号" minlength="4" maxlength="50" oninput="myXQdizhi()"/>
						</div>
						<div id="errorJD"></div>
					</div>
				</div>
				<div class="submit">
					<button class="sure" style="z-index: 2;" onclick="insert()">确认</button>
					<button class="sure" style="background-color:#008cff;color: white;position: absolute;left: 275px;z-index: -1;" onclick="xiugaiDZ()">修改</button>
					<button class="close">取消</button>
				</div>
			</div>
		</div>

		<!-- app二维码 和 返回顶部 -->
		<div class="site-gotop">
			<a href="#" class="gotop-suggest" title="建议反馈"></a>
			<div class="gotop-arrow" title="回到顶部" onclick="pageScroll()"></div>
		</div>
	</body>
	<script src="/js/jquery-3.5.1.min.js"></script>
	<script src="/js/streets-data.min.js"></script>
	<script src="/js/cityPicker-2.0.5.js"></script>
	<script type="text/javascript">
		$(function () {
			//原生城市-联动
			let select = $('.city-picker-select').cityPicker({
				dataJson: cityData,
				renderMode: false,
				storage: false,
				autoSelected: true,
				level: 4,
				onChoiceEnd: function() {
					console.log(this.values)
				}
			});

			// 设置城市
			/*select.setCityVal(', , , ');*/
		});
	</script>
</html>