<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
	<head>
		<meta charset="utf-8">
		<title>购物车-我的购物车*</title>
		<!-- 引入页签图片（标题前的小图片） -->
		<link href="//store.res.meizu.com/layout/img/favicon-90c2e618ff.ico" rel="shortcut icon" />
		<!-- 引入取出默认样式的css -->
		<link rel="stylesheet" href="../../css/base.css"/>
		<!-- 引入css样式 -->
		<link rel="stylesheet" href="../../css/cart.css"/>
		<script src="/js/jquery-3.5.1.min.js"></script>
		<script src="/js/cart.js"></script>
		<script>history.go(1);</script><%--让其无法回退，去第一个页面--%>
		<script>
			$(function () {
				<c:if test="${empty carts}" var="op">
					$(".empty").show();				 /*没有商品的空购物车*/
					$(".mzcontainer .full").toggle();
					$(".cart-footer").toggle();
				</c:if>
				<c:if test="${not op}">
					$(".empty").toggle();
					$(".mzcontainer .full").show();/*有商品的购物车*/
					$(".cart-footer").show();
				</c:if>
			})
			/*删除 “x” 的点击事件的删除方法*/
			function deleteCart(cid){
				if(confirm("您确定要删除该商品吗？")){
					$.ajax({
						url: "/cart",
						type: "post",
						data: {
							"v":"deleteCart",
							cid
						},
						dataType: "json",
						success:(resp=>{
							document.getElementById(cid).remove();
							$(".merchant-body").innerHTML="";
							/*let html=``;
							resp.cartList.forEach(s=>{
								html+=`<tr class="cart-ProductServlet">
										<td class="cart-col-select">
											<div class="select-all">
												<input type="checkbox" name="check" class="mz-checkbok" checked="checked" onclick="check(this)"/>
											</div>
											<a class="ProductServlet-link" onclick="toDetail(\${c.did})">
												<img src="\${s.cphoto}" class="ProductServlet-img"/>
											</a>
											<a class="ProductServlet-link ProductServlet-info" onclick="toDetail(\${c.did})">
												<p class="ProductServlet-name">\${s.gname}</p>
												<p class="ProductServlet-desc">\${s.dcolor}&nbsp;\${s.dversion}</p>
											</a>
										</td>
										<td class="cart-col-price">
											<p>
												<span class="ProductServlet-price">￥<i>\${s.dprice}</i>.00</span>
											</p>
										</td>
										<td class="cart-col-number">
											<div class="number-adder">
												<p class="number-max" style="display: none;">限购5件</p>
												<div class="mz-adder">
													<button class="adder-subtract">
													</button>
													<div class="adder-num">
														<input type="number" class="adder-input" value="\${s.count}" name="count" oninput="if(value>5)value=5;if(value.length>1)value=value.slice(0,1);if(value<1)value=1" />
													</div>
													<button class="adder-add">
													</button>
												</div>
											</div>
										</td>
										<td class="cart-col-total">
											<span class="ProductServlet-price total">￥<i class="xiaoji">\${s.dprice*s.count}</i>.00</span>
										</td>
										<td class="cart-col-edit">
											<div class="ProductServlet-removex" onclick="deleteCart(\${s.cid})">x</div>
										</td>
									</tr>`;
							})*//*这个回显不要，效果也是一样的，主要是，回显的也不是这里面的代码，因为这里我没有给+-号添加点击事件，然而我删除一个商品后，它的+-号有加点击事件，也就是还是一开始的代码，并不是这里面回显的代码*/
							if(resp.cartList.length==0){
								$(".empty").show();				 /*没有商品的空购物车*/
								$(".mzcontainer .full").toggle();
								$(".cart-footer").toggle();
							}
							// $(".merchant-body").innerHTML=html;
							$(".cart-footer-num").eq(0).html(resp.s);
							$(".cart-footer-num").eq(1).html(0);
						})
					})
				}
			}
			/*删除选中的购物车商品*/
			function deleteCarts() {/*traditional:是否使用传统的方式浅层序列化。数组或jQuery对象会按照name/value对进行序列化，普通对象按照key/value对进行序列化。//默认false*/
				if(confirm("您确定要删除选中商品吗？")){
					var cids = new Array();
					var listCheck = document.querySelectorAll('[name="check"]');//单选框
					for (let i = 0; i < listCheck.length; i++) {
						if(listCheck[i].checked == true){
							var cid = $(listCheck[i]).parents("tr").attr("id");//购物车id
							cids.push(cid);
							$(listCheck[i]).parents("tr").remove();
						}
					}
					$.ajax({
						url: "/cart",
						type: "post",
						traditional:true, //默认false
						data: {
							"v":"deleteCarts",
							cids
						},
						dataType: "json",
						success:(resp=>{
							$(".merchant-body").innerHTML="";
							if(resp.cartList.length==0){
								$(".empty").show();				 /*没有商品的空购物车*/
								$(".mzcontainer .full").toggle();
								$(".cart-footer").toggle();
							}
							$(".cart-footer-num").eq(0).html(resp.s);
							$(".cart-footer-num").eq(1).html(0);
						})
					})

				}
			}
			/* “-” 的点击事件的数量减少方法*/
			function subtract(zhe){/*下一个同胞元素的子元素*/
				// $("zhe").parent().prev().css("display","none");//限购
				var count = parseInt($(zhe).next().children().val())
				if(count>1) {
					//找到当前点击对象的下一个同胞元素的子元素的value值,数量加1
					var addcount = parseInt(count) - 1;
					//找到当前点击对象的父亲元素tr，获取tr的id属性值
					var cid = $(zhe).parents("tr").attr("id");
					count = $(zhe).next().children().val(addcount);
					var price =parseInt($(zhe).parents("tr").children().eq(1).children().children().children().html());
					var xiaoji = $(zhe).parents("tr").children().eq(3).children().children().html(price*addcount);
					// var flag = $(zhe).parents(".merchant-body").prev().children().eq(0).children().is(":checked");	//判断全选，前期只做全选用上了
					var flag2 = -1;
					var flag3 = $(zhe).parents("tr").children().eq(0).children().eq(0).children().is(":checked");

					changCount(flag2,flag3,price,addcount, cid);


					$(zhe).parent().prev().css("display","none")
				}
				/*对减号进行操作变灰色*/
				if($(zhe).next().children().val() == 1 ){
					$(zhe).addClass("disabled");
				}
			}
			/* “+” 的点击事件的数量增加方法*/
			function add(zhe){/*上一个同胞元素的子元素*//*用cid传进来，然后$(this)获取不到点击的DOM对象*/
				/*$(zhe)当前点击的button的DOM结构*/
				var count = $(zhe).prev().children().val()
				if(count<5) {
					//找到当前点击对象的上一个同胞元素的子元素的value值,数量加1
					var addcount = parseInt(count) + 1;
					//找到当前点击对象的父亲元素tr，获取tr的id属性值
					var cid = $(zhe).parents("tr").attr("id");
					count = $(zhe).prev().children().val(addcount);//数量
					var price =parseInt($(zhe).parents("tr").children().eq(1).children().children().children().html());	//单价
					var xiaoji = $(zhe).parents("tr").children().eq(3).children().children().html(price*addcount);		//小计
					// var flag = $(zhe).parents(".merchant-body").prev().children().eq(0).children().is(":checked");		//判断全选，前期只做全选用上了
					var flag2 = 1;
					var flag3 = $(zhe).parents("tr").children().eq(0).children().eq(0).children().is(":checked");
					changCount(flag2,flag3,price,addcount, cid);
					$(zhe).prev().prev().removeClass("disabled");//限购
				}
				if($(zhe).prev().children().val() == 5){
					$(zhe).parent().prev().css("display","block");
				}

			}
			/*加减数量，合计的回显与数据库数据更改*/
			function changCount(flag2,flag3,price,count, cid) {//flag2判断是+还是-，flag3判断当前点击对象是否被选中
				$.ajax({
					url: "/cart",
					type: "post",
					data: {
						v:"updateCartCount",
						cid,count
					},
					dataType: "json"
				})
				/*if (flag){//判断全选的
					var total = 0;	//合计
					var number = 0;
					var $mz = $(".mz-checkbok");//获取所有复选框，含有不含数量和价格的去掉前面两个和最后一个
					var $price = $(".ProductServlet-price .price");
					var $count = $(".adder-input");
					for (let i = 0; i < $mz.length-3; i++) {
						var price = $price.eq(i).html();
						var count = $count.eq(i).val();
						total += parseFloat(price)*parseFloat(count);
					}
					$(".footer-total").html(total);
					$(".cart-footer-num").html();
				}*/
				/*for (let i = 0; i < listCheck.length; i++) {
					if (listCheck[i].checked == true) {
						var num = $(".cart-footer-num").eq(1).html();			  //已选商品数量
						var heji = $(".footer-total").html();
						if(flag2>0){
							number = parseInt(num)+1;
							total = parseInt(heji)+parseInt(price);
						}else {
							number = parseInt(num)-1;
							total = parseInt(heji)-parseInt(price);
						}
						$(".footer-total").html(total);
						break;
					}
				}*/
				var listCheck = document.querySelectorAll('[name="check"]');//单选框
				var number = 0,total = 0;
				var num1 = $(".cart-footer-num").eq(0).html();	//共多少件商品
				if(flag2>0){
					number1 = parseInt(num1)+1;
				}else {
					number1 = parseInt(num1)-1;
				}
				if(flag3){
					var num = $(".cart-footer-num").eq(1).html();			  //已选商品数量
					var heji = $(".footer-total").html();
					if(flag2>0){
						number = parseInt(num)+1;
						total = parseInt(heji)+parseInt(price);
					}else {
						number = parseInt(num)-1;
						total = parseInt(heji)-parseInt(price);
					}
					$(".footer-total").html(total);
					$(".cart-footer-num").eq(1).html(number);
				}
				$(".cart-footer-num").eq(0).html(number1);
			}

			/* 头部跳转--退出 */
			$(function () {
				$(".exit").click(function () {
					location.href="/user";
				})
			})
			/* 跳转商品详情页 */
			function toDetail(did) {
				open("/detail?did="+did);
			}
		</script>
	</head>
	<body>
		<!-- 头部区域开始 -->
		<div class="header clearfix">
			<div class="mzcontainer">
				<div class="header-logo">
					<a target="_blank" href="/index" alt="魅族科技" class="logo-link">
						<img src="../../img/store/MEIZU.png"
							style="max-width: 125px;margin-top: 28px;"
						>
					</a>
				</div>
				<ul class="header-bread">
					<li class="bread-block active">购物车</li>
					<li class="bread-block">确认订单</li>
					<li class="bread-block">在线支付</li>
					<li class="bread-block">完成</li>
				</ul>
				<ul class="header-right">
					<li class="right-item">
						<a href="/order?v=show" target="_blank" class="right-link">我的订单</a>
					</li>
					<li class="right-item siginout">
						<a href="#" class="right-link">登录</a>
					</li>
					<li class="right-item siginout">
						<a href="#" class="right-link">注册</a>
					</li>
					<!-- 未登录藏起来的，登录显示的部分 -->
					<li class="member sigin">
						<a href="#" class="member-link">
							<span class="member-username">${user.uname}</span>
							的商城
							<i class="member-triangle"></i><!-- 两个(上下)三角 -->
						</a>
						<ul class="member-downmenu">
							<li class="downmenu-item">
								<a href="/addressServlet" target="_blank" class="downmenu-lint">地址管理</a>
							</li>
							<li class="downmenu-item">						
								<a href="#" class="downmenu-lint">我的收藏</a>
							</li>							
							<li class="downmenu-item">																						
								<a href="#" class="downmenu-lint">我的回购金</a>
							</li>							
							<li class="downmenu-item">																			
								<a href="#" class="downmenu-lint">问题反馈</a>
							</li>							
							<li class="downmenu-item">																					
								<a class="downmenu-lint exit">退出</a>
							</li>
						</ul>
					</li>
				</ul>				
			</div>
		</div>
		<!-- 头部区域结束 -->
		
		<!-- 购物车区域开始 -->
		<div class="cart" id="cart">
			<!-- 未登录提示 -->
			<div class="tips" style="display: none">
				<i class="tips-icon">i</i>				
				您还没有登录！登录后可显示您账号中已加入的商品哦~
				<a href="#" class="mz-btn s">去登录</a>
				<div class="close-tips"></div>
			</div>
			
			<div class="mzcontainer">
				<!-- 未登录的空购物车 -->
				<div class="empty" style="">
					<img src="img/store/emptyCartOrigin.png" style="width: 100%; height: 100%;"/>
				</div>			
				
				<!-- 未登录有商品的购物车 -->
				<div class="full" style="">
					<table class="cart-header">
						<tbody>
							<tr>
								<td class="cart-col-select">
									<div class="select-all">
										<label>
										<input type="checkbox" name="checks" class="mz-checkbok"  checked="checked" onclick="checkAll(this.checked)"/>
										<span class="select-title">全选</span>
										</label>
									</div>
								</td>
								<td class="cart-col-price">单价(元)</td>
								<td class="cart-col-number">数量</td>
								<td class="cart-col-total">小计(元)</td>
								<td class="cart-col-edit" id="cartedit">编辑</td>
							</tr>
						</tbody>
					</table>
					<ul class="cart-list">
						<li class="cart-merchant">
							<div class="merchant-header">
								<div class="select-all">
									<label>
									<input type="checkbox" name="checks" class="mz-checkbok" checked="checked" onclick="checkAll(this.checked)"/>
									<span class="select-title">魅族</span>
									</label>
								</div>
								<div class="select-fee">
									<span class="fee">已免运费</span>
								</div>
							</div>
							<table class="merchant-body">
								<c:forEach items="${carts}" var="c">
<%--									<input type="hidden" name="did" value="${c.did}">&lt;%&ndash;详情id，选中的通过表单传给确认订单&ndash;%&gt;--%>
<%--									<input type="hidden" name="counts" value="">&lt;%&ndash;数量，选中的通过表单传给确认订单&ndash;%&gt;--%>
									<tr class="cart-product" id="${c.cid}">
										<td class="cart-col-select">
											<div class="select-all">
												<input type="checkbox" name="check" class="mz-checkbok" checked="checked" onclick="check(this)"/>
											</div>
											<a class="product-link" onclick="toDetail(${c.did})">
												<img src="${c.cphoto}" class="product-img"/>
											</a>
											<a class="product-link product-info" onclick="toDetail(${c.did})">
												<p class="product-name">${c.gname}</p>
												<p class="product-desc">${c.dcolor}&nbsp;${c.dversion}</p>
											</a>
										</td>
										<td class="cart-col-price">
											<p>
												<span class="product-price">￥<i class="price">${c.dprice}</i>.00</span>
											</p>
										</td>
										<td class="cart-col-number">
											<div class="number-adder">
												<c:if test="${c.count == 5}" var="mop">
												<p class="number-max">限购5件</p>
												</c:if>
												<c:if test="${not mop}">
													<p class="number-max" style="display: none;">限购5件</p>
												</c:if>
												<div class="mz-adder">
													<c:if test="${c.count == 1}" var="cop">
													<button class="adder-subtract disabled" onclick="subtract(this)"><%--type="button"type="button"--%>
														<%--减号--%>
													</button>
													</c:if>
													<c:if test="${not cop}">
														<button class="adder-subtract" onclick="subtract(this)"><%--type="button"type="button"--%>
																<%--减号--%>
														</button>
													</c:if>
													<div class="adder-num">
<%--														<input type="text" class="adder-input" value="${c.count}"/>--%>
														<input type="number" class="adder-input" value="${c.count}" name="count" oninput="if(value>5)value=5;if(value.length>1)value=value.slice(0,1);if(value<1)value=1" />
													</div>
													<button class="adder-add" onclick="add(this)">
														<%--加号--%>
													</button>
												</div>
											</div>
										</td>
										<td class="cart-col-total">
											<span class="product-price total">￥<i class="xiaoji">${c.dprice*c.count}</i>.00</span>

										</td>
										<td class="cart-col-edit">
<%--											<div class="ProductServlet-remove" style="display:none;">--</div>--%>
											<div class="product-removex" onclick="deleteCart(${c.cid})">x</div>
										</td>
									</tr>
								</c:forEach>
							</table>
						</li>
					</ul>
				</div>
			</div>
			<div class="cart-footer" style="">
				<div class="mzcontainer">
					<div class="cart-footer-left">
						<div class="select-all">
							<label>
							<input type="checkbox" name="checks" class="mz-checkbok" checked="checked" onclick="checkAll(this.checked)"/>
							<span class="select-title">全选</span>
							</label>
						</div>
						<span class="cart-remove-selected" onclick="deleteCarts()">删除选中的商品</span>
						<span class="cart-footer-count">
							共
							<span class="cart-footer-num">${s}</span>
							件商品，已选择
							<span class="cart-footer-num blue">${s}</span>
							件
						</span>
					</div>
					<div class="cart-footer-right">
						<span class="cart-footer-sum">
							已优惠
							<span class="red">0.00</span>
							元，合计(不含运费)：
							<span class="cart-footer-total">
								￥<span class="footer-total">${total}</span>.00
							</span>         
						</span>
						<div class="mz-btn settle to-order"><%-- disabled="disabled"为选择商品为这个  onclick="toSettle()"--%>
							去结算        
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 购物车区域结束 -->
		
		<!-- 底部区域开始 -->
		<div class="footer">
			<div class="footer-link">
				<div class="wrap">
					<div class="row clearfix">
						<ul class="service">
							<li><a href="#"><img src="../../img/store/service1.png"><span>满80免运费</span></a></li>
							<li><a href="#"><img src="../../img/store/service2.png"><span>100+ 城市次日送达</span></a></li>
							<li><a href="#"><img src="../../img/store/service3.png"><span>7天无理由退货</span></a></li>
							<li><a href="#"><img src="../../img/store/service4.png"><span>15天换货保障</span></a></li>
							<li><a href="#"><img src="../../img/store/service5.png"><span>1年免费保修</span></a></li>
							<li><a href="#"><img src="../../img/store/service6.png"><span>上门快修</span></a></li>
						</ul>
						<div class="onlive-server">
								<span>周一至周日 8:00-24:00</span>
								<p class="tel">400-788-3333</p>
								<a href="#" class="onlive-btn">在线客服</a>
							</div>
					</div>
						<hr>
						<div class="co">
							<div class="row clearfix">
								<div class="footer-info">
									<div class="info-left">
										<ul>
											<li><a href="#">了解魅族</a></li>
											<li><a href="#">加入我们</a></li>
											<li><a href="#">联系我们</a></li>
											<li><a href="#">线下专卖店</a></li>
											<li><a href="#">线上销售授权名单</a></li>
											<li><a href="#">隐私政策</a></li>
											<li><a href="#">涉网络暴力有害信息举报</a></li>
											<li><a href="#">涉未成年人网络有害信息举报</a></li>
											<li><a href="#">涉养老诈骗有害信息举报</a></li>
											<li class="no-border">
												<a href="#" class="language">
													<i class="icon-logo"></i>
													简体中文
												</a>
												<div class="modal-div">
													<ul class="modal-list">
														<li>
															<a href="">
																<i style="background-image:url(../../img/lang-logos.png);background-position: 0 0;"></i>
																<span>简体中文</span>
															</a>
														</li>
														<li>
															<a href="">
																<i style="background-image:url(../../img/lang-logos.png);background-position: 0 -20px;"></i>
																<span>繁體中文</span>
															</a>
														</li>
														<li>
															<a href="">
																<i style="background-image:url(../../img/lang3.png);background-position: 0 0;"></i>
																<span>English</span>
															</a>
														</li>
														<li>
															<a href="">
																<i style="background-image:url(../../img/lang-logos.png);background-position: 0px -80px;"></i>
																<span>Русский</span>
															</a>
														</li>
													</ul>
												</div>
											</li> 
										</ul>
										<div class="clearfix"></div>
										<div class="certificate">
											©2023 Meizu Telecom Equipment Co., Ltd. All Rights Reserved.
											<a href="">粤ICP备13003602号-2</a>
											<a href="">合字B2-20170010 </a>
											<a href="">营业执照</a>
											<a href="">法律声明</a>
											<a href="">粤公网安备 44049102496009 号</a>
										</div>
									</div>
									<div class="subscribe">
										<a href="">
											<img src="../../img/store/sub-img1.png"/>
											<i class="sub-img" style="background-size: 96%;"></i>
										</a>
										<span></span>
										<a href="" class="weixin">
											<img src="../../img/store/sub-img2.jpg"/>
											<i class="sub-img" style="background-size: 96%;"></i>
										</a>
										<span></span>
										<a href="">
											<i class="sub-img" style="background-size: 96%;"></i>
										</a>
										<span></span>
										<a href="">
											<i class="sub-img" style="background-size: 96%;"></i>
										</a>
										<span></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 底部区域结束 -->
		
		<!-- 返回顶部 -->
		<div class="site-gotop">
			<a href="#" class="gotop-suggest" title="建议反馈"></a>
			<div class="gotop-arrow" title="回到顶部" onclick="pageScroll()"></div>
		</div>
	</body>
	<script src="../../js/jquery-3.5.1.min.js"></script>
	<script>
		/* "商品下单条"  当鼠标滚动到一定位置 会有一个导航漂浮起来   */
		window.onscroll = function() {
			//设置scrollTop > 300，即滚动条向下滚动，div的内容上方超出浏览器最顶部位置>300。
			//获取元素在页面的高度
			var scro = document.querySelector(".cart-list").scrollHeight;
			var cartFooter = document.querySelector(".cart-footer");
			let scrollTop = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop;
			if (scrollTop > scro-400) {							
				$(".cart-footer").attr("class","cart-footer");
			} else {
				$(".cart-footer").attr("class","cart-footer fixed");
			}
			
			/* 返回顶部的盒子 ====*/
			if(scrollTop>=600){
				$(".site-gotop").css("display","block");
			}else{
				$(".site-gotop").css("display","");
			}
		}
	</script>
	<script>
		/*点击返回顶部*/
		function pageScroll(){
		    //把内容滚动指定的像素数（第一个参数是向右滚动的像素数，第二个参数是向下滚动的像素数）
		    window.scrollBy(0,-100);
		    //延时递归调用，模拟滚动向上效果
		    scrolldelay = setTimeout('pageScroll()',50);
		    //获取scrollTop值，声明了DTD的标准网页取document.documentElement.scrollTop，否则取document.body.scrollTop；因为二者只有一个会生效，另一个就恒为0，所以取和值可以得到网页的真正的scrollTop值
		    var sTop=document.documentElement.scrollTop+document.body.scrollTop;
		    //判断当页面到达顶部，取消延时代码（否则页面滚动到顶部会无法再向下正常浏览页面）
		    if(sTop==0) clearTimeout(scrolldelay);
		}


	</script>
</html>