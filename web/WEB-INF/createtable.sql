-- noinspection LossyEncodingForFile

/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2023/3/20 14:33:18                           */
/*==============================================================*/


create database if not exists meizu;

drop table if exists tb_address;

drop table if exists tb_order_detail;

drop table if exists tb_cart;

drop table if exists tb_photo;

drop table if exists tb_detail;

drop table if exists tb_goods;

drop table if exists tb_goodsSort;

drop table if exists tb_order;

drop table if exists tb_user;

drop table if exists tb_htuser;

/*==============================================================*/
/* Table: tb_address 地址表                                      */
/*==============================================================*/
create table tb_address
(
   aid                  int not null auto_increment     comment '地址ID',
   uid                  int not null                    comment '用户ID',
   aname                varchar(50) not null            comment '收货人名字',
   sheng                varchar(30) not null            comment '省',
   shi                  varchar(16) not null            comment '市',
   qv                   varchar(30) not null            comment '区',
   jie                  varchar(50) not null            comment '街道',
   adetail              varchar(150) not null           comment '详细地址',
   dzphone              char(11) not null               comment '收货人电话',
   set_default          tinyint   not null  default '0' comment '设置默认',
   primary key (aid)
);

/*==============================================================*/
/* Table: tb_cart  购物车表                                      */
/*==============================================================*/
create table tb_cart
(
   cid                  int not null auto_increment     comment '购物车ID',
   did                  int not null                    comment '商品详情ID',
   uid                  int not null                    comment '用户id',
   cphoto               text not null                   comment '图片',
   count                int not null default 1          comment '数量',
   primary key (cid)
);

/*==============================================================*/
/* Table: tb_detail 商品详情表                                        */
/*==============================================================*/
create table tb_detail
(
   did         int not null auto_increment     comment '商品详情ID',
   gid         int not null                    comment '商品ID',
   dprice      int not null                    comment '价格',
   dcolor      varchar(8)                      comment '颜色',
   dversion    varchar(50)                     comment '版本',
   primary key (did)
);

/*==============================================================*/
/* Table: tb_goods  商品表                                            */
/*==============================================================*/
create table tb_goods
(
   gid                  int not null auto_increment     comment '商品ID',
   sid                  int not null                    comment '商品类别ID',
   gname                varchar(20) not null            comment '商品名字',
   gdetail              text                            comment '商品详情',
   gdiscount            text                            comment '优惠信息',
   gphoto               text not null                   comment '商品主图',
   gstate               tinyint  default 1              comment '商品状态',
   primary key (gid)
);

/*==============================================================*/
/* Table: tb_goodsSort   商品类别表                                       */
/*==============================================================*/
create table tb_goodsSort
(
   sid                  int not null auto_increment     comment '商品类别ID',
   sname                varchar(20) not null            comment '类别名称',
   sphoto               varchar(50)                     comment '类别图片',
   primary key (sid)
);

/*==============================================================*/
/* Table: tb_order     订单表                                         */
/*==============================================================*/
create table tb_order
(
   oid     bigint not null auto_increment         comment '订单ID',
   uid     int not null                        comment '用户ID',
   otime   timestamp default now() not null    comment '订单时间',
   ostate  tinyint not null default 0          comment '订单状态',
   aid     int not null                        comment '地址ID',
   cnum    varchar(50)                         comment '快递单号',
   primary key (oid)
);

/*==============================================================*/
/* Table: tb_order_detail   订单详情表                                    */
/*==============================================================*/
create table tb_order_detail
(
   xqid        int not null auto_increment     comment '订单详情ID',
   oid         bigint not null                 comment '订单ID',
   count       int not null                    comment '商品详情数量',
   did         int not null                    comment '商品详情ID',
   primary key (xqid)
);

/*==============================================================*/
/* Table: tb_photo        图片表                                      */
/*==============================================================*/
create table tb_photo
(
   pid                  int not null auto_increment     comment '图片ID',
   did                  int not null                    comment '商品详情ID',
   photo                text not null                   comment '图片地址',
   primary key (pid)
);

/*==============================================================*/
/* Table: tb_user      用户表                                         */
/*==============================================================*/
create table tb_user
(
   uid                  int not null auto_increment                     comment '用户ID',
   uname                varchar(50) default 'user_'                     comment '用户名',
   uheadshot            varchar(150) default '../img/people/head.png'   comment '用户头像',
   uemail               varchar(30)                                     comment '用户邮箱',
   uphone               char(11) not null                               comment '用户手机号',
   upassword            varchar(16) not null                            comment '用户密码',
   ustate               tinyint default 0                               comment '用户状态',
   primary key (uid),
   unique key AK_Key_2 (uphone)
);

/*==============================================================*/
/* Table: tb_htuser      后台管理员表                                         */
/*==============================================================*/
create table tb_htuser
(
    htuid                  int not null auto_increment                     comment '后台用户ID',
    htuname                varchar(50) not null                            comment '后台用户名',
    uphone                 char(11) not null                               comment '后台用户手机号',
    upassword              varchar(16) not null                            comment '后台用户密码',
    primary key (htuid)
);

alter table tb_address add constraint FK_Reference_5 foreign key (uid)
      references tb_user (uid) on delete restrict on update restrict;

alter table tb_cart add constraint FK_Reference_4 foreign key (did)
      references tb_detail (did) on delete restrict on update restrict;
alter table tb_cart add constraint FK_Reference_8 foreign key (uid)
    references tb_user (uid) on delete restrict on update restrict;

alter table tb_detail add constraint FK_Reference_2 foreign key (gid)
      references tb_goods (gid) on delete restrict on update restrict;

alter table tb_goods add constraint FK_Reference_1 foreign key (sid)
      references tb_goodsSort (sid) on delete restrict on update restrict;

alter table tb_order_detail add constraint FK_Reference_6 foreign key (oid)
      references tb_order (oid) on delete restrict on update restrict;

alter table tb_order add constraint FK_Reference_9 foreign key (uid)
    references tb_user (uid) on delete restrict on update restrict;

alter table tb_order_detail add constraint FK_Reference_7 foreign key (did)
      references tb_detail (did) on delete restrict on update restrict;

alter table tb_photo add constraint FK_Reference_3 foreign key (did)
      references tb_detail (did) on delete restrict on update restrict;
alter table tb_order auto_increment=50000;
