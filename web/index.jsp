<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/3/16
  Time: 15:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>静态页面查看</title>
</head>
<body>
    <p>
        <a href="/allServlet">订单页面</a>
    </p>
    <p>
        <a href="/allServlet?v=goodsList">商品列表页面</a>
    </p>
    <p>
        <a href="/allServlet?v=Login">登录页面</a>
    </p>
    <p>
        <a href="/allServlet?v=Register">注册页</a>
    </p>
    <p>
        <a href="/index" style="color: red">首页</a>
    </p>
    <p>
        <a href="/allServlet?v=orderSure">订单确认页</a>
    </p>
    <p>
        <a href="/allServlet?v=Payment">付款页</a>
    </p>
    <p>
        <a href="/allServlet?v=cart">购物车页面</a>
    </p>
    <p>
        <a href="/allServlet?v=detail">商品详情页</a>
    </p>
    <p>
        <a href="/allServlet?v=Address">地址管理</a>
    </p>
    <p>
        <a href="/allServlet?v=UserInfo">个人中心</a>
    </p>
</body>
</html>
