/*底部图片显示================*/
$(function () {
    let img=document.querySelectorAll(".sub-img");
    let num=1;
    let src;
    let src2;
    let num2=11;
    img.forEach(x=>{
        src="img/store/sub"+num+".png";
        src2="img/store/sub"+num2+".png";
        x.setAttribute("onmouseover","this.style.backgroundImage='url("+src2+")'" );
        x.setAttribute("onmouseout","this.style.backgroundImage='url("+src+")'");
        x.style.backgroundImage= "url("+src+")";
        num++;
        num2++;
    })
})

/*全选 : 数量统计，合计统计================*/
/*
function checkAll(checks){
    var listCheck = document.querySelectorAll(".mz-checkbok");//获取所有复选框
    var listPrice = document.querySelectorAll(".product-price .price");//单价
    var listCount = document.querySelectorAll(".adder-input");//数量
    for (let i = 0; i < listCheck.length; i++) {
        listCheck[i].checked = checks;//所有所有复选框全部勾上
    }
    var total = 0,number = 0;//合计，已选商品数量
    if(checks == true){
        for (let i = 0; i < listCheck.length-3; i++) {
            var price = listPrice[i].innerHTML;
            var count = listCount[i].value;
            total += parseFloat(price)*parseFloat(count);
            number += parseInt(count);
        }
    }else {
        total = 0;
        number = 0;
    }
    $(".cart-footer-total").html("￥"+total+".00");//合计
    $(".cart-footer-num").html(number);			  //已选商品数量
}
*/
/*全选 =================================*/
function checkAll(checks){
    var listCheck = document.querySelectorAll(".mz-checkbok");//获取所有复选框
    for (let i = 0; i < listCheck.length; i++) {
        if(checks == true){
            listCheck[i].checked = true;//所有所有复选框全部勾上
        }else {
            listCheck[i].checked = false;//所有所有复选框全部勾上
        }
        check();
    }
}

/*任选===============================*/
function check(){
    var listCheck = document.querySelectorAll('[name="check"]');//单选框
    var listCheckAll = document.querySelectorAll('[name="checks"]');//全选框
    var listPrice = document.querySelectorAll(".product-price .price");//单价
    var listCount = document.querySelectorAll(".adder-input");//数量
    var total = 0,number = 0,changdu = 0;
    for (let i = 0; i < listCheck.length; i++) {
        if(listCheck[i].checked == true){
            var price = listPrice[i].innerHTML;
            var count = listCount[i].value;
            total += parseFloat(price)*parseFloat(count);
            number += parseInt(count);
            changdu++;
        }
    }
    //全选勾上和不勾上的情况
    if(changdu == listCheck.length){
        for (let i = 0; i < listCheckAll.length; i++) {
            listCheckAll[i].checked = true;//所有所有复选框全部勾上
        }
    }else {
        for (let i = 0; i < listCheckAll.length; i++) {
            listCheckAll[i].checked = false;//所有所有复选框全部勾上
        }
    }
    $(".footer-total").html(total);//合计
    $(".cart-footer-num").eq(1).html(number);			  //已选商品数量
    if(changdu == 0){
        $(".to-order").attr("disabled","disabled");
        $(".settle").unbind("click");
        // $(".to-order").removeClass("settle");
    }else {
        $(".to-order").removeAttr("disabled");
        // $(".to-order").addClass("settle");
        $(".settle").bind('click', function() {
            var listCheck = document.querySelectorAll('[name="check"]');//单选框
            var url = "/orderSureServlet?v=excutrr";
            for (let i = 0; i < listCheck.length; i++) {
                if(listCheck[i].checked == true){
                    var cid = $(listCheck[i]).parents("tr").attr("id");//购物车id
                    url += "&cid="+ cid;
                    console.log("url:", url);
                }
            }
            open(url);
        });
    }
}

/*去结算 ==============================*/
//给去结算添加点击事件，转发到订单确认
// $(".to-order").click(function () {
//     var listCheck = document.querySelectorAll('[name="check"]');//单选框
//     for (let i = 0; i < listCheck.length; i++) {
//         if (listCheck[i].checked == true){
//             var count = $(listCheck[i]).parents("tr").children().eq(2).children().children().eq(0).val;
//             console.log("count:",count)
//             $(listCheck[i]).parents("tr").prev().val(count);
//         }
//     }
//     $("#form").submit();
// })
//
/*function toSettle() {//不用表单提交了，用字符串拼接传参
    /!*var listCheck = document.querySelectorAll('[name="check"]');//单选框
    for (let i = 0; i < listCheck.length; i++) {
        if (listCheck[i].checked == true){
            var count = $(listCheck[i]).parents("tr").children().eq(2).children().children().eq(0).val;
            console.log("count:",count)
            $(listCheck[i]).parents("tr").prev().val(count);
        }
    }*!/
    var listCheck = document.querySelectorAll('[name="check"]');//单选框
    var url = "/order?v=";
    for (let i = 0; i < listCheck.length; i++) {
        if(listCheck[i].checked == true){
            var cid = $(listCheck[i]).parents("tr").attr("id");//购物车id
            url += "&cid="+ cid;
            console.log("url:", url);
        }
    }
    open(url);
}*/
$(function () {//
    $(".cart-footer .settle").click(function () {
        var listCheck = document.querySelectorAll('[name="check"]');//单选框
        var url = "/orderSureServlet?v=excutrr";
        for (let i = 0; i < listCheck.length; i++) {
            if(listCheck[i].checked == true){
                var cid = $(listCheck[i]).parents("tr").attr("id");//购物车id
                url += "&cid=" + cid;
                console.log("url:", url);
            }
        }
        open(url);
    })
})