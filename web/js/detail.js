$(function (){
    /*默认第一个版本和颜色为选中状态*/
    $(".set-sale .version a:nth-child(1)").attr("class","selected");
    $(".set-sale .color a:nth-child(1)").attr("class","selected");
    /*给版本添加点击事件*/
    $(".set-sale .version a").click(function (){
        $(".set-sale .version a").css("border-color","#d6d6d6");
        $(this).css("border-color","#008cff");
    })
    /*给每个框框添加点击事件改变颜色*/
    $(".set-sale .color a").click(function (){
        $(".set-sale .color a").css("border-color","#d6d6d6");
        $(this).css("border-color","#008cff");
    })
    /*给减号绑定点击事件*/
    $(".minus").click(function (){
        var count = $("[name=count]").val();
        if(count>1){
            var sum = parseInt(count)-1;//累减
            count=$("[name=count]").val(sum);
            var danjia = $(".mod-money").html();
            var total = danjia*count.val();
            $("#total_price").html(`<i>￥</i> `+total);
            $(".vm-price").html(`<i>￥</i> `+total);
            $(".amount").html(count.val())
        }
    })
    /*给加号绑定点击事件*/
    $(".plus").click(function (){
        var count = $("[name=count]").val();
        if(count<5){
            var sum = parseInt(count)+1;//累加
            count=$("[name=count]").val(sum);
            var danjia = $(".mod-money").html();
            var total = danjia*count.val();
            $("#total_price").html(`<i>￥</i> `+total);
            $(".vm-price").html(`<i>￥</i> `+total);
            $(".amount").html(count.val())
        }
    })
})