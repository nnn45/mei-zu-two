package com.meizu.control;

import com.meizu.entity.Address;
import com.meizu.entity.Order;
import com.meizu.entity.OrderDetail;
import com.meizu.entity.User;
import com.meizu.service.AddressService;
import com.meizu.service.OrderService;
import com.meizu.utils.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
/**
 * 查看订单详情 的控制层操作类
 * @date 2023/4/18
 **/
@WebServlet("/orderdetail")
public class OrderDetailServlet extends BaseServlet {
    OrderService os=new OrderService();
    AddressService as=new AddressService();
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Object user=session.getAttribute("user");
        if (user instanceof User){
            return "r:/orderdetail?v=show";
        }else {
            return "r:/user";//跳首页
        }
    }
    //单份订单的详情展示
    public String show(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Object user=session.getAttribute("user");
        if (user instanceof User){//未登录，返回登录页
            String o=request.getParameter("oid");//获取订单ID
            String a=request.getParameter("aid");//获取地址ID
            if(o!=null && !"".equals(o) && a!=null && !"".equals(a)){//订单ID和地址ID有误，返回首页
                Integer oid=Integer.valueOf(o);
                Integer aid=Integer.valueOf(a);
                Order order1= os.one(oid);
                List<OrderDetail> orderDetails=os.selectOneOrder(oid);//通过订单ID获取一份订单的所有订单详情
                if (orderDetails.size()>0 && ((User) user).getUid().equals(order1.getUid())){
                    Order order=os.one(oid);//获取订单信息（状态）
                    Address address= as.selectOne(aid);//获取地址
                    request.setAttribute("order",order);
                    request.setAttribute("ods",orderDetails);
                    request.setAttribute("add",address);
                    return "f:orderdetail";
                }
                return "f:admin/error";
            }
            return "r:/index";
        }else {
            return "r:/user";
        }
    }
}
