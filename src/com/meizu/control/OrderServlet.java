package com.meizu.control;

import com.alibaba.fastjson.JSONObject;
import com.meizu.entity.Cart;
import com.meizu.entity.Order;
import com.meizu.entity.User;
import com.meizu.service.CartService;
import com.meizu.service.OrderService;
import com.meizu.utils.BaseServlet;
import com.mysql.jdbc.StringUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
 * 订单中心 的控制层操作类
 * @date 2023/4/18
 **/
@WebServlet("/order")
public class OrderServlet extends BaseServlet {
    OrderService os=new OrderService();
    CartService cs = new CartService();
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Object user=session.getAttribute("user");
        if (user instanceof User){
            User u = (User) user;
            //避免头部购物车回显完，刷新页面回显又不见的问题
            List<Cart> cartList=null;//购物车商品集合
            Integer s=0;//购物车商品数量
            //拿到前五个购物车信息
            cartList= cs.selectByIndexUid (u.getUid ( ));//头部购物车的显示（最多5件）
            //拿到购物车的总件数
            s= cs.selectByUid(u.getUid ( )).stream ( ).collect (Collectors.summingInt (Cart::getCount));//购物车的商品总件数
            session.setAttribute ("cartList",cartList);
            session.setAttribute ("b",s);
            return "r:/order?v=show";
        }else {
            return "r:/user";
        }
    }
    //订单中心展示订单详情
    public String show(HttpServletRequest request,HttpServletResponse response)throws Exception {
        Object user=session.getAttribute("user");
        if (user instanceof User){
            User u=(User) user;
            Integer uid= u.getUid();
            String s=request.getParameter("s");
            Integer state=99;//99全部订单，0代付款，1待发货，2已发货，9其它
            if (s!=null && !"".equals(s)){
                state=Integer.valueOf(s);
            }
            List<Order> orders= os.selectAllOrder(uid,state);
            request.setAttribute("orders",orders);
            request.setAttribute("s",state);
            return "f:dingdan";
        }else {
            return "r:/user";
        }
    }
    //确认订单
    public String sure(HttpServletRequest request,HttpServletResponse response)throws Exception{
        String id=request.getParameter("oid");
        if (id!=null && !"".equals(id)) {
            Integer oid = Integer.valueOf(id);
            Integer r = os.change(oid, 1);
            if (r>0){
                return "r:/order?v=show";
            }
        }
        return "r:/index";
    }
    //取消订单
    public String cancel(HttpServletRequest request,HttpServletResponse response)throws Exception{
        String id=request.getParameter("oid");
        if (id!=null && !"".equals(id)) {
            Integer oid = Integer.valueOf(id);
            Integer r = os.change(oid, -1);
            if (r>0){
                return "r:/order?v=show";
            }
        }
        return "r:/index";
    }
    //取消订单 ajax
    public String cancelA(HttpServletRequest request,HttpServletResponse response)throws Exception{
        String id=request.getParameter("oid");
        if (id!=null && !"".equals(id)){
            Integer oid=Integer.valueOf(id);
            Integer r=os.change(oid,-1);
            JSONObject json=new JSONObject();
            if (r>0){
                json.put("flag",-1);
                json.put("oid",oid);
            }
            return json.toJSONString();
        }else {
            return "r:/user";
        }
    }
    //点击确认并支付后，新增订单和对应订单详情，计算总价，跳转支付v=pay
    public String create(HttpServletRequest request,HttpServletResponse response) throws Exception{
        Object user=session.getAttribute("user");
        Integer total=0,oid=0;
        if (user instanceof User){
            User u=(User) user;
            Integer uid= u.getUid();//用户ID
            Integer aid=(Integer)session.getAttribute("addressNum");//地址ID
            Object count=session.getAttribute("count");//商品数量
            Integer did=(Integer)session.getAttribute("did");//商品详情ID
            Object carts=session.getAttribute("cid");//购物车ID数组
            if (count!=null && !"".equals(count)){
                Integer c=(Integer) count;
                total=os.count1(did,c);
                oid= os.create1(uid,aid,c,did);
                session.setAttribute("count",null);
            }else {
                Integer[] cids=(Integer[]) carts;
                total=os.count2(cids);
                oid=os.create2(uid,aid,cids);
                session.setAttribute("cid",null);
            }
                session.setAttribute("hj",total);
                return "r:/order?v=pay&oid="+oid;
        }else {
            return "r:/user";
        }
    }
    //支付操作，跳转付款页
    public String pay(HttpServletRequest request,HttpServletResponse response) throws Exception{
        Object u=session.getAttribute("user");
        if (u instanceof User){
            User  user=(User) u;
            Object money=session.getAttribute("hj");
            if (money instanceof Integer){
                Integer total=(Integer) money;
                String oId=request.getParameter("oid");
                Integer oid=Integer.valueOf(oId);
                request.setAttribute("total",total);
                return "r:/paymentServlet?oid="+oid+"&total="+total;
            }
            return "r:/index";
        }else {
            return "r:/user";
        }
    }
}
