package com.meizu.control;

import com.alibaba.fastjson.JSONObject;
import com.meizu.entity.*;
import com.meizu.service.CartService;
import com.meizu.service.DetailService;
import com.meizu.service.GoodsService;
import com.meizu.service.PhotoService;
import com.meizu.utils.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 商品详情的控制层操作类
 * @author hxf
 * @date 2023/3/23
 **/
@WebServlet("/detail")
public class DetailServlet extends BaseServlet {
    DetailService ds = new DetailService();
    GoodsService gs = new GoodsService();
    PhotoService ps = new PhotoService();
    CartService cs=new CartService ();
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String id = request.getParameter("did");//从首页携带过来//1,10,13,22,24
        Integer did = Integer.parseInt(id);
        session.setAttribute("did",did);//详情id，给订单确认和订单创建
        //根据详情id查询商品名字
        Goods good = gs.selectConDetailById(did);//gname,gdetail,gdiscount,gphoto
        if(good != null){//避免用户直接在链接上修改did，而找不到数据报错
            String gname = good.getGname();
            Detail price = ds.selectPrice(did);
            List<Photo> photos = ps.selectPhoto(did);
            List<Detail> color = ds.selectColor(did);
            List<Detail> version = ds.selectVersion(did);
            Detail Gid = ds.selectGidByDid(did);
            Integer gid = Gid.getGid();
            request.setAttribute("good",good);
            request.setAttribute("price",price);
            request.setAttribute("photos",photos);
            request.setAttribute("color",color);
            request.setAttribute("version",version);
            session.setAttribute("shopid",gid);//商品id
            session.setAttribute("shopname",gname);//商品名字
            List<Cart> cartList=null;//购物车商品集合
            Integer s=0;//购物车商品数量
            User user=(User)session.getAttribute ("user");
            if (user!=null){//避免头部购物车回显完，刷新页面回显又不见的问题
                //拿到前五个购物车信息
                cartList= cs.selectByIndexUid (user.getUid ( ));//头部购物车的显示（最多5件）
                //拿到购物车的总件数
                s= cs.selectByUid(user.getUid ( )).stream ( ).collect (Collectors.summingInt (Cart::getCount));//购物车的商品总件数
                session.setAttribute ("cartList",cartList);
                session.setAttribute ("b",s);
            }
            return "f:detail";
        }else {//跳错误页面
            return "f:admin/error";
        }
    }

    /**
     *点击颜色刷新图片
     */
    public String colRefreshPho(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer shopid = (Integer) session.getAttribute("shopid");
        String shopname = (String) session.getAttribute("shopname");
        String dcolor = request.getParameter("yanse");
        String dversion = request.getParameter("banben");
        Detail detail;
        if(!"".equals(dversion)){//有版本选项
            detail = ds.selectDidByCondition(shopid, dcolor, dversion);//根据条件查详情id
        }else {//没有版本选项
            detail = ds.selectDidByConditionTwo(shopid, dcolor);
        }
        JSONObject json = new JSONObject();
        if(detail != null){
            try {
                Integer did = detail.getDid();
                session.setAttribute("did", did);//详情id
                List<Photo> photos;
                if (!"".equals(dversion)) {
                    photos = ps.selectPhotoByCondition(shopid, dcolor, dversion);//根据条件查详情图片-----可以改成根据详情id查图片了
                } else {
                    photos = ps.selectPhotoByConditionTow(shopid, dcolor);//根据条件查详情图片-----可以改成根据详情id查图片了
                    Detail price = ds.selectPriceByConditionTwo(shopid, dcolor);//根据条件查价格------可以改成更加详情id查价格
                    json.put("colorPrice",price);
                }
                String photo = photos.get(0).getPhoto();
                session.setAttribute("shopphoto", photo);//商品图片（给购物车）
                json.put("photo", photos);
                json.put("color", dcolor);
                json.put("version", dversion);
                json.put("shopname", shopname);
                json.put("flag",1);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }else {//没有该详情
            json.put("flag",-1);
        }
        return json.toJSONString();
    }

    /**
     *点击版本刷新价格
     */
    public String verRefreshPri(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer shopid = (Integer) session.getAttribute("shopid");
        String shopname = (String) session.getAttribute("shopname");
        String dcolor = request.getParameter("yanse");
        String dversion = request.getParameter("banben");
        Detail detail = ds.selectDidByCondition(shopid, dcolor, dversion);//根据条件查详情id
        JSONObject json = new JSONObject();
        if(detail != null){
            Integer did = detail.getDid();
            session.setAttribute("did",did);//详情id
            Detail price = ds.selectPriceByCondition(shopid, dcolor,dversion);//根据条件查价格------可以改成更加详情id查价格
            json.put("price",price);
            json.put("color",dcolor);
            json.put("version",dversion);
            json.put("shopname",shopname);
        }else {
            json.put("flag",-1);
        }
        return json.toJSONString();

    }
    /*想合并点击颜色刷新图片点击版本刷新价格 没用上*/
    /*public String getSkuInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer shopid = (Integer) session.getAttribute("shopid");
        String shopname = (String) session.getAttribute("shopname");
        String dcolor = request.getParameter("yanse");
        String dversion = request.getParameter("banben");
        String chose = request.getParameter("chose");
        Detail detail;
        JSONObject json = new JSONObject();
        if(Integer.valueOf(chose)==1){
            if(!"".equals(dversion)){
                detail = ds.selectDidByCondition(shopid, dcolor, dversion);//根据条件查详情id
            }else {
                detail = ds.selectDidByConditionTwo(shopid, dcolor);
            }
            if(detail != null){
                try {
                    Integer did = detail.getDid();
                    session.setAttribute("did", did);//详情id
                    List<Photo> photos;
                    if (!"".equals(dversion)) {
                        photos = ps.selectPhotoByCondition(shopid, dcolor, dversion);//根据条件查详情图片-----可以改成根据详情id查图片了
                    } else {
                        photos = ps.selectPhotoByConditionTow(shopid, dcolor);//根据条件查详情图片-----可以改成根据详情id查图片了
                    }
                    String photo = photos.get(0).getPhoto();
                    session.setAttribute("shopphoto", photo);//商品图片（给购物车/订单）

                    json.put("photo", photos);
                    json.put("color", dcolor);
                    json.put("version", dversion);
                    json.put("shopname", shopname);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                json.put("flag",-1);
            }
        }
        return json.toJSONString();
    }*/

}
