package com.meizu.control;

import cn.hutool.core.util.NumberUtil;
import com.alibaba.fastjson.JSONObject;
import com.meizu.entity.Cart;
import com.meizu.entity.Goods;
import com.meizu.entity.User;
import com.meizu.service.CartService;
import com.meizu.service.SearchService;
import com.meizu.utils.BaseServlet;
import com.meizu.utils.Pager;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;
/**
 * 搜索列表
 * @date 2023/4/18
 **/
@WebServlet("/search")
public class SearchServlet extends BaseServlet {
    SearchService ss=new SearchService();
    CartService cs = new CartService();
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String search=request.getParameter("search");
        String o=request.getParameter("order");
        Integer order=0;

        if (o!=null){
            order=Integer.valueOf(o);
        }
        if (search==null || "".equals(search)){
            search="魅族 18s";
        }
        Integer pageNo=1,pageSize=3;
                //调用业务类分页
        Pager pager=ss.selectPager(pageNo,pageSize,search,order);
        //将分页对象储存到作用域
        request.setAttribute("search",search);
        request.setAttribute("pager",pager);
        request.setAttribute("order",order);
        List<Cart> cartList=null;//购物车商品集合
        Integer s=0;//购物车商品数量
        User user=(User)session.getAttribute ("user");
        if (user!=null){//避免头部购物车回显完，刷新页面回显又不见的问题
            //拿到前五个购物车信息
            cartList= cs.selectByIndexUid (user.getUid ( ));//头部购物车的显示（最多5件）
            //拿到购物车的总件数
            s= cs.selectByUid(user.getUid ( )).stream ( ).collect (Collectors.summingInt (Cart::getCount));//购物车的商品总件数
            session.setAttribute ("cartList",cartList);
            session.setAttribute ("b",s);
        }
        return "f:goodslist";
    }
    /**
     *分页查询 展示商品信息
     **/
    public JSONObject rightPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType ("application/json;charset=utf-8");
        String search=null;
        String sear=request.getParameter("search");
        String o=request.getParameter ("order");
        Integer order=0;//升序和降序判断
        if (o!=null){
            order=Integer.valueOf(o);
        }
        if(sear==null||"".equals(sear)){
            search="魅族 18s";
        }else if(sear!=null){
            search=sear;
        }
        Integer pageNo=1,pageSize=3;
        String No=request.getParameter("pageNo");
        if (No!=null) {
            if (NumberUtil.isInteger (No)){
                pageNo = Integer.parseInt (No);
            }
        }
        //调用业务类分页
        Pager pager=ss.selectPager(pageNo,pageSize,search,order);
        //将分页对象储存到作用域
        JSONObject json=new JSONObject();
        json.put ("search",search);
        json.put ("pager",pager);
        json.put ("order",order);
        return json;
    }
}