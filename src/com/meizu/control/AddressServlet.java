package com.meizu.control;

import com.alibaba.fastjson.JSONObject;
import com.meizu.dao.UserDao;
import com.meizu.entity.Address;
import com.meizu.entity.User;
import com.meizu.service.AddressService;
import com.meizu.service.UserService;
import com.meizu.utils.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/addressServlet")
@MultipartConfig
public class AddressServlet extends BaseServlet {
    AddressService as=new AddressService();
    UserDao us=new UserDao();
    //默认方法展示地址页面
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Object u=session.getAttribute("user");
        if (u instanceof User){
            User user=(User) u;
            List<Address> address=as.selectAll(user.getUid());//查询该用户所有的地址
            Address count=as.selectCount(user.getUid());//查询该用户的地址条数
            System.out.println("进入excute方法！！！");
            //将集合对象address存储在request作用域里面
            request.setAttribute("add",address);
            request.setAttribute("count",count);
            return "f:address";
        }
        return "r:/user";
    }
    //新增地址
    public JSONObject doInsertAddress(HttpServletRequest request, HttpServletResponse response) throws Exception {
        User user=(User)session.getAttribute("user");
        response.setContentType("application/json;charset=utf-8");
        //获取ajax参数值
        String name=request.getParameter("name");
        String phone=request.getParameter("phone");
        String sheng=request.getParameter("sheng");
        String shi=request.getParameter("shi");
        String qu=request.getParameter("qu");
        String jie=request.getParameter("jie");
        String dizhi=request.getParameter("dizhi");
        String set_default=request.getParameter("set_default");
        //将其它的默认值为1修改为0；
        if (Integer.parseInt(set_default)==1){
            //根据用户ID查询出默认地址1
            Address ad=as.selectMrzYi(user.getUid());
            if(ad!=null){
                //将原默认地址1修改为普通地址0
                as.udateMoren(user.getUid());
            }
        }
        //将这些变量整合到一个地址对象里面
        Address address=new Address(user.getUid(),name,sheng,shi,qu,jie,dizhi,phone,Integer.parseInt(set_default));
        //调用insertAll方法
        JSONObject json=new JSONObject();
        //据用户id查询用户地址总条数
        Address dzNum=as.selectCount(user.getUid());
        if (dzNum.getCount()<10){
            Integer r=as.insertAll(address);
            json.put("dzNUm",dzNum.getCount());
        }else {
            json.put("dzNUm",dzNum.getCount());
        }
        List<Address> dz1=as.selectAll(user.getUid());
        Address count=as.selectCount(user.getUid());
        String a=JSONObject.toJSONString(dz1);
        json.put("akey",a);
        json.put("cckey",count.getCount());
        //将已存在的地址数量存入session；
//        session.setAttribute("adressNum",count.getCount());
        return json;

    }
    //根据aid删除地址信息
    public JSONObject dodDeleteAddress(HttpServletRequest request, HttpServletResponse response) throws Exception {
        User user=(User)session.getAttribute("user");
        String del=request.getParameter("id");
        Integer Id=Integer.parseInt(del);
        Integer i=as.deleteByaid(Id);//进入删除
        JSONObject json=new JSONObject();
        Address count=as.selectCount(user.getUid());//回显的地址数量
        json.put("cout",count.getCount());
        return json;
    }
    //地址框点击修改的回显
    public String selectOneAddress(HttpServletRequest request, HttpServletResponse response) throws Exception{
        response.setContentType("application/json;charset=utf-8");
        String id=request.getParameter("id");
        //将id存入session，更新地址使用
        session.setAttribute("ID",id);
        Integer Id=Integer.parseInt(id);
        Address  selone=as.selectOne(Id);//根据地址ID查询一条地址
        return JSONObject.toJSONString(selone);
    }
    //更新地址
    public String doUpdateAddress(HttpServletRequest request, HttpServletResponse response) throws Exception{
        response.setContentType("application/json;charset=utf-8");
        User user=(User)session.getAttribute("user");
        Object id=session.getAttribute("ID");
        Integer Id=Integer.parseInt(id.toString());
        String name=request.getParameter("name");
        String phone=request.getParameter("phone");
        String sheng=request.getParameter("sheng");
        String shi=request.getParameter("shi");
        String qu=request.getParameter("qu");
        String jie=request.getParameter("jie");
        String dizhi=request.getParameter("dizhi");
        String set_default=request.getParameter("set_default");
        //将其它的默认值为1修改为0；
        if (Integer.parseInt(set_default)==1){
            Address ad=as.selectMrzYi(user.getUid());
            if (ad!=null) {
                as.udateMoren(user.getUid());
            }
        }
        Address address=new Address(Id,user.getUid(),name,sheng,shi,qu,jie,dizhi,phone,Integer.parseInt(set_default));
        System.out.println(address.getAid());
        //修改地址
        Integer r=as.updateByaid(address);
        //修改后重新查询用户的所有地址进行回显
        as.selectAll(user.getUid());
       /* Address selone=new Address() ;*/
        String json=JSONObject.toJSONString(as.selectAll(user.getUid()));
        return json;
    }
    //地址页面点击直接修改默认地址
    public String updateMorenzhi(HttpServletRequest request, HttpServletResponse response) throws Exception{
        response.setContentType("application/json;charset=utf-8");
        User user=(User)session.getAttribute("user");
        String id =request.getParameter("id");
        Integer Id=Integer.parseInt(id);//修改的地址的ID
        Address ad=as.selectMrzYi(user.getUid());
        if (ad !=null) {
            as.udateMoren(user.getUid());//将默认地址改为普通地址
        }
        as.updateDefault(1,Id);//将修改的地址设为默认
        //查询所有并回显
        return JSONObject.toJSONString(as.selectAll(user.getUid()));
    }
}
