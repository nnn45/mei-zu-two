package com.meizu.control;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.json.JSON;
import com.alibaba.fastjson.JSONObject;
import com.meizu.dao.UserDao;
import com.meizu.entity.Cart;
import com.meizu.entity.Goods;
import com.meizu.entity.GoodsSort;
import com.meizu.entity.User;
import com.meizu.service.CartService;
import com.meizu.service.GoodsService;
import com.meizu.service.UserService;
import com.meizu.utils.BaseServlet;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
 * 首页的控制层操作类
 * @date 2023/4/18
 **/
@WebServlet("/index")
public class IndexServlet extends BaseServlet {
    GoodsService gs=new GoodsService ();
    CartService cs=new CartService ();
    private List<Cart> cartList=null;
    private Integer s=0;//头部购物车的商品数量
    JSONObject json=new JSONObject ();
    //默认方法展示
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<GoodsSort> goodsSorts=gs.selectGoodsSort ();//头部用商品集合
        List<GoodsSort> goodsSorts1=gs.selectGoodsSortAsc ();//首页用商品集合
        for(GoodsSort goodsSort : goodsSorts1) {
            goodsSort.setList (gs.selectGoodsDetailQT(goodsSort.getSid()));//插入商品集合
            System.out.println (gs.selectGoodsDetailQT (goodsSort.getSid ( )));
        }
        //拿到session中的用户对象

        User user=(User)session.getAttribute ("user");
        if (user!=null){
            //拿到前五个购物车信息
            cartList= cs.selectByIndexUid (user.getUid ( ));//头部购物车的显示（最多5件）
            //拿到购物车的总件数
            s= cs.selectByUid(user.getUid ( )).stream ( ).collect (Collectors.summingInt (Cart::getCount));//购物车的商品总件数
        }
        session.setAttribute ("goodsSorts",goodsSorts);
        session.setAttribute ("goodsSort",goodsSorts1);
        System.out.println ("集合"+goodsSorts );
        System.out.println ("adsdsfsfsfsfsffe"+goodsSorts1 );
        session.setAttribute ("cartList",cartList);
        session.setAttribute ("b",s);
        return "f:meizu";
    }
    //头部商品列表的商品展示 Ajax
    public String goodList(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String sid=request.getParameter ("id");//商品类别ID
        Integer Sid=0;
        response.setContentType ("application/json;charset=utf-8");
        if (NumberUtil.isNumber (sid)){
            Sid=Integer.parseInt (sid);//转换类型
        }
        List<Goods> goods=gs.selectGoodsDetailQT (Sid);//查询出商品类别
        String json= JSONObject.toJSONString (goods);//用JSON返回
       return json;
    }
    //从首页头部删除购物车中的商品
    public JSONObject deleteCart(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType ("application/json;charset=utf-8");
        String id=request.getParameter ("cid");//获取购物车ID
        Integer cid=Integer.valueOf (id);
        cs.deleteCartByCid (cid);//删除购物车中的商品信息
        User user=(User)session.getAttribute ("user");
        if (user!=null){//如果已经登录，重新查询购物车的商品和数量
            cartList=cs.selectByIndexUid(user.getUid());
            s=cs.selectByUid(user.getUid()).stream ().collect(Collectors.summingInt(Cart::getCount));
        }
        json.put ("cartLIst",cartList);
        json.put ("s",s);
        return json;
    }
    //头部购物车的内容回显 Ajax
    public JSONObject selectAllCart(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType ("application/json;charset=utf-8");
        User user=(User)session.getAttribute ("user");
        if (user!=null){//如果已经登录，查询购物车的商品和数量
            cartList=cs.selectByIndexUid(user.getUid());
            s=cs.selectByUid (user.getUid()).stream().collect(Collectors.summingInt(Cart::getCount));
        }
        json.put ("cart",cartList);
        json.put ("f",s);
        return json;

    }
}
