package com.meizu.control.admin;

import com.alibaba.fastjson.JSONObject;
import com.meizu.entity.Htuser;
import com.meizu.entity.User;
import com.meizu.service.UserService;
import com.meizu.utils.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 后台用户管理的控制层操作类
 * @author hxf
 * @date 2023/4/10
 * ustate=0,账号可用，1账号不可以用
 **/
@WebServlet("/ht/user")
public class HtUserServlet extends BaseServlet {
    UserService us = new UserService();
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //判断后台是否登录
        Object htuser = session.getAttribute("htuser");
        if(htuser instanceof Htuser){//判断是否登录
            List<User> users = us.selectAll();//所有用户信息
            request.setAttribute("users",users);
            return "f:admin/member-list1";
        }else {
            return "r:/htLogin";
        }
    }
    /**
     * 通过uid修改用户状态(启用和停用)
     * @Author hxf
     * @Date 2023/4/10 16:22
    **/
    public void updateState(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String id = request.getParameter("id");
        Integer isUse = Integer.valueOf(request.getParameter("isUse"));
        Integer uid = Integer.parseInt(id);
        if(isUse == -1){//修改成停用
            Integer r = us.updateState(-1, uid);//ustate=0,账号可用，-1账号不可以用
        }else {
            Integer r = us.updateState(0, uid);
        }
    }
    /**
     * 展示修改的用户信息
     * @Author hxf
     * @Date 2023/4/11 8:30
    **/
    public String editInfos(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer uid = Integer.valueOf(request.getParameter("uid"));//获取用户id
        System.out.println("sss:"+uid);
        session.setAttribute("htuid",uid);
        User user = us.selectByUid(uid);
        request.setAttribute("editUser",user);
        return "f:admin/member-edit";
    }
    /**
     * 修改用户信息
     * @Author hxf
     * @Date 2023/4/12 8:42
    **/
    public String editInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Object htuid = session.getAttribute("htuid");//获取用户id
        Integer uid = (Integer) htuid;
        String phone = request.getParameter("phone");
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String pwd = request.getParameter("repass");
        System.out.println(uid+","+phone+","+username+","+email+","+pwd);
        User user = new User(uid,username,email,phone,pwd);
        us.change(user);
        return "r:/ht/user";
    }
    /**
     * 重置密码
     * @Author hxf
     * @Date 2023/4/12 8:43
    **/
    public void resetPwd(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer uid = Integer.valueOf(request.getParameter("id"));//获取用户id
        us.resetPwd(uid);
    }
}
