package com.meizu.control.admin;

import com.meizu.entity.Address;
import com.meizu.entity.Htuser;
import com.meizu.entity.Order;
import com.meizu.entity.OrderDetail;
import com.meizu.service.AddressService;
import com.meizu.service.OrderService;
import com.meizu.utils.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
/**
 * 后台订单管理的控制层操作类
 * @date 2023/4/18
 **/
@WebServlet("/ht/order")
public class OrderListServlet extends BaseServlet {
    OrderService os=new OrderService();
    AddressService ad=new AddressService();
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Object user=session.getAttribute("htuser");
        if (user instanceof Htuser){//判断是否登录
            List<Order> list= os.selectOrders();
            for (Order order : list) {//插入地址信息
                order.setAddress(ad.selectOne(order.getAid()));
            }
            request.setAttribute("list",list);
            return "f:admin/order-list";//跳订单列表
        }
        return "f:admin/login";//跳后台的登录
    }
    /**
     *查看后台订单详情
     **/
    public String orderDHT(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String o=request.getParameter("oid");//获取订单ID
        String a=request.getParameter("aid");//获取地址ID
        if(o!=null && !"".equals(o) && a!=null && !"".equals(a)) {//订单ID和地址ID有误，返回首页
            Integer oid = Integer.valueOf(o);
            Integer aid = Integer.valueOf(a);
            List<OrderDetail> orderDetails = os.selectOneOrder(oid);//通过订单ID获取一份订单的所有订单详情
            Order order = os.one(oid);//获取订单信息（状态）
            Address address = ad.selectOne(aid);//获取地址
            session.setAttribute("order", order);
            session.setAttribute("htod", orderDetails);
            session.setAttribute("add", address);
            return "f:admin/orderdetailHT";
        }
        return "f:admin/error";
    }
    /**
     *展示发货页面
     **/
    public String toAdd(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String id=request.getParameter("oid");
        Integer oid=Integer.valueOf(id);
        request.setAttribute("oid",oid);
        return "f:admin/order-add";
    }
    /**
     *订单的确认发货(的表单提交)
     **/
    public String surePost(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String id=request.getParameter("oid");
        Integer oid=Integer.valueOf(id);
        String num=request.getParameter("number");
        os.change(oid,2);//修改为发货状态
        Integer r=os.addCNum(oid,num);
        return r.toString();
    }
}
