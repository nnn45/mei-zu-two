package com.meizu.control.admin;

import com.meizu.entity.GoodsSort;
import com.meizu.service.GoodssortService;
import com.meizu.utils.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
/**
 *后台商品分类管理的控制层操作类
 * @date 2023/4/18
 **/
@WebServlet("/ht/goodssort")
public class GoodssortServlet extends BaseServlet {
    GoodssortService gs=new GoodssortService();
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<GoodsSort> goodsSorts=gs.selectAll();
        request.setAttribute("goodsSort",goodsSorts);
        return "f:admin/sort-manage";//跳分类管理
    }
    /**
     *打开分类管理的修改界面
     **/
    public String goodssortUpdate(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer sid=Integer.parseInt(request.getParameter("sid"));
        session.setAttribute("htsid",sid);
        GoodsSort goodsSort=gs.selectOne(sid);
        session.setAttribute("goodsSort",goodsSort);
        return "f:admin/sort-update";
    }
    /**
     *修改分类名字（未控制分类名相同的问题）
     **/
    public String goodsSortUpdates(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Object htsid=session.getAttribute("htsid");
        Integer sid=(Integer)htsid;
        String name=request.getParameter("gsname");
        gs.updateGoodssort(sid,name);
        return "r:/ht/goodssort";
    }
}
