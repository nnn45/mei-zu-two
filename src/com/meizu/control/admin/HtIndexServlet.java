package com.meizu.control.admin;

import com.meizu.entity.Goods;
import com.meizu.entity.Htuser;
import com.meizu.entity.Order;
import com.meizu.entity.User;
import com.meizu.service.GoodsService;
import com.meizu.service.OrderService;
import com.meizu.service.UserService;
import com.meizu.utils.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * 后台架子 - 首页
 * @author hxf
 * @date 2023/4/12
 **/
@WebServlet("/ht/index")
public class HtIndexServlet extends BaseServlet {
    UserService us = new UserService();
    OrderService os = new OrderService();
    GoodsService gs = new GoodsService();
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Object htuser = session.getAttribute("htuser");
        if(htuser instanceof Htuser) {//判断是否登录
            Htuser user = (Htuser) session.getAttribute("htuser");
            session.setAttribute("htuname",user.getHtuname());
            return "f:/admin/index";//跳转后台主操作页
        }else {
            return "r:/htLogin";
        }
    }

    /**
     *后台 我的桌面展示
     */
    public String welcome(HttpServletRequest request, HttpServletResponse response) throws Exception {
        User user = us.countUser();
        request.setAttribute("member",user.getCount());
        Order order = os.countOrders();
        request.setAttribute("orderCount",order.getCount());
        Goods goods = gs.countGoods();
        request.setAttribute("goodsCount",goods.getCount());
        return "f:/admin/welcome";//操作页的默认进入欢迎页面
    }
    /**
     * 退出操作
     * @Author hxf
     * @Date 2023/4/12 16:12
    **/
    public String logout(HttpServletRequest request, HttpServletResponse response) throws Exception {
        session.removeAttribute("htuser");//移除登录状态
        return "r:/htLogin";//跳转后台登录
    }
}
