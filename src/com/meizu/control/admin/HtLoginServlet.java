package com.meizu.control.admin;

import com.meizu.entity.Htuser;
import com.meizu.service.HtuserService;
import com.meizu.utils.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 后台登录的控制层操作类
 * @date 2023/4/18
 **/
@WebServlet("/htLogin")
public class HtLoginServlet extends BaseServlet {
    HtuserService hs=new HtuserService();
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return "f:admin/login";//跳后台登录
    }

    /**
     *登录表单提交的验证
     */
    public String login(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String phone=request.getParameter("htphone");
        String pwd=request.getParameter("htpwd");
        Integer i=hs.HtuserLogin(phone,pwd);
        if (i==-1){
            request.setAttribute ("error","账号或密码错误");
            request.setAttribute ("display","block");
            return "f:admin/login";
        }else {
            Htuser htuser=hs.search(phone);
            session.setAttribute("htuser",htuser);//用于其它页面判断是否登录
            return "r:/ht/index"; //跳转后台主操作页
        }

    }
}
