package com.meizu.control.admin;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.NumberUtil;
import com.meizu.dao.DetailDao;
import com.meizu.entity.Detail;
import com.meizu.entity.Photo;
import com.meizu.service.DetailService;
import com.meizu.service.PhotoService;
import com.meizu.utils.BaseServlet;
import com.meizu.utils.PartUtil;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * 后台商品详情的控制层操作类
 * @date 2023/4/18
 **/
@WebServlet("/ht/productEdit")
@MultipartConfig
public class ProductDetailServlet extends BaseServlet {
    DetailService ds=new DetailService();
    DetailDao dd=new DetailDao();
    PhotoService ps=new PhotoService();
    String oldName,newName,stoPath,outPath;
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String Sid=request.getParameter("sid");
        String Gid=request.getParameter("gid");
        Integer gid=Integer.valueOf(Gid);
        Integer sid=Integer.valueOf(Sid);
        List<String> dversion=new ArrayList<>();//将版本定死3个
        dversion.add("全网通公开版 8+128GB");
        dversion.add("全网通公开版 8+256GB");
        dversion.add("全网通公开版 12+256GB");
        request.setAttribute("gid",gid);
        request.setAttribute("sid",sid);
        request.setAttribute("editVersion",dversion);
        return "f:admin/product-detail-add";//商品详情添加页面
    }
    /**
     *商品详情编辑的 展示页
     **/
    public String detailEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String id=request.getParameter("did");
        Integer did=Integer.valueOf(id);
        Detail detail=dd.selectXQ(did);
        detail.setGid(ds.selectGidByDid(did).getGid());
        List<Photo> photos=ps.selectPhoto(did);
        List<Detail> dversion=ds.selectVersion(did);
        request.setAttribute ("htDid",did);
        request.setAttribute("editDetail",detail);
        request.setAttribute("htPhotos",photos);
        request.setAttribute("editVersion",dversion);
        return "f:admin/product-detail-edit";
    }
    /**
     *商品详情新增（from表单提交）
     **/
    public String detailAdd(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String Gid=request.getParameter("gid");
        String dcolor=request.getParameter("dcolor");
        String dversion=request.getParameter("dversion");
        String dprice=request.getParameter("dprice");
        Integer gid=Integer.valueOf(Gid);
        Part part1= request.getPart("gphoto1");
        Part part2= request.getPart("gphoto2");
        Part part3= request.getPart("gphoto3");
        Part part4= request.getPart("gphoto4");
        List<Part> pList=new ArrayList<>();
        pList.add(part1);
        pList.add(part2);
        pList.add(part3);
        pList.add(part4);
        PartUtil util=new PartUtil();
        String realPath=request.getServletContext().getRealPath("/");
        Detail d= ds.selectDidByCondition(gid,dcolor,dversion);
        if (d==null){
            Integer did=ds.insertDetail(gid,dprice,dcolor,dversion);
            for (int i=0;i<4;i++){
                String p=util.uploadImage(pList.get(i),realPath);
                Photo photo=new Photo(did,p);
                ps.insertPhoto(photo);
            }
            request.setAttribute("addError","新增成功！*（^-^）*");
            return "f:admin/product-detail-add";
        }
        request.setAttribute("addError","新增失败，查到已有相同条件的详情存在,请新增其他详情！");
        return "f:admin/product-detail-add";
    }
    /**
     *商品详情编辑（修改）
     **/
    public String detailUpdate(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String dversion=request.getParameter ("dversion");
        String Did=request.getParameter ("did");
        Integer did=0;
        if (NumberUtil.isNumber (Did)){
            did=Integer.valueOf (Did);
        }
        String Dprice=request.getParameter ("dprice");
        Integer dprice=0;
        if (NumberUtil.isNumber (Dprice)){
            dprice=Integer.valueOf (Dprice);
        }
        String dcolor = request.getParameter ("dcolor");
        Part part1=request.getPart ("photo1");
        Part part2=request.getPart ("photo2");
        Part part3=request.getPart ("photo3");
        Part part4=request.getPart ("photo4");
        String img1=request.getParameter ("img1");
        String img2=request.getParameter ("img2");
        String img3=request.getParameter ("img3");
        String img4=request.getParameter ("img4");
        List<Part> list=new ArrayList<> ();
        list.add (part1);
        list.add (part2);
        list.add (part3);
        list.add (part4);
        String [] imgArr={img1,img2,img3,img4};
        PartUtil partUtil=new PartUtil ();
        String realPath = request.getServletContext().getRealPath("/");
        for (int i=0;i<imgArr.length;i++){
            if (list.get(i).getSize()!=0){
                imgArr[i]=partUtil.uploadImage (list.get (i),realPath);
                System.out.println (partUtil.uploadImage(list.get (i),realPath));
            }
        }
        System.out.println (Arrays.toString (imgArr));
        Detail detail=new Detail ();
        detail.setDid (did);
        detail.setDprice(dprice);
        detail.setDcolor (dcolor);
        detail.setDversion (dversion);
        System.out.println (detail );
        Detail detail1=ds.selectDetailOne (dcolor,dversion);
        System.out.println (detail1);
        if (detail1==null) {
            ds.updateGoodsDetail (detail);
            List<Photo> photoIdList=ps.selectPhotoId (did);
            System.out.println (photoIdList);
            ds.updatePhoto (imgArr[0], photoIdList.get (0).getPid ());
            ds.updatePhoto (imgArr[1], photoIdList.get (1).getPid ());
            ds.updatePhoto (imgArr[2], photoIdList.get (2).getPid ());
            ds.updatePhoto (imgArr[3], photoIdList.get (3).getPid ());
            return "<h3>修改成功😀😁😁</h3>";
        }else {
            return "<h3>详情信息修改失败,请重新修改😱😱😱😱😱</h3>";
        }
    }
}
