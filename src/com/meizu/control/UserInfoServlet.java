package com.meizu.control;

import cn.hutool.core.io.FileUtil;
import com.meizu.entity.User;
import com.meizu.service.UserService;
import com.meizu.utils.BaseServlet;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * 个人中心的控制层操作类
 * @date 2023/4/18
 **/
@WebServlet("/userinfo")
@MultipartConfig
public class UserInfoServlet extends BaseServlet {
    UserService us=new UserService();
    String oldName,newName,stoPath,outPath;
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Object obj=session.getAttribute("uphone");
        if (obj==null){
            return "r:/user";
        }else{
            return "r:/userinfo?v=show";
        }
    }
    /**
     * 展示个人中心页面
     * @date 2023/4/18
     **/
    public String show(HttpServletRequest request,HttpServletResponse response) throws Exception{
        Object uphone = session.getAttribute("uphone");
        String uPhone;
        if (uphone instanceof String){
            uPhone=(String) uphone;
        }else {
            request.setAttribute("error","登录过期");
            return "f:userinfo";
        }
        User user= us.search(uPhone);
        request.setAttribute("user",user);
        return "f:userinfo";
    }
    /**
     * 个人信息修改的表单提交
     * @date 2023/4/18
     **/
    public String change(HttpServletRequest request,HttpServletResponse response) throws Exception {
        //获取上传的新头像文件
        Part part = request.getPart("uheadshot");
        String uheadshot=null;
        //获取更改的用户信息
        String id = request.getParameter("uid");
        Integer uid = Integer.valueOf(id);
        String uname = request.getParameter("uname");
        String uemail = request.getParameter("uemail");
        String uphone = request.getParameter("uphone");
        String upassword = request.getParameter("upassword");
        oldName = part.getSubmittedFileName();//获取文件的原始名字
        if (part.getSize()>0){
            String realPath = request.getServletContext().getRealPath("/");
            //D:\Java03\YF03405\02Java\code\Java\Git\meizu\out\artifacts\meizu_Web_exploded\
            stoPath=realPath.substring(0,realPath.indexOf("out"))+"web/img/people/"+uid+"/";
            outPath=realPath+"img/people/"+uid;
            newName = uniqeName() + getSuffix(oldName);
            //上传头像文件
            FileUtil.writeFromStream(part.getInputStream(),stoPath+newName);
            uheadshot = "../img/people/"+uid+"/"+newName;
        }
        //修改数据库信息
        Object obj = session.getAttribute("uphone");
        if (obj == null) {
            return "r:/user";
        } else {
            User user = new User(uid, uname, uheadshot, uemail, uphone, upassword);
            //前往数据库更新
            Integer r = us.change(user);
            if (uheadshot!=null){
                File file=new File(outPath);
                if (!file.exists()){
                    file.mkdirs();
                }
                FileUtil.copyFile(stoPath+newName,outPath+"/"+newName);
            }
            if (r > 0) {
                User user1=us.search(user.getUphone());
                session.setAttribute("user",user1);
                session.setAttribute("uphone", uphone);
                return "r:/userinfo?v=show";
            } else {
                request.setAttribute("error", "更新失败");
                return "f:userinfo";
            }
        }
    }
    //获取文件后缀名
    public String getSuffix(String oldName){
        //.png/.jpg……
        int i = oldName.lastIndexOf(".");
        return oldName.substring(i);
    }
    //生成唯一的文件名
    public String uniqeName(){
        //获取当前系统时间
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String s=sdf.format(date);
        //生成四位数的随机数  1-9999之间，不够四位的，补齐四位 0001  0025
        int rnd = (int)(Math.random()*9999)+1;
        //rnd=4
        String n = rnd+"";
        for(int i=0;i<(4-n.length());i++){
            n="0"+n;
        }
        return s+n;
    }
}
