package com.meizu.control;

import com.meizu.entity.Order;
import com.meizu.entity.User;
import com.meizu.service.OrderService;
import com.meizu.utils.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * 付款页 的控制层操作类
 * @date 2023/4/18
 **/
@WebServlet("/paymentServlet")
public class PaymentServlet extends BaseServlet {
    OrderService os=new OrderService();
    @Override
    public String excute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        User user=(User) session.getAttribute("user");
        if (user.getUstate()!=1){
            String oId=request.getParameter("oid");
            if (oId!=null && !"".equals(oId)){
                Integer oid=Integer.valueOf(oId);
                Order order= os.one(oid);
                if (order==null || order.getOstate()!=0){//控制地址栏的更改，不存在的订单和待支付以外的订单都会跳报错页面
                    return "f:admin/error";
                }else{
                    String to=request.getParameter("total");
                    Integer total=Integer.valueOf(to);
                    request.setAttribute("total",total);
                    request.setAttribute("orrder",oid);
                    return "f:payment";
                }
            }else {
                return "r:/index";
            }
        }
        return "r:/user";
    }
}
