package com.meizu.dao;

import com.meizu.entity.User;
import com.meizu.utils.RowMapper;
import com.meizu.utils.SQLHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
/**
 * 个人中心（登录注册）的CRUD的DAO类
 * @Date 2023/4/14 10:05
 * ustate=0 账号可用，ustate=-1 账号不可用
**/
public class UserDao {

    SQLHelper helper=new SQLHelper ();
    //登录（查询）
    //根据手机号和密码查询该条账号的所有数据
    public User Login(String phone,String upassword){
       String sql="select * from tb_user where uphone=? and upassword=?";
       return helper.one (sql,new UserRowMapper (),phone,upassword);
    }

    /**
     * 新用户注册
     * @param phone
     * @param upassword
     * @return 新增的主键UID
     */
    public Integer Register(String phone,String upassword){
        String sql="insert into tb_user values(null,default,default,null,?,?,default)";
        return helper.insert (sql,phone,upassword);
    }
    //根据手机号查询用户信息
    public User search(String phone){
        String sql="select * from tb_user where uphone=?";
        return helper.one(sql,new UserRowMapper(),phone);
    }
    //修改用户信息（包括头像）
    public Integer change1(User user){
        String sql="update tb_user set uname=?,uheadshot=?,uemail=?,uphone=?,upassword=? where uid=?";
        return helper.update(sql,user.getUname(),
                user.getUheadshot(),
                user.getUemail(),
                user.getUphone(),
                user.getUpassword(),
                user.getUid());
    }
    //修改用户信息（不包括头像）
    public Integer change2(User user){
        String sql="update tb_user set uname=?,uemail=?,uphone=?,upassword=? where uid=?";
        return helper.update(sql,user.getUname(),
                user.getUemail(),
                user.getUphone(),
                user.getUpassword(),
                user.getUid());
    }
    /**
     *查询所有用户信息（给后台）
     * @author hxf
     * @date 2023/4/10
     **/
    public List<User> selectAll(){
        String sql = "select * from tb_user";
        return helper.query(sql,new UserRowMapper());
    }
    /**
     * 根据uid修改用户状态（给后台）
     * @Author hxf
     * @Date 2023/4/10 16:28
     * @Param [uid] 用户id
     * @return java.lang.Integer
    **/
    public Integer updateState(Integer ustate,Integer uid){
        String sql = "update tb_user set ustate=? where uid=?";
        return helper.update(sql,ustate,uid);
    }
    /**
     * 根据用户id查询用户信息（给后台）
     * @Author hxf
     * @Date 2023/4/11 9:55
     * @Param [uid] 用户id
     * @return com.meizu.entity.User
    **/
    public User selectByUid(Integer uid){
        String sql = "select * from tb_user where uid=?";
        return helper.one(sql,new UserRowMapper(),uid);
    }
    /**
     * 根据uid修改重置用户密码（给后台）
     * @Author hxf
     * @Date 2023/4/12 8:53
     * @Param [uid] 用户id
     * @return java.lang.Integer
    **/
    public Integer resetPwd(Integer uid){
        String sql = "update tb_user set upassword='a123456' where uid=?";
        return helper.update(sql,uid);
    }
    /**
     * 查询用户数量（给后台）
     * @Author hxf
     * @Date 2023/4/11 9:55
     **/
    public User countUser(){
        String sql = "select count(*) from tb_user";
        return helper.one(sql, new RowMapper<User>() {
            @Override
            public User map(ResultSet rs) throws SQLException {
                return new User(rs.getInt(1));
            }
        });
    }

class UserRowMapper implements RowMapper<User>{

    @Override
    public User map(ResultSet rs) throws SQLException {
        return new User (rs.getInt ("uid"),
                rs.getString ("uname"),
                rs.getString ("uheadshot"),
                rs.getString ("uemail"),
                rs.getString ("uphone"),
                rs.getString ("upassword"),
                rs.getInt ("ustate"));
    }
 }

}
