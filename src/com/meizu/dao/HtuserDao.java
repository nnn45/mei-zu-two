package com.meizu.dao;

import com.meizu.entity.Htuser;
import com.meizu.utils.RowMapper;
import com.meizu.utils.SQLHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
/**
 * 后台用户的CRUD的DAO类
 * @date 2023/4/19
 **/
public class HtuserDao {
    SQLHelper helper=new SQLHelper();
    //根据手机号和密码查管理员信息
    public Htuser HtuserLogin(String phone,String pwd){
        String sql="select * from tb_htuser where uphone=? and upassword=?";
        return helper.one(sql,new HtuserRowMapper(),phone,pwd);
    }
    //根据手机号查管理员信息
    public Htuser search(String phone){
        String sql="select * from tb_htuser where uphone=?";
        return  helper.one(sql,new HtuserRowMapper(),phone);
    }
class HtuserRowMapper implements RowMapper<Htuser>{

    @Override
    public Htuser map(ResultSet rs) throws SQLException {
        return new Htuser(rs.getInt("htuid"),
                rs.getString("htuname"),
                rs.getString("uphone"),
                rs.getString("upassword"));
    }
}
}
