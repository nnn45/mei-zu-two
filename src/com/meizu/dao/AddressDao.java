package com.meizu.dao;

import com.meizu.entity.Address;
import com.meizu.utils.RowMapper;
import com.meizu.utils.SQLHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
/**
 * 地址管理的CRUD的DAO类
 * @Date 2023/4/14 9:55
 * set_default=0 普通地址，set_default=1默认地址，set_default=-1禁用的地址（删除）
**/
public class AddressDao {
    SQLHelper helper=new SQLHelper();

    /**
     * 根据用户id查询用户的所有地址
     * @param uid
     * @return
     */
    public List<Address> selectAll(Integer uid){
        String sql="select * from tb_address where uid=? and set_default>=0 order by set_default desc";
        return helper.query(sql,new AddressRowMapper(),uid);
    }

    /**
     * 根据地址ID查询具体的地址信息
     * @param aid 地址ID
     * @return 具体的地址信息
     */
    public  Address selectOne(Integer aid){
        String sql="select * from tb_address where aid=?";
        return helper.one(sql,new AddressRowMapper(),aid);
    }
    /**
     * 据用户id查询用户地址总条数
     * @param uid 用户ID
     * @return 地址总条数
     */
    public Address selectCount(Integer uid){
        String sql="select count(*) from tb_address where uid=? and set_default>=0";
        return helper.one(sql,new CountRowMapper(),uid);
    }
    /**
     * 新添地址
     * @param address 地址对象
     * @return 新增的主键编号
     */
    public Integer insertAll(Address address){
        String sql="insert into tb_address values(null,?,?,?,?,?,?,?,?,?)";
        return helper.insert(sql,address.getUid(),
                address.getAname(),
                address.getSheng(),
                address.getShi(),
                address.getQu(),
                address.getJie(),
                address.getAdetail(),
                address.getDzphone(),
                address.getSet_default());
    }

    /**
     * 根据id删除地址
     * @param aid
     * @return
     */
    public Integer deleteByaid(Integer aid){
        String sql="update tb_address set set_default=-1 where aid=?";
        return helper.update(sql,aid);
    }

    /**
     * 修改地址
     * @param address
     * @return
     */
    public Integer updateByaid(Address address){
        String sql="update tb_address set aname=?,dzphone=?,sheng=?,shi=?,qv=?,jie=?,adetail=?,set_default=? where aid=?";
        return helper.update(sql,address.getAname(),
                address.getDzphone(),
                address.getSheng(),
                address.getShi(),
                address.getQu(),
                address.getJie(),
                address.getAdetail(),
                address.getSet_default(),
                address.getAid());
    }

    /**
     * 修改默认值
     * @param moren 默认值
     * @param aid 地址ID
     * @return 修改结果 >0-成功
     */
    public  Integer updateDefault(Integer moren,Integer aid){
        String sql="update tb_address set set_default=? where aid=?";
        return helper.update(sql,moren,aid);
    }

    /**
     * 将原默认地址修改为普通地址
     * @return
     */
    public  Integer udateMoren(Integer uid){
        String sql ="select * from tb_address where set_default=1 and uid=?";
        Address address=helper.one(sql,new AddressRowMapper(),uid);
        String sql2="update tb_address set set_default =0 where aid=? and uid=?";
        return helper.update(sql2,address.getAid(),uid);
    }

    /**
     * 根据用户ID查询出默认地址1
     * @return
     */
    public Address selectMrzYi(Integer uid){
        String sql ="select * from tb_address where set_default=1 and uid=?";
        return helper.one(sql,new AddressRowMapper(),uid);
    }
   class AddressRowMapper  implements RowMapper<Address>{

       @Override
       public Address map(ResultSet rs) throws SQLException {
           return new Address(
                   rs.getInt("aid"),
                   rs.getInt("uid"),
                   rs.getString("aname"),
                   rs.getString("sheng"),
                   rs.getString("shi"),
                   rs.getString("qv"),
                   rs.getString("jie"),
                   rs.getString("adetail"),
                   rs.getString("dzphone"),
                   rs.getInt("set_default"));
       }
   }
   class CountRowMapper implements RowMapper<Address>{

       @Override
       public Address map(ResultSet rs) throws SQLException {
           return new Address(rs.getInt(1));
       }
   }
}
