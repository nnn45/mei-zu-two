package com.meizu.dao;

import com.meizu.entity.Detail;
import com.meizu.utils.RowMapper;
import com.meizu.utils.SQLHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * 商品详情的CRUD的DAO类
 * @author hxf
 * @date 2023/3/23
 **/
public class DetailDao {
    SQLHelper helper = new SQLHelper();

    /**
     * 根据条件查询详情dprice
     * @param gid
     * @param dcolor
     * @param dversion
     * @return
     */
    public Detail selectPriceByCondition(Integer gid,String dcolor,String dversion){
        String sql = "select dprice from tb_detail where did=(\n" +
                "select did from tb_detail where gid=? and dcolor=? and dversion=?)";
        return helper.one(sql,new DetailDpriceRowMapper(), gid, dcolor,dversion);
    }
    /**
     * 根据条件查询详情dprice
     * @param gid
     * @param dcolor
     * @return
     */
    public Detail selectPriceByConditionTwo(Integer gid,String dcolor){
        String sql = "select dprice from tb_detail where did=(\n" +
                "select did from tb_detail where gid=? and dcolor=?)";
        return helper.one(sql,new DetailDpriceRowMapper(), gid, dcolor);
    }

    /**
     * 根据条件查询详情id(商品类别为1-3)
     * @param gid
     * @param dcolor
     * @param dversion
     * @return
     */
    public Detail selectDidByCondition(Integer gid,String dcolor,String dversion){
        String sql = "select did\n" +
                "from tb_detail where gid=? and dcolor=? and dversion=?";
        return helper.one(sql,new DetailDidRowMapper(),gid,dcolor,dversion);
    }
    /**
     * 根据条件查询详情id(商品类别为4-5)
     * @param gid
     * @param dcolor
     * @return
     */
    public Detail selectDidByConditionTwo(Integer gid,String dcolor){
        String sql = "select did\n" +
                "from tb_detail where gid=? and dcolor=?";
        return helper.one(sql,new DetailDidRowMapper(),gid,dcolor);
    }
    /**
     * 根据详情id查询gid
     * @param did   详情id
     * @return
     */
    public Detail selectGidByDid(Integer did){
        String sql = "select gid from tb_detail where did=?";
        return helper.one(sql,new DetailGidRowMapper(),did);

    }

    /**
     * 根据详情id查询价格（进行详情页的默认展示）
     * @param did   详情id
     * @return 商品详情对象
     */
    public Detail selectPrice(Integer did){
        String sql = "select dprice from tb_detail where did=?";
        return helper.one(sql,new DetailDpriceRowMapper(),did);

    }

    /**
     * 查指定商品的所有颜色
     * @param did   商品id
     * @return
     */
    public List<Detail> selectColor(Integer did){
        String sql = "select distinct dcolor from tb_detail\n" +
                "where gid=(select gid from tb_detail where did=?)";
        return helper.query(sql,new DetailColorRowMapper(),did);
    }
    /**
     * 查指定商品的所有版本
     * @param did   商品id
     * @return
     */
    public List<Detail> selectVersion(Integer did){
        String sql = "select distinct dversion from tb_detail\n" +
                "where gid=(select gid from tb_detail where did=?)";
        return helper.query(sql,new DetailVersionRowMapper(),did);
    }
    /**
     * 查指定商品的所有版本
     * @param gid   商品id
     * @return
     */
    public List<Detail> selectVersions(Integer gid){
        String sql = "select distinct dversion from tb_detail\n" +
                "where gid=?";
        return helper.query(sql,new DetailVersionRowMapper(),gid);
    }
    /**
     * 根据详情id查询dprice,dcolor,dversion,gname
     * @param did
     * @return
     */
    public Detail selectXQ(Integer did){
       // String sql="select gname from tb_goods where gid=(select gid from tb_detail where did=?)";
        String sql="select dprice,dcolor,dversion,(select gname from tb_goods where gid=(select gid from tb_detail where did=?)) as gname from tb_detail where did=? ";
        return helper.one(sql,new DetailXQRowMapperParent(),did,did);
    }
    //后台修改详情
    public Integer updateGoodsDetail(Detail detail){
        String sql="update tb_detail set dprice=?,dcolor=?,dversion=? where did=?";
        return helper.update (sql,detail.getDprice (),detail.getDcolor (),detail.getDversion (),detail.getDid ());
    }
    //后台修改图片表
    public Integer updatePhoto(String photo,Integer pid){
        String sql="update tb_photo set photo=? where pid=?";
        return helper.update (sql,photo,pid);
    }

    /**
     * 插入一条商品详情数据（有版本）
     * @param gid 商品ID
     * @param dprice 价格
     * @param dcolor 颜色
     * @param dversion 版本
     * @return 新增的主键编号
     */
    public Integer insertDetail1(Integer gid,String dprice,String dcolor,String dversion){
        String sql="insert into tb_detail values(null,?,?,?,?)";
        return helper.insert(sql,gid,dprice,dcolor,dversion);
    }
    /**
     * 插入一条商品详情数据（无版本）
     * @param gid 商品ID
     * @param dprice 价格
     * @param dcolor 颜色
     * @return 新增的主键编号
     */
    public Integer insertDetail2(Integer gid,String dprice,String dcolor){
        String sql="insert into tb_detail values(null,?,?,?,null)";
        return helper.insert(sql,gid,dprice,dcolor);
    }
    //根据版本和颜色查询单条详情
    public Detail selectDetailOne(String banben,String yanse){
        String sql="select * from tb_detail where dcolor=? and dversion=?";
        return helper.one (sql, new DetailRowMapperParent (),yanse,banben);
    }
    /**
     * 查看商品是否有详情id（给后台判断上下架）
     * @Author hxf
     * @Date 2023/4/17 9:21
    **/
    public List<Detail> selectDetailByGid(Integer gid){
        String sql = "select did from tb_detail where gid=?";
        return  helper.query(sql,new DetailDidRowMapper(),gid);
    }

    class DetailXQRowMapperParent implements RowMapper<Detail>{

        @Override
        public Detail map(ResultSet rs) throws SQLException {
            Detail detail=new Detail();
            detail.setDprice(rs.getInt("dprice"));
            detail.setDcolor(rs.getString("dcolor"));
            detail.setDversion(rs.getString("dversion"));
            detail.setGname(rs.getString("gname"));
            return detail;
        }
    }

    class DetailRowMapperParent implements RowMapper<Detail>{

        @Override
        public Detail map(ResultSet rs) throws SQLException {
            return new Detail(rs.getInt("did"),
                    rs.getInt("gid"),
                    rs.getInt("dprice"),
                    rs.getString("dcolor"),
                    rs.getString("dversion"));
        }
    }
    //did
    class DetailDidRowMapper implements RowMapper<Detail>{

        @Override
        public Detail map(ResultSet rs) throws SQLException {
            Detail detail = new Detail();
            detail.setDid(rs.getInt("did"));
            return detail;
        }
    }
    //gid
    class DetailGidRowMapper implements RowMapper<Detail>{

        @Override
        public Detail map(ResultSet rs) throws SQLException {
            Detail detail = new Detail();
            detail.setGid(rs.getInt("gid"));
            return detail;
        }
    }

    //dprice
    class DetailDpriceRowMapper implements RowMapper<Detail>{
        @Override
        public Detail map(ResultSet rs) throws SQLException {
            Detail detail = new Detail();
            detail.setDprice(rs.getInt("dprice"));
            return detail;
        }
    }


    //dcolor
    class DetailColorRowMapper implements RowMapper<Detail>{
        @Override
        public Detail map(ResultSet rs) throws SQLException {
            Detail detail = new Detail();
            detail.setDcolor(rs.getString("dcolor"));
            return detail;
        }
    }
    //dversionr
    class DetailVersionRowMapper implements RowMapper<Detail>{
        @Override
        public Detail map(ResultSet rs) throws SQLException {
            Detail detail = new Detail();
            detail.setDversion(rs.getString("dversion"));
            return detail;
        }
    }
}
