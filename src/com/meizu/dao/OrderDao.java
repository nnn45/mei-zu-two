package com.meizu.dao;

import com.meizu.entity.Order;
import com.meizu.entity.User;
import com.meizu.utils.RowMapper;
import com.meizu.utils.SQLHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
/**
 * 订单中心的CRUD的DAO类
 * @Date 2023/4/14 9:58
 * ostate=0 待支付，ostate=1 待发货，ostate=-1 已取消，ostate=2 已发货
**/
public class OrderDao {
    SQLHelper helper=new SQLHelper();

    /**
     * 查询所有订单（给后台）
     * @return 所有用户订单集合
     */
    public List<Order> selectOrders(){
        String sql="select * from tb_order order by oid desc";
        return helper.query(sql,new OrderRowMapper());
    }
    /**
     * 查询所用订单数量（给后台）
     * @Author hxf
     * @Date 2023/4/11 9:55
     **/
    public Order countOrders(){
        String sql = "select count(*) from tb_order";
        return helper.one(sql, new RowMapper<Order>() {
            @Override
            public Order map(ResultSet rs) throws SQLException {
                return new Order(rs.getInt(1));
            }
        });
    }
    /**
     * 根据用户ID查询该用户的所有订单
     * @param uid 用户ID
     * @return 订单集合
     */
    public List<Order> selectAllOrder(Integer uid){
        String sql="select * from tb_order where uid=? order by oid desc";
        return helper.query(sql,new OrderRowMapper(),uid);
    }

    /**
     * 根据用户ID和订单状态查询某种状态的所有订单
     * @param uid 用户ID
     * @param ostate 订单状态ID
     * @return 订单集合
     */
    public List<Order> selectByState(Integer uid,Integer ostate){
        String sql="select * from tb_order where uid=? and ostate=? order by oid desc";
        return helper.query(sql,new OrderRowMapper(),uid,ostate);
    }

    /**
     * 根据订单号查询一份订单
     * @param oid 订单号
     * @return 订单对象
     */
    public Order selectOne(Integer oid){
        String sql="select * from tb_order where oid=?";
        return helper.one(sql,new OrderRowMapper(),oid);
    }
    /**
     * 根据订单号更改订单状态（取消订单）
     * @param oid 订单号
     * @return 更新订单状态数据的结果，>0 成功
     */
    public Integer change(Integer oid,Integer ostate){
        String sql="update tb_order set ostate=? where oid=?";
        return helper.update(sql,ostate,oid);
    }

    /**
     * 向已发货的订单存入快递单号
     * @param oid 订单号
     * @param cnum 快递单号
     * @return 更新结果
     */
    public Integer addCNum(Integer oid,String cnum){
        String sql="update tb_order set cnum=? where oid=?";
        return helper.update(sql,cnum,oid);
    }
    /**
     * 新增订单（点击下单并支付）
     * @param uid 用户ID
     * @param aid 地址ID
     * @return 新增的订单编号oid
     */
    public Integer insertOrder(Integer uid,Integer aid){
        String sql="insert into tb_order values(null,?,default,default,?,null)";
        return helper.insert(sql,uid,aid);
    }
    /**
     * 让主键自增连续（订单表）
     * @return
     */
    public Integer doUpId(){
        String sql = "alter table tb_order AUTO_INCREMENT=50000";
        return helper.update(sql);
    }
    class OrderRowMapper implements RowMapper<Order>{
        @Override
        public Order map(ResultSet rs) throws SQLException {
            return new Order(rs.getInt("oid"),
                    rs.getInt("uid"),
                    rs.getTimestamp("otime"),
                    rs.getInt("ostate"),
                    rs.getInt("aid"),
                    rs.getString("cnum")
            );
        }
    }
}
