package com.meizu.dao;

import com.meizu.entity.OrderDetail;
import com.meizu.utils.RowMapper;
import com.meizu.utils.SQLHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
/**
 * 订单详情的CRUD的DAO类
 * @Date 2023/4/14 9:58
 **/
public class OrderDetailDao {
    SQLHelper helper=new SQLHelper();

    /**
     * 根据订单ID查询订单详情
     * @param oid 订单编号
     * @return 订单详情集合（详情ID、订单ID、商品详情ID、数量、价格、颜色、版本、商品名）
     */
    public List<OrderDetail> selectOrderDetails(Integer oid){
        String sql="select tod.*,td.dprice,td.dcolor,td.dversion,tg.gname from tb_order_detail tod\n" +
                "left join tb_detail td on td.did = tod.did\n" +
                "left join tb_goods tg on tg.gid = td.gid where oid=?";
        return helper.query(sql,new OrderDetailRowMapper(),oid);
    }

    /**
     * 逐条新增订单详情
     * @param oid 订单编号
     * @param count 商品数量
     * @param did 商品详情ID
     * @return 新增的订单详情编号
     */
    public Integer insertOrderDetail(Integer oid,Integer count,Integer did){
        String sql="insert into tb_order_detail values(null,?,?,?)";
        return helper.insert(sql,oid,count,did);
    }
    /**
     * 让主键自增连续
     * @return
     */
    public Integer doUpId(){
        String sql = "alter table tb_order_detail AUTO_INCREMENT=1";
        return helper.update(sql);
    }
    class OrderDetailRowMapper implements RowMapper<OrderDetail>{
        @Override
        public OrderDetail map(ResultSet rs) throws SQLException {
            return new OrderDetail(rs.getInt("xqid"),
                    rs.getInt("oid"),
                    rs.getInt("count"),
                    rs.getInt("did"),
                    rs.getInt("dprice"),
                    rs.getString("dcolor"),
                    rs.getString("dversion"),
                    rs.getString("gname")
            );
        }
    }
}
