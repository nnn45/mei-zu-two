package com.meizu.dao;


import com.meizu.entity.GoodsSort;
import com.meizu.utils.RowMapper;
import com.meizu.utils.SQLHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class GoodssortDao {
    SQLHelper helper=new SQLHelper();
    //查询商品类别信息
    public List<GoodsSort> selectAll(){
        String sql="select sid,sname from tb_goodssort";
        return helper.query(sql,new GoodsSortRowMapper());
    }
    //根据类别id查询类别名称
    public GoodsSort selectOne(Integer id){
        String sql="select sname from tb_goodssort where sid=?";
        return helper.one(sql,new GoodsSortRowMapperName(),id);
    }
    //根据类别id删除该类别
    public Integer deleteGoodssort(Integer id){
        String sql="delete from tb_goodssort where sid=?";
        return helper.update(sql,id);
    }
    //插入商品类别
    public Integer insertGoodssort(GoodsSort goodsSort){
        String sql ="insert into tb_goodssort value(null,?,null)";
        return helper.insert(sql,goodsSort.getSname());
    }
    //根据类别id修改商品类别
    public Integer updateGoodssort(Integer id,String name){
        String sql="update tb_goodssort set sname=? where sid=?";
        return helper.update(sql,name,id);
    }
    //查询商品类别信息
    public List<GoodsSort> selectAllnotSid(Integer sid){
        String sql="select sid,sname from tb_goodssort where not sid=?";
        return helper.query(sql,new GoodsSortRowMapper());
    }
    class GoodsSortRowMapperName implements RowMapper<GoodsSort>{

        @Override
        public GoodsSort map(ResultSet rs) throws SQLException {
            return new GoodsSort(rs.getString("sname"));
        }
    }
    class GoodsSortRowMapper implements RowMapper<GoodsSort>{

        @Override
        public GoodsSort map(ResultSet rs) throws SQLException {
            return new GoodsSort(rs.getInt("sid"),
                    rs.getString("sname"));
        }
    }
    class GoodsSortRowMapperInsert implements RowMapper<GoodsSort>{

        @Override
        public GoodsSort map(ResultSet rs) throws SQLException {
            return new GoodsSort(rs.getInt("sid"),
                    rs.getString("sname"),
                    rs.getString("photo"));
        }
    }
}
