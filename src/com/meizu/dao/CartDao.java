package com.meizu.dao;

import com.meizu.entity.Cart;
import com.meizu.utils.RowMapper;
import com.meizu.utils.SQLHelper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
/**
 * 购物车的CRUD的DAO类
 * @author hxf
 * @date 2023/3/23
 **/
public class CartDao {
    SQLHelper helper = new SQLHelper();

    /**
     *插入购物车信息
     */
    public Integer InsertCart(Integer did,Integer uid,String  cphoto,Integer count){
        String sql = "insert into tb_cart values(null,?,?,?,?)";
        return helper.insert(sql,did,uid,cphoto,count);
    }

    /**
     * 根据用户 连表 查询 购物车信息
     * @param uid
     * @return
     */
    public List<Cart> selectByUid(Integer uid){
        String sql = "select c.cid,d.did,c.cphoto,g.gname,d.dprice,d.dcolor,d.dversion,c.count from tb_cart c,tb_detail d,tb_goods g,tb_user u\n" +
                "where c.uid=u.uid and c.did=d.did and d.gid=g.gid\n" +
                "and c.uid=? order by c.cid desc";
        return helper.query(sql,new CartRowMapper(),uid);
    }

    /**
     * 根据购物车ID查询购物车详情的did和数量
     * @param cid 购物车ID
     * @return 购物车详情对象
     */
    public Cart selectByCid(Integer cid){
        String sql="select did,count,cphoto from tb_cart where cid=?";
        return helper.one(sql,new CartRowMapper1(),cid);
    }

    public Cart selectByDid(Integer uid,Integer did){
        String sql = "select count from tb_cart where uid=? and did=?";
        return helper.one(sql, new RowMapper<Cart>() {
            @Override
            public Cart map(ResultSet rs) throws SQLException {
                Cart cart = new Cart();
                cart.setCount(rs.getInt(1));
                return cart;
            }
        },uid,did);
    }

    /***
     * 点击加入购物车 存在 的 修改数量
     * @Author hxf
     * @Date 2023/3/31 13:56
    **/
    public Integer updateCount(Integer count,Integer uid,Integer did){
        String sql = "update tb_cart set count=? where uid=? and did=?";
        return helper.update(sql,count,uid,did);
    }
    /**
     * 数量限购
     * @Author hxf
     * @Date 2023/4/7 17:15
    **/
    public Integer updateCountForFive(Integer uid,Integer did){
        String sql = "update tb_cart set count=5 where uid=? and did=?";
        return helper.update(sql,uid,did);
    }

    /***
     * 购物车商品修改数量
     * @Author hxf
     * @Date 2023/3/31 13:56
     **/
    public Integer changeCount(Integer count,Integer cid){
        String sql = "update tb_cart set count=? where cid=?";
        return helper.update(sql,count,cid);
    }


    /**
     * 根据用户 连表 倒叙查询五条 购物车信息
     * @Author lyz
     * @Date 2023/3/30 15:53
     * @Param [uid]
     * @return java.util.List<com.meizu.entity.Cart>
    **/
    public List<Cart> selectByIndexUid(Integer uid){
        String sql = "select c.cid,d.did,c.cphoto,g.gname,d.dprice,d.dcolor,d.dversion,c.count from tb_cart c,tb_detail d,tb_goods g,tb_user u\n" +
                "where c.uid=u.uid and c.did=d.did and d.gid=g.gid\n" +
                "and c.uid=? order by c.cid desc limit 0,5";
        return helper.query(sql,new CartRowMapper(),uid);
    }
    /**
     * 删除购物车信息
     * @Author lyz
     * @Date 2023/3/30 16:41
     * @Param [id]
     * @return java.lang.Integer
    **/
    
    public Integer deleteCartByCid(Integer id){
        String sql="delete from tb_cart where cid=?";
        return helper.update (sql,id);
    }

    /**
     * 让主键自增连续
     * @return
     */
    public Integer doUpId(){
        String sql = "alter table tb_cart AUTO_INCREMENT=1";
        return helper.update(sql);
    }
    class CartRowMapper1 implements RowMapper<Cart>{
        @Override
        public Cart map(ResultSet rs) throws SQLException {
            return new Cart(rs.getInt("did"),
                    rs.getInt("count"),
                    rs.getString("cphoto"));
        }
    }
    //内部类(没有uid)
    class CartRowMapper implements RowMapper<Cart>{

        @Override
        public Cart map(ResultSet rs) throws SQLException {
            return new Cart(rs.getInt("cid"),
                    rs.getInt("did"),
                    rs.getString("cphoto"),
                    rs.getString("gname"),
                    rs.getInt("dprice"),
                    rs.getString("dcolor"),
                    rs.getString("dversion"),
                    rs.getInt("count"));
        }
    }
}
