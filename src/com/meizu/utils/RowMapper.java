package com.meizu.utils;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 行映射类.将数据库的一条记录转换成java中的一个对象
 * @param <T>
 */
public interface RowMapper<T> {
    /**
     * 将数据库的一条记录转换成java中的一个对象
     * @param rs  获取数据库的一条记录
     * @return      返回一个实体对象
     * @throws SQLException
     */
    T map(ResultSet rs) throws SQLException;
}
