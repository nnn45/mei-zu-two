package com.meizu.utils;

import java.io.Serializable;
import java.util.List;

/**
 * page工具类
 */
public class Pager implements Serializable {
    private Integer pageNo;//当前页码
    private Integer pageSize;//每页的数量
    private Integer total;//总条数
    private List<?> datas;//当前的数据

    public boolean getHasPrev(){
        return !(pageNo==1);
    }

    public boolean getHasNext(){
        return !(pageNo==getPageCount());
    }

    /**
     * 计算的总页数
     * @return
     */
    public Integer getPageCount(){
        if(total%pageSize==0){
            return total/pageSize;
        }else{
            return total/pageSize+1;
        }
    }

    public Pager(Integer no, Integer size){
        this.pageNo=no;
        this.pageSize=size;
    }



    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<?> getDatas() {
        return datas;
    }

    public void setDatas(List<?> data) {
        this.datas = data;
    }

    @Override
    public String toString() {
        return "Pager{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", total=" + total +
                ", data=" + datas +
                '}';
    }
}
