package com.meizu.service;

import com.meizu.dao.PhotoDao;
import com.meizu.entity.Photo;

import java.util.List;

/**
 * 图片的业务层
 */
public class PhotoService {
    private PhotoDao dao=new PhotoDao();

    /**
     * 根据颜色，版本，商品类别，查图片
     * @param gid
     * @param dcolor
     * @param dversion
     * @return
     */
    public List<Photo> selectPhotoByCondition(Integer gid,String dcolor,String dversion){
        return dao.selectPhotoByCondition(gid, dcolor, dversion);
    }
    /**
     * 根据颜色，商品类别，查图片（局部刷新）
     * @param gid
     * @param dcolor
     * @return
     */
    public List<Photo> selectPhotoByConditionTow(Integer gid,String dcolor){
        return dao.selectPhotoByConditionTow(gid, dcolor);
    }

    /**
     * 根据详情id（did）查询图片
     * @param did
     * @return
     */
    public List<Photo> selectPhoto(Integer did){
        return dao.selectPhoto(did);
    }
    public Integer insertPhoto(Photo photo){
        return dao.insertPhoto(photo);
    }
    public Integer deletePhoto(String...ids){
        return dao.deletePhoto(ids);
    }

    public List<Photo> selectPhotoId(Integer did){
        return dao.selectPhotoId (did);
    }
}
