package com.meizu.service;

import com.meizu.dao.UserDao;
import com.meizu.entity.User;

import java.util.List;

/**
 * 个人中心（登录注册） 的业务层
 */
public class UserService {

    private UserDao dao=new UserDao ();

    /**
     * 账号登录
     * @param uphone
     * @param upassword
     * @return 登录（查询）结果
     */
    public Integer Login(String uphone,String upassword){//ustate=0,账号可用，1账号不可以用
        User user=new User ();
        user=dao.Login (uphone,upassword);//获得数据库查询的结果
        if (user==null){//判断账号和密码是否符合
            return -1;
        }else {
            if (user.isUstate()!=0){//账号是否可用
                return 0;
            }else {
               return 1;//所有验证通过
            }
        }
    }

    /**
     * 查询用户信息
     * @param uphone
     * @return 查询出的用户对象
     */
    public User search(String uphone){
        User user= dao.search(uphone);//通过手机号获取用户对象，前提是已经登录
        String uPhone=user.getUphone();//获取用户手机号
        //将手机号与char数组互转实现加密处理，存为用户对象的新属性nphone
        char[] pArr=uPhone.toCharArray();
        for (int i=3;i<7;i++){
            pArr[i]='*';
        }
        String nPhone=String.valueOf(pArr);
        user.setNphone(nPhone);
        //此时用户具有两个关于手机号的属性，一个加密用于页面呈现，一个未加密用于用户修改数据
        return user;
    }

    /**
     * 注册账号
     * @param phone
     * @param upassword
     * @return 新增编号UID
     */
    public Integer Register(String phone,String upassword){
        return dao.Register (phone,upassword);
    }

    /**
     * 修改用户信息
     * @Author lyz
     * @Date 2023/3/24 16:39
     * @Param [user]
     * @return java.lang.Integer
    **/
    
    public Integer change(User user){
        if (user.getUheadshot()==null){
            return dao.change2(user);
        }else {
            return dao.change1(user);
        }
    }

    /**
     *查询所有用户信息（给后台）
     * @author hxf
     * @date 2023/4/10
     **/
    public List<User> selectAll(){
        return dao.selectAll();
    }
    /**
     * 根据uid修改用户状态
     * @Author hxf
     * @Date 2023/4/10 16:28
     * @Param [uid] 用户id
     * @return java.lang.Integer
     **/
    public Integer updateState(Integer ustate,Integer uid){//ustate=0,账号可用，1账号不可以用
        return dao.updateState(ustate,uid);
    }
    /**
     * 根据用户id查询用户信息
     * @Author hxf
     * @Date 2023/4/11 9:55
     * @Param [uid]
     * @return com.meizu.entity.User
     **/
    public User selectByUid(Integer uid){
        return dao.selectByUid(uid);
    }
    /**
     * 根据uid修改重置用户密码
     * @Author hxf
     * @Date 2023/4/12 8:53
     * @Param [uid] 用户id
     * @return java.lang.Integer
     **/
    public Integer resetPwd(Integer uid){
        return dao.resetPwd(uid);
    }
    /**
     * 查询用户数量
     * @Author hxf
     * @Date 2023/4/11 9:55
     **/
    public User countUser(){
        return dao.countUser();
    }
}
