package com.meizu.service;

import com.meizu.dao.GoodssortDao;
import com.meizu.entity.GoodsSort;

import java.util.List;
/**
 * 商品类别 的业务层
 * @date 2023/4/19
 **/
public class GoodssortService {
    GoodssortDao dao=new GoodssortDao();

    /**
     * 查询商品类别信息
     * @return
     */
    public List<GoodsSort> selectAll(){
        return dao.selectAll();
    }

    /**
     * 根据类别id查询类别名称
     * @param id
     * @return
     */
    public GoodsSort selectOne(Integer id){
        return dao.selectOne(id);
    }

    /**
     * 根据类别id删除该类别
     * @param id
     * @return
     */
    public Integer deleteGoodssort(Integer id){
        return dao.deleteGoodssort(id);
    }

    /**
     * 插入商品类别
     * @param goodsSort
     * @return
     */
    public Integer insertGoodssort(GoodsSort goodsSort){
        return dao.insertGoodssort(goodsSort);
    }

    /**
     * 根据类别id修改商品类别
     * @param id
     * @param name
     * @return
     */
    public Integer updateGoodssort(Integer id,String name){
        return dao.updateGoodssort(id,name);
    }

    /**
     * 查询商品类别信息
     * @param sid
     * @return
     */
    public List<GoodsSort> selectAllnotSid(Integer sid){
        return dao.selectAllnotSid(sid);
    }
}
