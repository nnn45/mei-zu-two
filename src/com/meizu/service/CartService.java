package com.meizu.service;

import com.meizu.dao.CartDao;
import com.meizu.entity.Cart;
import java.util.List;

/**
 * 购物车的业务层
 * @author hxf
 * @date 2023/3/23
 **/
public class CartService {
    CartDao dao = new CartDao();

    /**
     *插入购物车信息
     */
    public Integer InsertCart(Integer did,Integer uid,String  cphoto,Integer count){
        return dao.InsertCart(did, uid, cphoto, count);
    }

    /**
     * 根据用户 查询 购物车信息
     * @param uid
     * @return
     */
    public List<Cart> selectByUid(Integer uid){
        return dao.selectByUid(uid);
    }
    /***
     * 修改数量
     * @Author hxf
     * @Date 2023/3/31 14:00
     **/
    public Integer updateCount(Integer count,Integer uid,Integer did){
        //如果加入的数量大于5，该商品数量就为5
        Cart cart = dao.selectByDid(uid, did);
        System.out.println("count:" + cart.getCount());
        if(cart.getCount() >= 5 || cart.getCount()+count>=5){//数量为5，再加还是5，不做累加(限购)
            return dao.updateCountForFive(uid,did);
        }else {
            return dao.updateCount(count, uid, did);
        }

    }
    /***
     * 修改数量
     * @Author hxf
     * @Date 2023/3/31 13:56
     **/
    public Integer changeCount(Integer count,Integer cid){

        return dao.changeCount(count, cid);
    }
    /**
     * 根据用户 倒叙查询五条 购物车信息
     * @Author lyz
     * @Date 2023/3/30 16:45
     * @Param [uid]
     * @return java.util.List<com.meizu.entity.Cart>
    **/
    public List<Cart> selectByIndexUid(Integer uid){
      return   dao.selectByIndexUid (uid);
    }
    /**
     * 删除购物车信息
     * @Author lyz
     * @Date 2023/3/30 16:45
     * @Param [id]
     * @return java.lang.Integer
    **/

    public Integer deleteCartByCid(Integer id){
        return dao.deleteCartByCid (id);
    }
    /**
     * 让主键自增连续
     * @return
     */
    public Integer doUpId(){
        return  dao.doUpId();
    }
}
