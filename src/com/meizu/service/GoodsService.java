package com.meizu.service;

import com.meizu.dao.GoodsDao;
import com.meizu.entity.Detail;
import com.meizu.entity.Goods;
import com.meizu.entity.GoodsSort;
import com.meizu.entity.Photo;

import java.util.List;

/**
 * 商品类的业务层
 * @author hxf
 * @date 2023/3/24
 **/
public class GoodsService {
    GoodsDao dao = new GoodsDao();

    /**
     * 根据详情id 查询 商品信息 取(名字，优惠，折扣)
     * @param did
     * @return
     */
    public Goods selectConDetailById(Integer did){
        return dao.selectConDetailById(did);
    }
    /**
     * @Author lyz
     * @Date 2023/3/24 16:58
     * @Param [sid]
     * @return java.util.List<com.meizu.entity.Goods>
    **/
    /**
     * 查询商品类别(给后台)
     * @param sid 商品类别ID
     * @return 商品类别集合
     */
    public List<Goods> selectGoodsDetail(Integer sid) {
       return dao.selectGoodsDetail (sid);
    }
    /**
     * 根据类别编号查询 上架 商品
     * @param sid
     * @return
     */
    public List<Goods> selectGoodsDetailQT(Integer sid) {
        return dao.selectGoodsDetailQT(sid);
    }

    /**
     * 根据商品类别id模糊查询商品（给后台）
     * @param str
     * @return
     */
    public List<Goods> selectGoodMF(String str){
        return dao.selectGoodMF(str);
    }
    /**
     * 查询商品类别（名称）
     * @Author lyz
     * @Date 2023/3/24 17:01
     * @return 类别集合
     */
    public List<GoodsSort> selectGoodsSort(){
        return dao.selectGoodsSort ();
    }
    /**
     * 查询商品类别（名称+图片）
     * @return 类别集合
     */
    public List<GoodsSort> selectGoodsSortAsc(){
        return dao.selectGoodsSortAsc ();
    }
    /**
     * 查询所用商品种类的数量（给后台）
     * @Author hxf
     * @Date 2023/4/11 9:55
     **/
    public Goods countGoods(){
        return dao.countGoods();
    }
    //新增商品类别
    public Integer addGoodSort(String sname){
        return dao.addGoodSort (sname);
   }
    //修改商品状态
    public Integer updateGstate(Integer gstate,Integer gid){
        return dao.updateGstate (gstate,gid);
    }
    //查询商品详情
    public List<Detail> selectGoodDetail(Integer gid){
        return dao.selectGoodDetail (gid);
    }
    //查询商品图片
    public List<Photo> selectPhoto(Integer gid){
        return dao.selectPhoto (gid);
    }
    //查商品版本的集合
    public List<Detail> selectDversion() {
        return dao.selectDversion ();
    }
    //查询商品的颜色集合
    public List<Detail> selectDcolor(){
        return dao.selectDcolor ();
    }
    //后台新增商品
    public Integer insertGoods(Goods goods){
        return dao.insertGoods (goods);
    }
    //后台根据商品id查商品,修改用
    public Goods selectGoodsOne(Integer gid){
        return dao.selectGoodsOne (gid);
    }

    /**
     * 后台根据商品id查商品,修改商品信息
     * @param gname
     * @param gdetail
     * @param gdiscount
     * @param gphoto
     * @param gid
     * @return
     */
    public Integer updateGoodXX(String gname,
                              String gdetail,
                              String gdiscount,
                              String gphoto,
                              Integer gid){
        return dao.updateGoodXX(gname,gdetail,gdiscount,gphoto,gid);
    }
    /**
     * 查询所有商品名称
     * @return
     */
    public List<Goods> selectGoodsName(Integer gid){
        return dao.selectGoodsName(gid);
    }
}
