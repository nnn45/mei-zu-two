package com.meizu.service;

import com.meizu.dao.CartDao;
import com.meizu.dao.DetailDao;
import com.meizu.dao.PhotoDao;
import com.meizu.entity.Cart;
import com.meizu.entity.Detail;
import com.meizu.entity.Photo;
import java.util.List;

/**
 * 商品详情页的业务层
 * @author hxf
 * @date 2023/3/23
 **/
public class DetailService {
    DetailDao dao = new DetailDao();
    CartDao cd=new CartDao();
    PhotoDao pd=new PhotoDao();
    /**
     * 根据条件查询详情价格（局部刷新）
     * @param gid
     * @param dcolor
     * @param dversion
     * @return
     */
    public Detail selectPriceByCondition(Integer gid,String dcolor,String dversion){
        return dao.selectPriceByCondition(gid, dcolor, dversion);
    }
    /**
     * 根据条件查询详情价格（局部刷新）
     * @param gid
     * @param dcolor
     * @return
     */
    public Detail selectPriceByConditionTwo(Integer gid,String dcolor){
        return dao.selectPriceByConditionTwo(gid, dcolor);
    }
    /**
     * 根据条件查询详情id
     * @param gid
     * @param dcolor
     * @param dversion
     * @return
     */
    public Detail selectDidByCondition(Integer gid,String dcolor,String dversion){
        return dao.selectDidByCondition(gid, dcolor, dversion);
    }
    /**
     * 根据条件查询详情id(商品类别为4-5)
     * @param gid
     * @param dcolor
     * @return
     */
    public Detail selectDidByConditionTwo(Integer gid,String dcolor){
        return dao.selectDidByConditionTwo(gid, dcolor);
    }

    /**
     * 根据详情id查询gid
     * @param did   详情id
     * @return
     */
    public Detail selectGidByDid(Integer did){
        return dao.selectGidByDid(did);
    }

    /**
     * 根据详情id查询价格（进行详情页的默认展示）
     * @param did   详情id
     * @return
     */
    public Detail selectPrice(Integer did){
        return dao.selectPrice(did);
    }
    /**
     * 查指定商品的所有颜色
     * @param did   商品id
     * @return
     */
    public List<Detail> selectColor(Integer did){
        return dao.selectColor(did);
    }
    /**
     * 查指定商品的所有版本
     * @Author hxf
     * @Date 2023/3/24
     * @param did   商品id
     * @return
     */
    public List<Detail> selectVersion(Integer did){
        return dao.selectVersion(did);
    }
    /**
     * 查指定商品的所有版本
     * @Author hxf
     * @Date 2023/3/24
     * @param gid   商品id
     * @return
     */
    public List<Detail> selectVersion1(Integer gid){
        return dao.selectVersions(gid);
    }
    /**
     * 通过购物车id拿did，
     * 根据详情id查询dprice,dcolor,dversion,gname，Photo组成detail对象
     * @param cid 购物车id
     * @return 商品详情对象
     */
    public Detail selectXQ(Integer cid){
            Cart cart=cd.selectByCid(cid);//获取一条购物车详情
            Detail detail=dao.selectXQ(cart.getDid());//获取一条详情
            detail.setCount(cart.getCount());
            detail.setPhoto(cart.getCphoto());
        return detail;
    }
    /**
     * 根据详情id查询dprice,dcolor,dversion,gname，Photo组成detail对象
     * @param did 详情id
     * @return 详情对象
     */
    public Detail selectXQ1(Integer did,Integer count){
        Detail detail=dao.selectXQ(did);//获取一条详情
        List<Photo> photos= pd.selectPhoto(did);//商品图片集合
        String cphoto=photos.get(0).getPhoto();//获取单张商品图片
        detail.setCount(count);
        detail.setPhoto(cphoto);
        return detail;
    }

    /**
     * 插入详情数据
     * @param gid 商品ID
     * @param dprice 价格
     * @param dcolor 颜色
     * @param dversion 版本
     * @return 新增的主键编号
     */
    public Integer insertDetail(Integer gid,String dprice, String dcolor,String dversion){
        if(dversion==null || "".equals(dversion)){
            return dao.insertDetail2(gid, dprice, dcolor);//无版本的数据插入
        }else {
            return dao.insertDetail1(gid, dprice, dcolor, dversion);//有版本的数据插入
        }
    }
    //后台修改详情
    public Integer updateGoodsDetail(Detail detail){
        return dao.updateGoodsDetail (detail);
    }
    //后台修改图片表
    public Integer updatePhoto(String photo,Integer did){
        return dao.updatePhoto (photo,did);
    }
    //根据版本和颜色查询单条详情
    public Detail selectDetailOne(String banben,String yanse){
        return dao.selectDetailOne (yanse,banben);
    }
    /**
     * 查看商品是否有详情id（给后台判断上下架）
     * @Author hxf
     * @Date 2023/4/17 9:21
     **/
    public List<Detail> selectDetailByGid(Integer gid){
        return dao.selectDetailByGid(gid);
    }
}
