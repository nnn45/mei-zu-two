package com.meizu.service;

import com.meizu.dao.HtuserDao;
import com.meizu.entity.Htuser;
/**
 * 后台管理员的业务层
 * @date 2023/4/19
 **/
public class HtuserService {
    HtuserDao dao=new HtuserDao();
    public Integer HtuserLogin(String phone,String pwd){
        Htuser htuser=new Htuser();
        htuser=dao.HtuserLogin(phone,pwd);
        if (htuser==null){
            return -1;
        }else {
            return 1;
        }
    }
    public Htuser search(String phone){
        return dao.search(phone);
    }
}
