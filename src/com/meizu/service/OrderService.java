package com.meizu.service;

import com.meizu.dao.*;
import com.meizu.entity.Cart;
import com.meizu.entity.Order;
import com.meizu.entity.OrderDetail;
import com.meizu.entity.Photo;

import java.util.ArrayList;
import java.util.List;
/**
 * 订单中心 的业务层
 * @date 2023/4/19
 **/
public class OrderService {
    OrderDao od=new OrderDao();
    OrderDetailDao odd=new OrderDetailDao();
    PhotoDao pd=new PhotoDao();
    CartDao cd=new CartDao();
    DetailDao dd=new DetailDao();

    /**
     * 查询所有订单（后台用）
     * @return 全部订单集合
     */
    public List<Order> selectOrders(){
        List<Order> lists=new ArrayList<>();
            lists= od.selectOrders();
        forOrders(lists);
        return lists;
    }
    /**
     * 查询所用订单数量（给后台）
     * @Author hxf
     * @Date 2023/4/11 9:55
     **/
    public Order countOrders(){
        return od.countOrders();
    }
    //封装的循环查询订单详情集合
    public void forOrders(List<Order> lists) {
        for (Order order : lists) {
            List<OrderDetail> ods= odd.selectOrderDetails(order.getOid());//订单详情集合
            for (OrderDetail orderDetail : ods) {
                List<Photo> photos= pd.selectPhoto(orderDetail.getDid());//商品图片集合
                String cphoto=photos.get(0).getPhoto();//获取单张商品图片
                orderDetail.setCphoto(cphoto);//存入订单详情
            }
            order.setOds(ods);//存入订单
        }
    }

    /**
     * 根据用户id查询该用户的所有订单
     * @param uid 用户id
     * @return 某用户订单集合
     */
    public List<Order> selectAllOrder(Integer uid,Integer ostate){
        List<Order> ordersList=new ArrayList<>();
        if (ostate==99){
            ordersList=od.selectAllOrder(uid);//全部订单集合
        }else{
            ordersList= od.selectByState(uid, ostate);//某种状态的所有订单集合
        }
        forOrders(ordersList);
        return ordersList;
    }

    /**
     * 根据订单ID查询订单详情
     * @param oid 订单ID
     * @return 订单详情集合
     */
    public List<OrderDetail> selectOneOrder(Integer oid){
        List<OrderDetail> ods=odd.selectOrderDetails(oid);
        for (OrderDetail orderDetail : ods) {
            List<Photo> photos= pd.selectPhoto(orderDetail.getDid());//商品图片集合
            String cphoto=photos.get(0).getPhoto();//获取单张商品图片
            orderDetail.setCphoto(cphoto);//存入订单详情
        }
        return ods;
    }

    /**
     * 查询一份订单的信息（状态）
     * @param oid 订单ID
     * @return 订单对象
     */
    public Order one(Integer oid){
        return od.selectOne(oid);
    }

    /**
     * 向订单中存入快递单号
     * @param oid 订单号
     * @param cnum 快递单号
     * @return 更新结果
     */
    public Integer addCNum(Integer oid,String cnum){
        return od.addCNum(oid, cnum);
    }
    /**
     * 更改订单状态
     * @param oid 订单ID
     * @param ostate 订单状态
     * @return 更改结果 >0 成功
     */
    public Integer change(Integer oid,Integer ostate){
        return od.change(oid,ostate);
    }

    /**
     * 新增订单（直接接收商品详情ID和数量）
     * @param uid 用户ID
     * @param aid 地址ID
     * @param count 商品数量
     * @param did 详情ID
     * @return 订单号
     */
    public Integer create1(Integer uid,Integer aid,Integer count,Integer did){
        Integer oid=od.insertOrder(uid, aid);//向订单表新增订单数据
        odd.insertOrderDetail(oid,count,did);//根据订单ID向订单详情表新增数据
        return oid;
    }

    /**
     * 计算单个商品订单的总价
     * @param did 商品详情ID
     * @param count 商品数量
     * @return 总价
     */
    public Integer count1(Integer did,Integer count){
        Integer price=dd.selectPrice(did).getDprice();
        return price*count;
    }
    /**
     * 新增订单，并删除对应购物车的详情信息
     * @param uid 用户ID
     * @param aid 地址ID
     * @param cids 购物车ID数组
     * @return 订单ID，-1-新增订单详情出错 -2删除购物车详情出错
     */
    public Integer create2(Integer uid,Integer aid,Integer[] cids){
        Integer oid=od.insertOrder(uid, aid);
        Integer rs=0;
        for (Integer cid : cids) {
            Cart cart=cd.selectByCid(cid);//获取一条购物车详情
            rs=odd.insertOrderDetail(oid, cart.getCount(), cart.getDid());//新增订单详情信息
            if (rs<0){
                return rs;//如果新增出错，直接返回（值为-1）
            }else {
                Integer r=cd.deleteCartByCid(cid);//新增未出错，前往删除购物车中对应的详情信息
                if (r<0){
                    return -2;//如果删除购物车信息出错，返回-2
                }
            }
        }
        return oid;//成功完成所有操作，返回订单ID
    }

    /**
     * 计算购物车商品订单总价
     * @param cids 购物车ID集合
     * @return 总价
     */
    public Integer count2(Integer[] cids){
        Integer total=0;
        for (Integer cid : cids) {
            Cart cart=cd.selectByCid(cid);//获取一条购物车详情
            total+=dd.selectPrice(cart.getDid()).getDprice() * cart.getCount();
        }
        return total;
    }
}
