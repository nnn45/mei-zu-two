package com.meizu.service;

import com.meizu.dao.SearchDao;
import com.meizu.utils.Pager;

/**
 * 商品列表（搜索）的业务层
 */
public class SearchService {
    SearchDao dao=new SearchDao();
    public Pager selectPager(Integer no,Integer size,String pname,Integer order){
        if ("手机".equals(pname)){
            pname="魅族";
        }else if ("耳机".equals(pname)){
            pname="魅蓝";
        }else if ("魅族".equals(pname)){
            pname="魅";
        }
        return dao.selectByPager(no,size,pname,order);
    }
}
