package com.meizu.service;

import com.meizu.dao.AddressDao;
import com.meizu.entity.Address;

import java.util.List;

/**
 * 地址管理 的业务层
 * @date 2023/4/19
 **/
public class AddressService {
    AddressDao dao=new AddressDao();
    /**
     * 根据用户id查询用户的所有地址
     * @param uid 用户ID
     * @return 地址集合
     */
    public List<Address> selectAll(Integer uid){
       return dao.selectAll(uid);
    }
    /**
     * 根据地址ID查询具体的地址信息
     * @param aid 地址ID
     * @return 具体的地址信息
     */
    public  Address selectOne(Integer aid){
        return dao.selectOne(aid);
    }
    /**
     * 据用户id查询用户地址总条数
     * @param uid 用户ID
     * @return 地址总条数
     */
    public Address selectCount(Integer uid){
        return dao.selectCount(uid);
    }
    /**
     * 新添地址
     * @param address 地址对象
     * @return 新增的主键编号
     */
    public Integer insertAll(Address address){
        return  dao.insertAll(address);
    }
    /**
     * 根据id删除地址
     * @param aid 地址ID
     * @return 删除结果 >0-成功
     */
    public Integer deleteByaid(Integer aid){
        return dao.deleteByaid(aid);
    }
    /**
     * 修改地址
     * @param address 地址对象
     * @return 修改结果 >0-成功
     */
    public Integer updateByaid(Address address){
        return dao.updateByaid(address);
    }
    /**
     * 修改默认值
     * @param moren 默认值
     * @param aid 地址ID
     * @return 修改结果 >0-成功
     */
    public  Integer updateDefault(Integer moren,Integer aid){
        return dao.updateDefault(moren,aid);
    }
    /**
     * 将原默认地址修改为普通地址
     * @return 修改结果
     */
    public  Integer udateMoren(Integer uid){
        return dao.udateMoren(uid);
    }
    /**
     * 根据用户ID查询出默认地址
     * @return
     */
    public Address selectMrzYi(Integer uid){
        return dao.selectMrzYi(uid);
    }
}
