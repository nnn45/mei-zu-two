package com.meizu.entity;

/**
 * 购物车的实体类
 */
public class Cart {
    private Integer cid;        //购物车ID
    private Integer did;        //详情id
    private Integer uid;        //用户id
    private String  cphoto;     //图片
    //-------------------------------------临时字段
    private String gname;       //商品名字
    private Integer dprice;  //价格
    private String dcolor;      //颜色
    private String dversion;    //版本
    //-------------------------------------
    private Integer count;      //数量

    public Cart(Integer did, Integer count,String cphoto) {
        this.did = did;
        this.count = count;
        this.cphoto = cphoto;
    }

    public Cart(Integer cid, Integer did, String cphoto, String gname, Integer gprice, String gcolor, String gversion, Integer count) {
        this.cid = cid;
        this.did = did;
        this.cphoto = cphoto;
        this.gname = gname;
        this.dprice = gprice;
        this.dcolor = gcolor;
        this.dversion = gversion;
        this.count = count;
    }

    public Cart(Integer cid, Integer did, Integer uid, String cphoto, String gname, Integer gprice, String gcolor, String gversion, Integer count) {
        this.cid = cid;
        this.did = did;
        this.uid = uid;
        this.cphoto = cphoto;
        this.gname = gname;
        this.dprice = gprice;
        this.dcolor = gcolor;
        this.dversion = gversion;
        this.count = count;
    }

    public Cart() {
    }

    @Override
    public String toString() {
        return "Cart{" +
                "cid=" + cid +
                ", did=" + did +
                ", uid=" + uid +
                ", cphoto='" + cphoto + '\'' +
                ", gname='" + gname + '\'' +
                ", gprice=" + dprice +
                ", gcolor='" + dcolor + '\'' +
                ", gversion='" + dversion + '\'' +
                ", count=" + count +
                '}';
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public Integer getDid() {
        return did;
    }

    public void setDid(Integer did) {
        this.did = did;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getCphoto() {
        return cphoto;
    }

    public void setCphoto(String cphoto) {
        this.cphoto = cphoto;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public Integer getDprice() {
        return dprice;
    }

    public void setDprice(Integer gprice) {
        this.dprice = gprice;
    }

    public String getDcolor() {
        return dcolor;
    }

    public void setDcolor(String gcolor) {
        this.dcolor = gcolor;
    }

    public String getDversion() {
        return dversion;
    }

    public void setDversion(String gversion) {
        this.dversion = gversion;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
