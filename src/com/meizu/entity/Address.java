package com.meizu.entity;
/**
 * 地址的实体类
 * @date 2023/4/19
 **/
public class Address {
    private Integer aid;
    private Integer uid;
    private String aname;
    private String sheng;
    private String shi;
    private String qu;
    private String jie;
    private String adetail;
    private String dzphone;
    private Integer set_default;

    //临时字段count
    private Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Address(Integer count) {
        this.count = count;
    }

    public Address() {
    }

    public Address(Integer uid, String aname, String sheng, String shi, String qu, String jie, String adetail, String dzphone, Integer set_default) {
        this.uid = uid;
        this.aname = aname;
        this.sheng = sheng;
        this.shi = shi;
        this.qu = qu;
        this.jie = jie;
        this.adetail = adetail;
        this.dzphone = dzphone;
        this.set_default = set_default;
    }

    public Address(Integer aid, Integer uid, String aname, String sheng, String shi, String qu, String jie, String adetail, String dzphone, Integer set_default) {
        this.aid = aid;
        this.uid = uid;
        this.aname = aname;
        this.sheng = sheng;
        this.shi = shi;
        this.qu = qu;
        this.jie = jie;
        this.adetail = adetail;
        this.dzphone = dzphone;
        this.set_default = set_default;
    }

    public Integer getAid() {
        return aid;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getSheng() {
        return sheng;
    }

    public void setSheng(String sheng) {
        this.sheng = sheng;
    }

    public String getShi() {
        return shi;
    }

    public void setShi(String shi) {
        this.shi = shi;
    }

    public String getQu() {
        return qu;
    }

    public void setQu(String qu) {
        this.qu = qu;
    }

    public String getJie() {
        return jie;
    }

    public void setJie(String jie) {
        this.jie = jie;
    }

    public String getAdetail() {
        return adetail;
    }

    public void setAdetail(String adetail) {
        this.adetail = adetail;
    }

    public String getDzphone() {
        return dzphone;
    }

    public void setDzphone(String dzphone) {
        this.dzphone = dzphone;
    }

    public Integer getSet_default() {
        return set_default;
    }

    public void setSet_default(Integer set_default) {
        this.set_default = set_default;
    }

    @Override
    public String toString() {
        return "Address{" +
                "aid=" + aid +
                ", uid=" + uid +
                ", aname='" + aname + '\'' +
                ", sheng='" + sheng + '\'' +
                ", shi='" + shi + '\'' +
                ", qu='" + qu + '\'' +
                ", jie='" + jie + '\'' +
                ", adetail='" + adetail + '\'' +
                ", dzphone='" + dzphone + '\'' +
                ", set_default=" + set_default +
                ", count=" + count +
                '}';
    }
}
