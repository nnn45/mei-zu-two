package com.meizu.entity;

import java.math.BigDecimal;

/**
 * 商品详情类
 * @author hxf
 **/
public class Detail {
    private Integer did;        //商品详情ID
    private Integer gid;        //商品ID
    private Integer dprice;      //价格
    private String dcolor;      //颜色
    private String dversion;    //版本

    //临时字段
    private String gname;       //商品名字（商品表）
    private String photo;       //图片地址（图片表）

    private String gphoto;      //商品主图（商品表）
    private String gdetail;     //商品描述（商品表）
    private Integer dstate;     //商品状态（商品详情表）
    private Integer count;      //数量
    private String gdiscount;    //商品优惠（商品表）
    private String sname;        //商品类别名称（商品类别表）

    public Integer getCount() {
        return count;
    }
    //单查商品版本的构造方法


    public Detail(String dversion) {
        this.dversion = dversion;
    }



    public Detail(String sname, Integer dprice, String dcolor, String gname, String photo, String gdetail, String dversion, String gdiscount, Integer dstate, Integer did) {
        this.sname=sname;
        this.dprice = dprice;
        this.dcolor = dcolor;
        this.gname = gname;
        this.photo = photo;
        this.gdetail = gdetail;
        this.dversion=dversion;
        this.gdiscount=gdiscount;
        this.dstate=dstate;
        this.did=did;
    }
    //后台查询商品详情的构造方法
    public Detail(Integer did, Integer gid, Integer dprice, String dcolor, String dversion, String gname) {
        this.did = did;
        this.gid = gid;
        this.dprice = dprice;
        this.dcolor = dcolor;
        this.dversion = dversion;
        this.gname = gname;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Integer getDstate() {
        return dstate;
    }

    public void setDstate(Integer dstate) {
        this.dstate = dstate;
    }

    public String getGdiscount() {
        return gdiscount;
    }

    public void setGdiscount(String gdiscount) {
        this.gdiscount = gdiscount;
    }

    public String getGphoto() {
        return gphoto;
    }

    public void setGphoto(String gphoto) {
        this.gphoto = gphoto;
    }

    public String getGdetail() {
        return gdetail;
    }

    public void setGdetail(String gdetail) {
        this.gdetail = gdetail;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Detail(Integer dprice, String photo) {
        this.dprice = dprice;
        this.photo = photo;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Detail(Integer did, Integer gid, Integer dprice, String dcolor, String dversion) {
        this.did = did;
        this.gid = gid;
        this.dprice = dprice;
        this.dcolor = dcolor;
        this.dversion = dversion;
    }

    public Detail() {
    }

    @Override
    public String toString() {
        return "Detail{" +
                "did=" + did +
                ", gid=" + gid +
                ", dprice=" + dprice +
                ", dcolor='" + dcolor + '\'' +
                ", dversion='" + dversion + '\'' +
                '}';
    }

    public Integer getDid() {
        return did;
    }

    public void setDid(Integer did) {
        this.did = did;
    }

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public Integer getDprice() {
        return dprice;
    }

    public void setDprice(Integer dprice) {
        this.dprice = dprice;
    }

    public String getDcolor() {
        return dcolor;
    }

    public void setDcolor(String dcolor) {
        this.dcolor = dcolor;
    }

    public String getDversion() {
        return dversion;
    }

    public void setDversion(String dversion) {
        this.dversion = dversion;
    }
}
