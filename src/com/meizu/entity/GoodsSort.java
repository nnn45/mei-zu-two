package com.meizu.entity;

import java.util.List;

/**
 * 商品类别表 实体类
 * @author hxf
 **/
public class GoodsSort {
    private Integer sid;    //商品类别ID
    private String sname;   //类别名称
    private String sphoto;  //类别图片
    private List<Goods> list;


    public List<Goods> getList() {
        return list;
    }

    public void setList(List<Goods> list) {
        this.list = list;
    }


    public GoodsSort(Integer sid, String sname) {
        this.sid = sid;
        this.sname = sname;
    }

    public GoodsSort(Integer sid, String sname, String sphoto) {
        this.sid = sid;
        this.sname = sname;
        this.sphoto = sphoto;
    }

    public GoodsSort(String sname) {
        this.sname = sname;
    }

    public GoodsSort() {
    }

    @Override
    public String toString() {
        return "GoodsSort{" +
                "sid=" + sid +
                ", sname='" + sname + '\'' +
                ", sphoto='" + sphoto + '\'' +
                '}';
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getSphoto() {
        return sphoto;
    }

    public void setSphoto(String sphoto) {
        this.sphoto = sphoto;
    }
}
