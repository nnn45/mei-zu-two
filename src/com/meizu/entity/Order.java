package com.meizu.entity;

import java.sql.Timestamp;
import java.util.List;
/**
 * 订单表 的实体类
 * @date 2023/4/19
 **/
public class Order {
    private Integer oid;
    private Integer uid;
    private Timestamp otime;
    private Integer ostate;
    private Integer aid;
    private String cnum;//快递单号

    //临时字段 地址
    private List<OrderDetail> ods;
    private Address address;
    private Integer count;//订单数量

    public Order(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Order{" +
                "oid=" + oid +
                ", uid=" + uid +
                ", otime=" + otime +
                ", ostate=" + ostate +
                ", aid=" + aid +
                ", cnum=" + cnum +
                ", ods=" + ods +
                ", address=" + address +
                '}';
    }

    public Order() {
    }

    public Order(Integer oid, Integer uid, Timestamp otime, Integer ostate, Integer aid, String cnum) {
        this.oid = oid;
        this.uid = uid;
        this.otime = otime;
        this.ostate = ostate;
        this.aid = aid;
        this.cnum = cnum;
    }

    public String getCnum() {
        return cnum;
    }

    public void setCnum(String cnum) {
        this.cnum = cnum;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<OrderDetail> getOds() {
        return ods;
    }

    public void setOds(List<OrderDetail> ods) {
        this.ods = ods;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getAid() {
        return aid;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public Timestamp getOtime() {
        return otime;
    }

    public void setOtime(Timestamp otime) {
        this.otime = otime;
    }

    public Integer getOstate() {
        return ostate;
    }

    public void setOstate(Integer ostate) {
        this.ostate = ostate;
    }
}
