package com.meizu.entity;
/**
 * 商品详情的图片表 的实体类
 * @date 2023/4/19
 **/
public class Photo {
    private Integer pid;//图片ID
    private Integer did;//商品详情ID
    private String photo;//图片地址

    public Photo(Integer did, String photo) {
        this.did = did;
        this.photo = photo;
    }

    public Photo(String photo) {
        this.photo = photo;
    }

    public Photo() {
    }

    @Override
    public String toString() {
        return "Photo{" +
                "pid=" + pid +
                ", did=" + did +
                ", photo='" + photo + '\'' +
                '}';
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getDid() {
        return did;
    }

    public void setDid(Integer did) {
        this.did = did;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
