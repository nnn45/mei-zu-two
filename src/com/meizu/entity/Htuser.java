package com.meizu.entity;
/**
 * 后台用户表 的实体类
 * @date 2023/4/19
 **/
public class Htuser {
    private Integer htuid;
    private String htuname;
    private String uphone;
    private String upassword;

    public Htuser() {
    }

    public Htuser(Integer htuid, String htuname, String uphone, String upassword) {
        this.htuid = htuid;
        this.htuname = htuname;
        this.uphone = uphone;
        this.upassword = upassword;
    }

    public Integer getHtuid() {
        return htuid;
    }

    public void setHtuid(Integer htuid) {
        this.htuid = htuid;
    }

    public String getHtuname() {
        return htuname;
    }

    public void setHtuname(String htuname) {
        this.htuname = htuname;
    }

    public String getUphone() {
        return uphone;
    }

    public void setUphone(String uphone) {
        this.uphone = uphone;
    }

    public String getUpassword() {
        return upassword;
    }

    public void setUpassword(String upassword) {
        this.upassword = upassword;
    }

    @Override
    public String toString() {
        return "Htuser{" +
                "htuid=" + htuid +
                ", htuname='" + htuname + '\'' +
                ", uphone='" + uphone + '\'' +
                ", upassword='" + upassword + '\'' +
                '}';
    }
}
