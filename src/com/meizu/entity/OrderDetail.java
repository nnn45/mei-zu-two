package com.meizu.entity;
/**
 * 订单详情表 的实体类
 * @date 2023/4/19
 **/
public class OrderDetail {
    private Integer xqid;//详情ID
    private Integer oid;//订单ID
    private Integer count;//商品详情数量
    private Integer did;//商品详情ID
    private String gname;//商品名 从商品表获取
    private String dcolor;//颜色 从商品详情表获取
    private String dversion;//版本 从商品详情表获取
    private Integer dprice;//价格 从商品详情表获取
    private String cphoto;//商品图片 从图片表获取

    public OrderDetail(Integer xqid, Integer oid, Integer count, Integer did,
                       Integer dprice, String dcolor, String dversion,String gname ) {
        this.xqid = xqid;
        this.oid = oid;
        this.count = count;
        this.did = did;
        this.gname = gname;
        this.dcolor = dcolor;
        this.dversion = dversion;
        this.dprice = dprice;
    }

    public OrderDetail(Integer xqid, Integer oid, Integer count, Integer did,
                       String gname, String dcolor, String dversion, Integer dprice,
                       String cphoto) {
        this.xqid = xqid;
        this.oid = oid;
        this.count = count;
        this.did = did;
        this.gname = gname;
        this.dcolor = dcolor;
        this.dversion = dversion;
        this.dprice = dprice;
        this.cphoto = cphoto;
    }

    public OrderDetail() {
    }

    @Override
    public String toString() {
        return "OrderDetail{" +
                "xqid=" + xqid +
                ", oid=" + oid +
                ", amount=" + count +
                ", did=" + did +
                ", gname='" + gname + '\'' +
                ", dcolor='" + dcolor + '\'' +
                ", dversion='" + dversion + '\'' +
                ", dprice=" + dprice +
                ", cphoto='" + cphoto + '\'' +
                '}';
    }

    public Integer getXqid() {
        return xqid;
    }

    public void setXqid(Integer xqid) {
        this.xqid = xqid;
    }

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getDid() {
        return did;
    }

    public void setDid(Integer did) {
        this.did = did;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getDcolor() {
        return dcolor;
    }

    public void setDcolor(String dcolor) {
        this.dcolor = dcolor;
    }

    public String getDversion() {
        return dversion;
    }

    public void setDversion(String dversion) {
        this.dversion = dversion;
    }

    public Integer getDprice() {
        return dprice;
    }

    public void setDprice(Integer dprice) {
        this.dprice = dprice;
    }

    public String getCphoto() {
        return cphoto;
    }

    public void setCphoto(String cphoto) {
        this.cphoto = cphoto;
    }
}
