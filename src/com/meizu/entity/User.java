package com.meizu.entity;
/**
 * 用户表的实体类
 * @date 2023/4/19
 **/
public class User {
    private Integer uid;
    private String uname;
    private String uheadshot;
    private String uemail;
    private String uphone;
    private String nphone;
    private String upassword;
    private Integer ustate;

    //临时变量
    private Integer count;//用户数量

    public User(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public User() {
    }

    public User(Integer uid, String uname, String uheadshot, String uemail, String uphone, String upassword) {
        this.uid=uid;
        this.uname = uname;
        this.uheadshot = uheadshot;
        this.uemail = uemail;
        this.uphone = uphone;
        this.upassword = upassword;
    }

    public User(Integer uid, String uname, String uheadshot, String uemail, String uphone, String upassword, Integer ustate) {
        this.uid = uid;
        this.uname = uname;
        this.uheadshot = uheadshot;
        this.uemail = uemail;
        this.uphone = uphone;
        this.upassword = upassword;
        this.ustate = ustate;
    }

    public User(Integer uid, String uname, String uemail, String uphone, String upassword) {
        this.uid = uid;
        this.uname = uname;
        this.uemail = uemail;
        this.uphone = uphone;
        this.upassword = upassword;
    }

    public String getNphone() {
        return nphone;
    }

    public void setNphone(String nphone) {
        this.nphone = nphone;
    }

    public Integer getUstate() {
        return ustate;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUheadshot() {
        return uheadshot;
    }

    public void setUheadshot(String uheadshot) {
        this.uheadshot = uheadshot;
    }

    public String getUemail() {
        return uemail;
    }

    public void setUemail(String uemail) {
        this.uemail = uemail;
    }

    public String getUphone() {
        return uphone;
    }

    public void setUphone(String uphoto) {
        this.uphone = uphoto;
    }

    public String getUpassword() {
        return upassword;
    }

    public void setUpassword(String upassword) {
        this.upassword = upassword;
    }

    public Integer isUstate() {
        return ustate;
    }

    public void setUstate(Integer ustate) {
        this.ustate = ustate;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", uname='" + uname + '\'' +
                ", uheadshot='" + uheadshot + '\'' +
                ", uemail='" + uemail + '\'' +
                ", uphoto='" + uphone + '\'' +
                ", upassword='" + upassword + '\'' +
                ", ustate=" + ustate +
                '}';
    }
}
