package com.meizu.entity;

import java.math.BigDecimal;

/**
 * 商品表的实体类
 * @author hxf
 **/
public class Goods {
    private Integer gid;       //商品ID
    private Integer sid;       //商品类别ID
    private String gname;      //商品名字
    private String gdetail;    //商品详情
    private String gdiscount;  //优惠信息
    private String gphoto;     //商品主图
    private String gstate;     //商品状态

    //临时字段
    private Integer did;
    private BigDecimal dprice;
    private Integer count;//商品种类的数量

    public Goods(String gname) {
        this.gname = gname;
    }

    public Goods(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getGstate() {
        return gstate;
    }

    public void setGstate(String gstate) {
        this.gstate = gstate;
    }

    public Integer getDid() {
        return did;
    }

    public void setDid(Integer did) {
        this.did = did;
    }

    public BigDecimal getDprice() {
        return dprice;
    }

    public void setDprice(BigDecimal dprice) {
        this.dprice = dprice;
    }

    public Goods( Integer sid, String gname, String gdetail, String gdiscount, String gphoto) {
        this.sid = sid;
        this.gname = gname;
        this.gdetail = gdetail;
        this.gdiscount = gdiscount;
        this.gphoto = gphoto;
    }

    public Goods(Integer gid, Integer sid, String gname, String gdetail, String gdiscount, String gphoto, Integer did, BigDecimal dprice) {
        this.gid = gid;
        this.sid = sid;
        this.gname = gname;
        this.gdetail = gdetail;
        this.gdiscount = gdiscount;
        this.gphoto = gphoto;
        this.did = did;
        this.dprice = dprice;
    }

    public Goods(Integer gid, Integer sid, String gname, String gdetail, String gdiscount, String gphoto,String gstate) {
        this.gid = gid;
        this.sid = sid;
        this.gname = gname;
        this.gdetail = gdetail;
        this.gdiscount = gdiscount;
        this.gphoto = gphoto;
        this.gstate=gstate;
    }

    public Goods() {
    }

    @Override
    public String toString() {
        return "Goods{" +
                "gid=" + gid +
                ", sid=" + sid +
                ", gname='" + gname + '\'' +
                ", gdetail='" + gdetail + '\'' +
                ", gdiscount='" + gdiscount + '\'' +
                ", gphoto='" + gphoto + '\'' +
                ", did='" + did + '\'' +
                ",dprice='" +dprice + '\''+
                '}';
    }

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getGdetail() {
        return gdetail;
    }

    public void setGdetail(String gdetail) {
        this.gdetail = gdetail;
    }

    public String getGdiscount() {
        return gdiscount;
    }

    public void setGdiscount(String gdiscount) {
        this.gdiscount = gdiscount;
    }

    public String getGphoto() {
        return gphoto;
    }

    public void setGphoto(String gphoto) {
        this.gphoto = gphoto;
    }
}
